#pragma once

#include "Common.h"
#include "DrawableGameComponent.h"

namespace Library
{
	class Light : public GameComponent
	{
		RTTI_DECLARATIONS( Light, GameComponent )

	public:
		Light( Game& game );
		virtual ~Light();

		virtual void Update( const GameTime& gameTime ) override;

		const XMCOLOR& Color() const;
		XMVECTOR ColorVector() const;
		void SetColor( FLOAT r, FLOAT g, FLOAT b, FLOAT a );
		void SetColor( XMCOLOR color );
		void SetColor( FXMVECTOR color );

	protected:
		XMCOLOR mColor;
	};
}

