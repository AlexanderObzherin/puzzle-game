#pragma once

#include "DrawableGameComponent.h"

namespace Library
{
	//This class contains main kinematik staff like position, rotations etc.
	class Actor : public DrawableGameComponent
	{
		RTTI_DECLARATIONS( Actor, DrawableGameComponent )

	public:
		Actor( Game& game, Camera& camera );
		~Actor();

		const XMFLOAT4X4& WorldMatrix() const;
		const XMFLOAT4X4& ScaleMatrix() const;
		const XMFLOAT4X4& RotationMatrix() const;

		const XMFLOAT3& Position() const;
		const XMFLOAT3& Direction() const;
		const XMFLOAT3& Up() const;
		const XMFLOAT3& Right() const;

		XMVECTOR PositionVector() const;
		XMVECTOR DirectionVector() const;
		XMVECTOR UpVector() const;
		XMVECTOR RightVector() const;

		//If we need to set transformations for actor which we are not going to update,
		//we call transformation methods before Initialize()
		void SetPosition( FLOAT x, FLOAT y, FLOAT z );
		void SetPosition( FXMVECTOR position );
		void SetPosition( const XMFLOAT3& position );

		void SetRotation( XMFLOAT3 rotator );
		void SetScale( XMFLOAT3 scale );

		void ApplyRotation( CXMMATRIX transform );
		void ApplyRotation( const XMFLOAT4X4& transform );

		//To move attached actor, we must change mTransitionVectorForAttachment, but not a mPosition
		void AttachTransitionTo( Actor* parentActor, XMFLOAT3 inTransitionVector );
		void ClearParent();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;

	private:
		Actor();
		Actor( const Actor& rhs );
		Actor& operator=( const Actor& rhs );

	protected:
		XMFLOAT4X4 mWorldMatrix;
		XMFLOAT4X4 mScaleMatrix;
		XMFLOAT4X4 mRotationMatrix;

		XMFLOAT3 mPosition;
		XMFLOAT3 mDirection;
		XMFLOAT3 mUp;
		XMFLOAT3 mRight;

		XMFLOAT3 mTransitionVectorForAttachment;
		Actor* mParentActor;
	};
}

