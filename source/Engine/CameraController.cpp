#include "CameraController.h"
#include "Camera.h"
#include "Mouse.h"
#include "Keyboard.h"

namespace Library
{
	const float CameraController::DefaultRotationRate = XMConvertToRadians( 1.0f );
	const float CameraController::DefaultMovementRate = 10.0f;
	const float CameraController::DefaultMouseSensitivity = 100.0f;

	RTTI_DEFINITIONS( CameraController )

		CameraController::CameraController( Game& game, Camera& camera )
		:
		GameComponent( game ),
		mCamera( camera ),
		mKeyboard( nullptr ),
		mMouse( nullptr )
	{
	}

	CameraController::~CameraController()
	{
		mKeyboard = nullptr;
		mMouse = nullptr;
	}

	const Keyboard & CameraController::GetKeyboard() const
	{
		return *mKeyboard;
	}

	void CameraController::SetKeyboard( Keyboard & keyboard )
	{
		mKeyboard = &keyboard;
	}

	const Mouse & CameraController::GetMouse() const
	{
		return *mMouse;
	}

	void CameraController::SetMouse( Mouse & mouse )
	{
		mMouse = &mouse;
	}

	float & CameraController::MouseSensitivity()
	{
		return mMouseSensitivity;
	}

	float & CameraController::RotationRate()
	{
		return mRotationRate;
	}

	float & CameraController::MovementRate()
	{
		return mMovementRate;
	}

	void CameraController::SetPosition( FLOAT x, FLOAT y, FLOAT z )
	{
		XMVECTOR position = XMVectorSet( x, y, z, 1.0f );
		SetPosition( position );
	}

	void CameraController::SetPosition( FXMVECTOR position )
	{
		XMStoreFloat3( &mCamera.mPosition, position );
	}

	void CameraController::SetPosition( const XMFLOAT3 & position )
	{
		mCamera.mPosition = position;
	}

	void CameraController::ApplyRotation( CXMMATRIX transform )
	{
		XMVECTOR direction = XMLoadFloat3( &mCamera.mDirection );
		XMVECTOR up = XMLoadFloat3( &mCamera.mUp );

		direction = XMVector3TransformNormal( direction, transform );
		direction = XMVector3Normalize( direction );

		up = XMVector3TransformNormal( up, transform );
		up = XMVector3Normalize( up );

		XMVECTOR right = XMVector3Cross( direction, up );
		up = XMVector3Cross( right, direction );

		XMStoreFloat3( &mCamera.mDirection, direction );
		XMStoreFloat3( &mCamera.mUp, up );
		XMStoreFloat3( &mCamera.mRight, right );
	}

	void CameraController::ApplyRotation( const XMFLOAT4X4 & transform )
	{
		XMMATRIX transformMatrix = XMLoadFloat4x4( &transform );
		ApplyRotation( transformMatrix );
	}

	void CameraController::Initialize()
	{
		mKeyboard = (Keyboard*)mGame->Services().GetService( Keyboard::TypeIdClass() );
		mMouse = (Mouse*)mGame->Services().GetService( Mouse::TypeIdClass() );
	}

	void CameraController::Update( const GameTime & gameTime )
	{

	}
}


