#pragma once

#include "GameComponent.h"

namespace Library
{
	class GameTime;
	class CameraController;

	class Camera : public GameComponent
	{
		RTTI_DECLARATIONS( Camera, GameComponent )

	friend CameraController;

	public:
		Camera( Game& game );
		Camera( Game& game, float fieldOfView, float aspectRatio, float nearPlaneDistance, float farPlaneDistance );

		virtual ~Camera();

		const XMFLOAT3& Position() const;
		const XMFLOAT3& Direction() const;
		const XMFLOAT3& Up() const;
		const XMFLOAT3& Right() const;

		XMVECTOR PositionVector() const;
		XMVECTOR DirectionVector() const;
		XMVECTOR UpVector() const;
		XMVECTOR RightVector() const;

		float AspectRatio() const;
		float FieldOfView() const;
		float NearPlaneDistance() const;
		float FarPlaneDistance() const;

		XMMATRIX ViewMatrix() const;
		XMMATRIX ProjectionMatrix() const;
		XMMATRIX ViewProjectionMatrix() const;

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;

	protected:
		virtual void Reset();

		virtual void UpdateViewMatrix();
		virtual void UpdateProjectionMatrix();

		static const float DefaultFieldOfView;
		static const float DefaultAspectRatio;
		static const float DefaultNearPlaneDistance;
		static const float DefaultFarPlaneDistance;

		float mFieldOfView;
		float mAspectRatio;
		float mNearPlaneDistance;
		float mFarPlaneDistance;

		XMFLOAT3 mPosition;
		XMFLOAT3 mDirection;
		XMFLOAT3 mUp;
		XMFLOAT3 mRight;

		XMFLOAT4X4 mViewMatrix;
		XMFLOAT4X4 mProjectionMatrix;

	private:
		Camera( const Camera& rhs );
		Camera& operator=(const Camera& rhs);
	};
}

