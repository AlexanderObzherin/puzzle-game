#include "DirectionalLight.h"
#include "VectorHelper.h"
#include "Game.h"
#include "Keyboard.h"
#include "Mouse.h"


namespace Library
{
	const float DirectionalLight::LightModulationRate = UCHAR_MAX;
	const XMFLOAT2 DirectionalLight::LightRotationRate = XMFLOAT2( XM_2PI, XM_2PI );

	RTTI_DEFINITIONS( DirectionalLight )

	DirectionalLight::DirectionalLight( Game& game )
		: 
		Light( game ), 
		mDirection( Vector3Helper::Down ), 
		mUp( Vector3Helper::Forward ), 
		mRight( Vector3Helper::Right ),
		mKeyboard( nullptr ),
		mMouse( nullptr )
	{
	}

	DirectionalLight::~DirectionalLight()
	{
	}

	void DirectionalLight::Initialize()
	{
		mKeyboard = (Keyboard*)mGame->Services().GetService( Keyboard::TypeIdClass() );
		mMouse = (Mouse*)mGame->Services().GetService( Mouse::TypeIdClass() );

	}

	void DirectionalLight::Update( const GameTime & gameTime )
	{
		UpdateDirectionalLight( gameTime );
	}

	void DirectionalLight::SetRotation( XMFLOAT3 rotator )
	{
		rotator.x = XMConvertToRadians( rotator.x );
		rotator.y = XMConvertToRadians( rotator.y );
		rotator.z = XMConvertToRadians( rotator.z );

		ApplyRotation( XMMatrixRotationRollPitchYaw( rotator.x, rotator.y, rotator.z ) );
	}

	void DirectionalLight::UpdateDirectionalLight( const GameTime & gameTime )
	{
		static float directionalIntensity = this->Color().a;
		float elapsedTime = (float)gameTime.ElapsedGameTime();

		// Update directional light intensity		
		if(mKeyboard->IsKeyDown( DIK_HOME ) && directionalIntensity < UCHAR_MAX)
		{
			directionalIntensity += LightModulationRate * elapsedTime;

			XMCOLOR directionalLightColor = this->Color();
			directionalLightColor.a = (UCHAR)XMMin<float>( directionalIntensity, UCHAR_MAX );
			this->SetColor( directionalLightColor );
		}
		if(mKeyboard->IsKeyDown( DIK_END ) && directionalIntensity > 0)
		{
			directionalIntensity -= LightModulationRate * elapsedTime;

			XMCOLOR directionalLightColor = this->Color();
			directionalLightColor.a = (UCHAR)XMMax<float>( directionalIntensity, 0.0f );
			this->SetColor( directionalLightColor );
		}

		// Rotate directional light
		XMFLOAT2 rotationAmount = Vector2Helper::Zero;
		if(mKeyboard->IsKeyDown( DIK_LEFTARROW ))
		{
			rotationAmount.x += LightRotationRate.x * elapsedTime;
		}
		if(mKeyboard->IsKeyDown( DIK_RIGHTARROW ))
		{
			rotationAmount.x -= LightRotationRate.x * elapsedTime;
		}
		if(mKeyboard->IsKeyDown( DIK_UPARROW ))
		{
			rotationAmount.y += LightRotationRate.y * elapsedTime;
		}
		if(mKeyboard->IsKeyDown( DIK_DOWNARROW ))
		{
			rotationAmount.y -= LightRotationRate.y * elapsedTime;
		}

		XMMATRIX lightRotationMatrix = XMMatrixIdentity();
		if(rotationAmount.x != 0)
		{
			lightRotationMatrix = XMMatrixRotationY( rotationAmount.x );
		}

		if(rotationAmount.y != 0)
		{
			XMMATRIX lightRotationAxisMatrix = XMMatrixRotationAxis( this->RightVector(), rotationAmount.y );
			lightRotationMatrix *= lightRotationAxisMatrix;
		}

		if(rotationAmount.x != 0.0f || rotationAmount.y != 0.0f)
		{
			this->ApplyRotation( lightRotationMatrix );
		}
	}

	const XMFLOAT3& DirectionalLight::Direction() const
	{
		return mDirection;
	}

	const XMFLOAT3& DirectionalLight::Up() const
	{
		return mUp;
	}

	const XMFLOAT3& DirectionalLight::Right() const
	{
		return mRight;
	}

	XMVECTOR DirectionalLight::DirectionVector() const
	{
		return XMLoadFloat3(&mDirection);
	}

	XMVECTOR DirectionalLight::UpVector() const
	{
		return XMLoadFloat3(&mUp);
	}

	XMVECTOR DirectionalLight::RightVector() const
	{
		return XMLoadFloat3(&mRight);
	}

	void DirectionalLight::ApplyRotation(CXMMATRIX transform)
	{
		XMVECTOR direction = XMLoadFloat3( &mDirection );
		XMVECTOR up = XMLoadFloat3( &mUp );

		direction = XMVector3TransformNormal( direction, transform );
		direction = XMVector3Normalize( direction );

		up = XMVector3TransformNormal( up, transform );
		up = XMVector3Normalize( up );

		XMVECTOR right = XMVector3Cross( direction, up) ;
		up = XMVector3Cross( right, direction );

		XMStoreFloat3( &mDirection, direction );
		XMStoreFloat3( &mUp, up );
		XMStoreFloat3( &mRight, right );
	}

	void DirectionalLight::ApplyRotation(const XMFLOAT4X4& transform)
	{
		XMMATRIX transformMatrix = XMLoadFloat4x4(&transform);
		ApplyRotation( transformMatrix );
	}
}