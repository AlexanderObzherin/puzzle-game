#pragma once

#include "Game.h"

namespace Library
{
	class Camera;
	class Keyboard;
	class Mouse;

	class CameraController : public GameComponent
	{
		RTTI_DECLARATIONS( CameraController, GameComponent )

	public:
		CameraController( Game& game, Camera& camera );
		~CameraController();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;

		const Keyboard& GetKeyboard() const;
		void SetKeyboard( Keyboard& keyboard );

		const Mouse& GetMouse() const;
		void SetMouse( Mouse& mouse );

		float& MouseSensitivity();
		float& RotationRate();
		float& MovementRate();

		static const float DefaultMouseSensitivity;
		static const float DefaultRotationRate;
		static const float DefaultMovementRate;

		virtual void SetPosition( FLOAT x, FLOAT y, FLOAT z );
		virtual void SetPosition( FXMVECTOR position );
		virtual void SetPosition( const XMFLOAT3& position );

		void ApplyRotation( CXMMATRIX transform );
		void ApplyRotation( const XMFLOAT4X4& transform );

	protected:
		Camera& mCamera; //source to current camera to manipulate with

		float mMouseSensitivity;
		float mRotationRate;
		float mMovementRate;

		Keyboard* mKeyboard;
		Mouse* mMouse;
	};
}
