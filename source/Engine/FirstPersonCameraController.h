#pragma once

#include "Game.h"
#include "Camera.h"
#include "CameraController.h"

namespace Library
{
	class FirstPersonCameraController : public CameraController
	{
		RTTI_DECLARATIONS( FirstPersonCameraController, CameraController )

	public:
		FirstPersonCameraController( Game& game, Camera& camera );
		~FirstPersonCameraController();

		virtual void Update( const GameTime& gameTime ) override;

	protected:
		float mMouseSensitivity;
		float mRotationRate;
		float mMovementRate;
	};
}
