#include "FirstPersonCameraController.h"
#include "Mouse.h"
#include "Keyboard.h"
#include "VectorHelper.h"


namespace Library
{
	RTTI_DEFINITIONS( FirstPersonCameraController )

	FirstPersonCameraController::FirstPersonCameraController( Game& game, Camera& camera )
		:
		CameraController( game, camera ),
		mMouseSensitivity( DefaultMouseSensitivity ),
		mRotationRate( DefaultRotationRate ),
		mMovementRate( DefaultMovementRate )
	{

	}

	FirstPersonCameraController::~FirstPersonCameraController()
	{

	}

	void FirstPersonCameraController::Update( const GameTime & gameTime )
	{
		XMFLOAT2 movementAmount = Vector2Helper::Zero;
		if (mKeyboard != nullptr)
		{
			if (mKeyboard->IsKeyDown( DIK_W ))
			{
				movementAmount.y = 1.0f;
			}

			if (mKeyboard->IsKeyDown( DIK_S ))
			{
				movementAmount.y = -1.0f;
			}

			if (mKeyboard->IsKeyDown( DIK_A ))
			{
				movementAmount.x = -1.0f;
			}

			if (mKeyboard->IsKeyDown( DIK_D ))
			{
				movementAmount.x = 1.0f;
			}
		}

		XMFLOAT2 rotationAmount = Vector2Helper::Zero;
		if ((mMouse != nullptr) && (mMouse->IsButtonHeldDown( MouseButtonsLeft )))
		{
			LPDIMOUSESTATE mouseState = mMouse->CurrentState();
			rotationAmount.x = -mouseState->lX * mMouseSensitivity;
			rotationAmount.y = -mouseState->lY * mMouseSensitivity;
		}

		float elapsedTime = (float)gameTime.ElapsedGameTime();
		XMVECTOR rotationVector = XMLoadFloat2( &rotationAmount ) * mRotationRate * elapsedTime;
		//XMVECTOR right = XMLoadFloat3( &mCamera.mRight );
		XMVECTOR right = XMLoadFloat3( &mCamera.Right() );
		XMMATRIX pitchMatrix = XMMatrixRotationAxis( right, XMVectorGetY( rotationVector ) );
		XMMATRIX yawMatrix = XMMatrixRotationY( XMVectorGetX( rotationVector ) );

		ApplyRotation( XMMatrixMultiply( pitchMatrix, yawMatrix ) );

		//XMVECTOR position = XMLoadFloat3( &mPosition );
		XMVECTOR position = XMLoadFloat3( &mCamera.Position() );
		XMVECTOR movement = XMLoadFloat2( &movementAmount ) * mMovementRate * elapsedTime;

		XMVECTOR strafe = right * XMVectorGetX( movement );
		position += strafe;

		//XMVECTOR forward = XMLoadFloat3( &mDirection ) * XMVectorGetY( movement );
		XMVECTOR forward = XMLoadFloat3( &mCamera.Direction() ) * XMVectorGetY( movement );
		position += forward;

		//XMStoreFloat3( &mPosition, position );
		SetPosition( position );
	}
}