#include "Actor.h"
#include "VectorHelper.h"
#include "MatrixHelper.h"

namespace Library
{
	RTTI_DEFINITIONS( Actor )

		Actor::Actor( Game& game, Camera& camera )
		:
		DrawableGameComponent( game, camera ),
		mWorldMatrix( MatrixHelper::Identity ),
		mScaleMatrix( MatrixHelper::Identity ),
		mRotationMatrix( MatrixHelper::Identity ),
		mPosition( Vector3Helper::Zero ),
		mDirection( Vector3Helper::Forward ),
		mUp( Vector3Helper::Up ),
		mRight( Vector3Helper::Right ),
		mTransitionVectorForAttachment( Vector3Helper::Zero ),
		mParentActor( nullptr )
	{
	}

	Actor::~Actor()
	{
	}

	const XMFLOAT4X4 & Actor::WorldMatrix() const
	{
		return mWorldMatrix;
	}

	const XMFLOAT4X4 & Actor::ScaleMatrix() const
	{
		return mScaleMatrix;
	}

	const XMFLOAT4X4 & Actor::RotationMatrix() const
	{
		return mScaleMatrix;
	}

	const XMFLOAT3 & Actor::Position() const
	{
		return mPosition;
	}

	const XMFLOAT3 & Actor::Direction() const
	{
		return mDirection;
	}

	const XMFLOAT3 & Actor::Up() const
	{
		return mUp;
	}

	const XMFLOAT3 & Actor::Right() const
	{
		return mRight;
	}

	XMVECTOR Actor::PositionVector() const
	{
		return XMLoadFloat3( &mPosition );
	}

	XMVECTOR Actor::DirectionVector() const
	{
		return XMLoadFloat3( &mDirection );
	}

	XMVECTOR Actor::UpVector() const
	{
		return XMLoadFloat3( &mUp );
	}

	XMVECTOR Actor::RightVector() const
	{
		return XMLoadFloat3( &mRight );
	}

	void Actor::SetPosition( FLOAT x, FLOAT y, FLOAT z )
	{
		XMVECTOR position = XMVectorSet( x, y, z, 1.0f );
		SetPosition( position );
	}

	void Actor::SetPosition( FXMVECTOR position )
	{
		XMStoreFloat3( &mPosition, position );
	}

	void Actor::SetPosition( const XMFLOAT3 & position )
	{
		mPosition = position;
	}

	void Actor::SetRotation( XMFLOAT3 rotator )
	{
		rotator.x = XMConvertToRadians( rotator.x );
		rotator.y = XMConvertToRadians( rotator.y );
		rotator.z = XMConvertToRadians( rotator.z );

		XMStoreFloat4x4( &mRotationMatrix, XMMatrixRotationRollPitchYaw( rotator.x, rotator.y, rotator.z ) );
		ApplyRotation( XMMatrixRotationRollPitchYaw( rotator.x, rotator.y, rotator.z ) );
	}

	void Actor::SetScale( XMFLOAT3 scale )
	{
		XMVECTOR scaleVector = XMLoadFloat3( &scale );
		XMStoreFloat4x4( &mScaleMatrix, XMMatrixScalingFromVector( scaleVector ) );
	}

	void Actor::ApplyRotation( CXMMATRIX transform )
	{
		XMVECTOR direction = XMLoadFloat3( &mDirection );
		XMVECTOR up = XMLoadFloat3( &mUp );

		direction = XMVector3TransformNormal( direction, transform );
		direction = XMVector3Normalize( direction );

		up = XMVector3TransformNormal( up, transform );
		up = XMVector3Normalize( up );

		XMVECTOR right = XMVector3Cross( direction, up );
		up = XMVector3Cross( right, direction );

		XMStoreFloat3( &mDirection, direction );
		XMStoreFloat3( &mUp, up );
		XMStoreFloat3( &mRight, right );
	}

	void Actor::ApplyRotation( const XMFLOAT4X4 & transform )
	{
		XMMATRIX transformMatrix = XMLoadFloat4x4( &transform );
		ApplyRotation( transformMatrix );
	}

	void Actor::AttachTransitionTo( Actor * parentActor, XMFLOAT3 inTransitionVector = XMFLOAT3( 0.0f, 0.0f, 0.0f ) )
	{
		mParentActor = parentActor;
		mTransitionVectorForAttachment = inTransitionVector;

		if( mParentActor )
		{
			XMVECTOR transitionVector = XMLoadFloat3( &mTransitionVectorForAttachment ) + mParentActor->PositionVector();
			SetPosition( transitionVector );
		}

		XMVECTOR position = XMLoadFloat3( &mPosition );
		XMMATRIX worldMatrix = XMMatrixIdentity();
		XMMATRIX rotationMatrix = XMLoadFloat4x4( &mRotationMatrix );
		XMMATRIX scaleMatrix = XMLoadFloat4x4( &mScaleMatrix );
		MatrixHelper::SetTranslation( worldMatrix, mPosition );

		XMStoreFloat4x4( &mWorldMatrix, scaleMatrix * rotationMatrix * worldMatrix );
	}

	void Actor::ClearParent()
	{
		mParentActor = nullptr;
		mTransitionVectorForAttachment = XMFLOAT3( 0.0f, 0.0f, 0.0f );
	}

	void Actor::Initialize()
	{
		//W = Raxial * T * Rorbit

		XMVECTOR position = XMLoadFloat3( &mPosition );
		XMMATRIX worldMatrix = XMMatrixIdentity();
		XMMATRIX rotationMatrix = XMLoadFloat4x4( &mRotationMatrix );
		XMMATRIX scaleMatrix = XMLoadFloat4x4( &mScaleMatrix );
		MatrixHelper::SetTranslation( worldMatrix, mPosition );

		XMStoreFloat4x4( &mWorldMatrix, scaleMatrix * rotationMatrix * worldMatrix );
	}

	void Actor::Update( const GameTime & gameTime )
	{
		if( mParentActor )
		{
			XMVECTOR transitionVector = XMLoadFloat3( &mTransitionVectorForAttachment ) + mParentActor->PositionVector();
			SetPosition( transitionVector );
		}

		XMVECTOR position = XMLoadFloat3( &mPosition );
		XMMATRIX worldMatrix = XMMatrixIdentity();
		XMMATRIX rotationMatrix = XMLoadFloat4x4( &mRotationMatrix );
		XMMATRIX scaleMatrix = XMLoadFloat4x4( &mScaleMatrix );
		MatrixHelper::SetTranslation( worldMatrix, mPosition );

		XMStoreFloat4x4( &mWorldMatrix, scaleMatrix * rotationMatrix * worldMatrix );
	}
}