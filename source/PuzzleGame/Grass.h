#pragma once
#include "Actor.h"

using namespace Library;

namespace Library
{
	class DirectionalLight;
}

namespace Rendering
{
	class StaticModel;

	class Grass : public Actor
	{
		RTTI_DECLARATIONS( Grass, Actor )

	public:
		Grass( Game& game, Camera& camera, const DirectionalLight& directionalLightSource );
		~Grass();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

	private:
		Grass();
		Grass( const Grass& rhs );
		Grass& operator=( const Grass& rhs );

		const DirectionalLight& mDirectionalLightSource;
		StaticModel* mShape;
	};
}