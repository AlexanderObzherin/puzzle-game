#pragma once

#include <functional>
#include <map>
#include <random>
#include <algorithm>

namespace Rendering
{
	template <typename T>
	class Randomizer
	{
	public:
		Randomizer()
		:
			mSum( 0 )
		{}
		~Randomizer() {}
		
		std::function<T*()>& GetRandomNode()
		{
			int min = 0;

			//We create random int next
			std::random_device randomDevice;
			std::default_random_engine randomGenerator( randomDevice() );
			std::uniform_int_distribution<int> probDistribution( min, mSum );
			int number = probDistribution( randomGenerator );

			//And now we can just select difined node
			for( auto& it : mSetList )
			{
				if( number <= it.first )
				{
					return it.second;
				}
			}

			return mDefaultFunc;
		}

		void AddToSetList( unsigned int probability, std::function<T*()> func )
		{
			mSum += probability;
			mSetList[mSum] = func;
		}

	protected:
		std::map< unsigned int, std::function<T*()>> mSetList;
		std::function<T*()> mDefaultFunc = []() { return nullptr; };
		int mSum;
	};
}