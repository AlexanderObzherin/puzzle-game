#pragma once

#include "Actor.h"

using namespace Library;

namespace Rendering
{
	class CameraTarget : public Actor
	{
		RTTI_DECLARATIONS( CameraTarget, Actor )

	public:
		CameraTarget( Game& game, Camera& camera );
		~CameraTarget();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		XMFLOAT3 GetTargetPosition()
		{
			return mTargetPosition;
		}
		bool IsMoving();
		float GetDistanceBetweenTarget();

		//method to smooth folowing to new position
		void SetNewPosition( XMFLOAT3 newPosition );
		void SmoothFollowing( const GameTime& gameTime );

	protected:
		XMFLOAT3 mTargetPosition;
		float mVelocityFactor;

	};
}
