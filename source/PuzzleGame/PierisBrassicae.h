#pragma once
#include "Insect.h"
#include <list>
#include <functional>

using namespace Library;

namespace Library
{
	class DirectionalLight;
}

namespace Rendering
{
	class PierisBrassicae : public Insect
	{
		RTTI_DECLARATIONS( PierisBrassicae, Insect )

	public:
		PierisBrassicae( Game& game, Camera& camera, const DirectionalLight& directionalLightSource );
		~PierisBrassicae();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		virtual void PlayWalkingScenario() override;

	protected:
		virtual void SmoothRotation( const GameTime& gameTime ) override;

	private:
		void PickFlower( const GameTime& gameTime );
		void Sit( const GameTime& gameTime );
		void FlyAway( const GameTime& gameTime );

		//To fly away we create temporary point (it lives only inside insect, not in blockpath)
		std::unique_ptr<Point> mAdditionalPoint;
		float mTimer;
	};

}