#include "LadybugNPC.h"

#include "AnimatedModel.h"

namespace Rendering
{
	RTTI_DEFINITIONS( LadybugNPC )

		LadybugNPC::LadybugNPC( Game& game, Camera& camera, const DirectionalLight& directionalLightSource )
		:
		Insect( game, camera, directionalLightSource ),
		bIsRunning( false )
	{}
	LadybugNPC::~LadybugNPC()
	{
	}

	void LadybugNPC::Initialize()
	{
		Insect::Initialize();

		SetScale( XMFLOAT3( 0.6f, 0.6f, 0.6f ) );

		mAnimatedModel = std::unique_ptr<AnimatedModel>( new AnimatedModel( *this, mDirectionalLightSource ) );
		mAnimatedModel->SetModelName( "Content\\Models\\Ladybug\\Ladybug.dae" );
		mAnimatedModel->SetColorTexture( L"Content\\Models\\Ladybug\\LadybugColorTexture.png" );
		mAnimatedModel->SetNormalMapTexture( L"Content\\Models\\Ladybug\\LadybugNormalMapTexture.png" );
		mAnimatedModel->Initialize();
		mAnimatedModel->DrawPhongWithNormalMap();

		mAnimatedModel->AddAnimation( "LadybugMoving", 0.0f, 0.7f );
		mAnimatedModel->AddAnimation( "LadybugWingsMoving", 1.1f, 7.0f );
		mAnimatedModel->SetAnimation( "LadybugMoving" );


	}
	void LadybugNPC::Update( const GameTime & gameTime )
	{
		Insect::Update( gameTime );

	}
	void LadybugNPC::Draw( const GameTime & gameTime )
	{
		Insect::Draw( gameTime );

	}

	void LadybugNPC::Run( const GameTime & gameTime )
	{
		mAnimatedModel->SetAnimation( "LadybugMoving" );
		WalkBetweenPoints();
		Move( gameTime );
		SmoothRotation( gameTime );
	}
	void LadybugNPC::Stop( const GameTime & gameTime )
	{
		mAnimatedModel->SetAnimation( "LadybugWingsMoving" );
	}
	bool LadybugNPC::GetIsRunning() const
	{
		return bIsRunning;
	}


}