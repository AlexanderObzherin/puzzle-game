#pragma once

#include "Scene.h"

using namespace Library;

namespace Library
{
	class Camera;
	class FirstPersonCameraController;
	class Keyboard;
	class Mouse;
	class RenderStateHelper;
	class DirectionalLight;
}

namespace DirectX
{
	
	class SpriteBatch;
	class SpriteFont;
}

namespace Rendering
{
	class Logo;
	class ThirdPersonCameraController;
	class LadybugNPC;

	class StartScene : public Scene
	{
		RTTI_DECLARATIONS( StartScene, Scene )

	public:
		StartScene( Game& game, SceneManager& sceneManager );
		~StartScene();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

	private:
		static const XMVECTORF32 BackgroundColor;

		SpriteBatch* mSpriteBatch;
		SpriteFont* mSpriteFont;
		RenderStateHelper* mRenderStateHelper;
		Keyboard* mKeyboard;

		Camera* mCamera;
		//ThirdPersonCameraController* mCameraController;
		FirstPersonCameraController* mCameraController;
		DirectionalLight* mDirectionalLight;

		Logo* mLogo;
		LadybugNPC* mLadybugNPC;


		float mTimer;
	};

}