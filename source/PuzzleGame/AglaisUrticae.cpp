#include "AglaisUrticae.h"
#include "AnimatedModel.h"
#include "GameTime.h"
#include "BlockPath.h"
#include "GameGrid.h"
#include "Block.h"
#include "Point.h"
#include "LawnBlockPath.h"

//for std::bind stuff. Is needed to bind functions with parameters.
using namespace std::placeholders;

namespace Rendering
{
	RTTI_DEFINITIONS(AglaisUrticae)

		AglaisUrticae::AglaisUrticae( Game& game, Camera& camera, const DirectionalLight& directionalLightSource )
		:
		Insect( game, camera, directionalLightSource ),
		mTimer( 0.0f )
	{}
	AglaisUrticae::~AglaisUrticae()
	{
	}

	void AglaisUrticae::Initialize()
	{
		Insect::Initialize();

		SetScale(XMFLOAT3(0.8f, 0.8f, 0.8f));

		mAnimatedModel = std::unique_ptr<AnimatedModel>( new AnimatedModel( *this, mDirectionalLightSource ) );
		mAnimatedModel->SetModelName( "Content\\Models\\AglaisUrticae\\AglaisUrticae.dae" );
		mAnimatedModel->SetColorTexture( L"Content\\Models\\AglaisUrticae\\AglaisUrticaeColorTexture.png" );
		
		mAnimatedModel->Initialize();
		mAnimatedModel->DrawAmbientDiffuse();

		mAnimatedModel->AddAnimation( "AglaisUrticaeSitting", 0.0f, 1.8f );
		mAnimatedModel->AddAnimation( "AglaisUrticaeFlying", 1.8f, 2.0f );
		mAnimatedModel->SetAnimation( "AglaisUrticaeFlying" );

		mVelocityFactor = 15.0f;
		mAngularVelocity = 100.0f;
	}
	void AglaisUrticae::Update( const GameTime & gameTime )
	{
		Insect::Update( gameTime );
	}
	void AglaisUrticae::Draw( const GameTime & gameTime )
	{
		Insect::Draw( gameTime );
	}

	void AglaisUrticae::PlayWalkingScenario()
	{
		SetPosition( CreateRandomPosition() );
		mAnimatedModel->SetAnimation( "AglaisUrticaeFlying" );
		mTargetPoint = SearchForClosestFreePoint();
		if ( mTargetPoint )
		{
			mActionState = std::bind( &AglaisUrticae::PickFlower, this, _1 );
		}
		else
		{
			//If there is no free flower for this, set it inactive
			SetEnabled( false );
		}
	}
	void AglaisUrticae::SmoothRotation( const GameTime & gameTime )
	{
		//Project Velocity direction unto XZ plane first
		XMVECTOR cross1 = CrossProduct( XMLoadFloat3( &mVelocityDirection ), XMLoadFloat3( &Vector3Helper::Up ) );
		XMVECTOR cross2 = CrossProduct( XMLoadFloat3(&Vector3Helper::Up), cross1 );
		XMFLOAT3 proj;
		XMStoreFloat3( &proj, cross2 );

		mTargetYaw = GetRotator( proj ).y;

		if ( (int)mCurrentYaw > (int)mTargetYaw )
		{
			mCurrentYaw -= mAngularVelocity * gameTime.ElapsedGameTime();
		}
		else
		{
			mCurrentYaw += mAngularVelocity * gameTime.ElapsedGameTime();
		}

		SetRotation( XMFLOAT3(0.0f, mCurrentYaw, 0.0f) );
	}
	void AglaisUrticae::PickFlower( const GameTime & gameTime )
	{
		//Move to target points
		if ( mTargetPoint )
		{
			XMVECTOR distanceVector = ( mTargetPoint->PositionVector() - this->PositionVector() );

			float distance = XMVector3Length( distanceVector ).m128_f32[0];
			if ( (int)distance > 0 )
			{
				XMStoreFloat3( &mVelocityDirection, distanceVector * 0.05f );

				if ( mTargetPoint )
				{
					mTargetPoint->GetBlock().SetIsOwnedByInsect( true );
				}
			}
			else
			{
				mAnimatedModel->SetAnimation( "AglaisUrticaeSitting" );
				mActionState = bind( &AglaisUrticae::Sit, this, _1 );
			}


			Move( gameTime );
			SmoothRotation( gameTime );
		}
	}
	void AglaisUrticae::Sit( const GameTime & gameTime )
	{
		mTimer += gameTime.ElapsedGameTime();

		if ( mTimer > 20.0f )
		{
			mTargetPoint->GetBlock().SetIsOwnedByInsect( false );

			mAdditionalPoint = std::make_unique<Point>( *mGame, *mCamera, mDirectionalLightSource, mTargetPoint->GetBlock() );
			mTargetPoint = mAdditionalPoint.get();

			mTargetPoint->SetPosition( CreateRandomPosition() );

			mActionState = bind( &AglaisUrticae::FlyAway, this, _1 );
		}

	}
	void AglaisUrticae::FlyAway( const GameTime & gameTime )
	{
		mAnimatedModel->SetAnimation( "AglaisUrticaeFlying" );

		//Move to target points
		if ( mTargetPoint )
		{
			XMVECTOR distanceVector = mTargetPoint->PositionVector() - this->PositionVector();
			float distance = XMVector3Length(distanceVector).m128_f32[0];
			if ( (int)distance > 0 )
			{
				XMStoreFloat3( &mVelocityDirection, XMVector3Normalize(distanceVector) );

				if ( mTargetPoint )
				{
					//mLastPoint->GetBlock().SetIsOwnedByInsect( false );
					mTargetPoint->GetBlock().SetIsOwnedByInsect(true);
				}
			}
			else
			{
				mAnimatedModel->SetAnimation("AglaisUrticaeMoving");
				//std::unique_ptr<Point> newTarget = std::unique_ptr<Point>( new Point( *mGame, *mCamera, mDirectionalLightSource, mTargetPoint->GetBlock() ) );
				//mTargetPoint = newTarget.get();
				//mTargetPoint->SetPosition( CreateRandomPosition() );

				//mActionState = bind( &AglaisUrticae::Sit, this, _1 );
				mActionState = nullptr;
				SetEnabled(false);
			}


			Move( gameTime );
			SmoothRotation( gameTime );
		}

	}

}