#include "GameException.h"
#include "WICTextureLoader.h"
#include "LevelsCommon.h"
#include "RenderStateHelper.h"
#include <sstream>
#include <iomanip>
#include <SpriteBatch.h>
#include <SpriteFont.h>
#include "Game.h"
#include "Utility.h"
#include "ColorHelper.h"
#include "Keyboard.h"
#include "Camera.h"
#include "ThirdPersonCameraController.h"
#include "FirstPersonCameraController.h"
#include "CameraTarget.h"
#include "DirectionalLight.h"
#include "ProxyModel.h"
#include "Skybox.h"
#include "LadybugPlayable.h"
#include "GameGrid.h"
#include "LawnBlockPath.h"


#include "Level2.h"
#include "StartScene.h"

#include "Block.h"
#include "Point.h"

#include "Lawn.h"
#include "ParticleEffect.h"
#include "AudioPlayer.h"

//for std::bind stuff. Is needed to bind functions with parameters.
using namespace std::placeholders;

namespace Rendering
{
	RTTI_DEFINITIONS( LevelsCommon )

	const XMVECTORF32 Level1::BackgroundColor = ColorHelper::CornflowerBlue;
	const float Level1::LightModulationRate = UCHAR_MAX;
	const XMFLOAT2 Level1::LightRotationRate = XMFLOAT2( XM_2PI, XM_2PI );

	LevelsCommon::LevelsCommon( Game& game, SceneManager& sceneManager )
		:
		Scene( game, sceneManager ),
		mSpriteBatch( nullptr ),
		mSpriteFont( nullptr ),

		mKeyboard( nullptr ),
		mCamera( nullptr ),
		mCameraController( nullptr ),
		mCameraTarget( nullptr ),

		mDirectionalLight( nullptr ),
		mRenderStateHelper( nullptr ),
		mSkybox( nullptr ),

		mLadybugPlayable( nullptr ),
		mNumberOfGameGrids( 1 ),
		mDistancebetweenGrids( 80.0f ),
		mCurrentScenarioTerm( nullptr ),

		mFlyingInsectsList( nullptr ),

		bGameIsPaused( false ),
		bGridIsPlaying( false ),
		mMenuActiveOption( Menu::ReturnToGame ),

		///To spawn npc insects
		mTimerForFlyingInsectsSpawning( 0.0f ),
		mSpawningDelay( 0.0f ),
		bIsItSpawning( false ),
		
		//mLawn( nullptr ),
		
		//Regular menu
		mMenuBgTexture( nullptr ),
		mMenuTextureRect( { 0, 0, 30, 40 } ),
		mMenuPosition( XMFLOAT2( 0.0f, 0.0f ) ),
		mMenuBgWidth( 30 ),
		mMenuBgHeight( 40 ),
		mMenuBgScale( 1.0f ),

		mParticleEffect( nullptr ),
		mAudioPlayer( nullptr ),
		mLevelNum( 0 )
	{
	}
	LevelsCommon::~LevelsCommon()
	{
		DeleteObject( mAudioPlayer );
		DeleteObject( mParticleEffect );
		for ( auto& it : mLawns )
		{
			DeleteObject( it );
		}

		for( auto& it : mFlyingInsects )
		{
			DeleteObject( it );
		}

		for( auto& it : mLawnBlockPath )
		{
			DeleteObject( it );
		}
		for( auto& it : mGameGrids )
		{
			DeleteObject( it );
		}
		DeleteObject( mFlyingInsectsList );
		DeleteObject( mLadybugPlayable )
		DeleteObject( mSkybox );
		DeleteObject( mRenderStateHelper );
		DeleteObject( mDirectionalLight );
		DeleteObject( mCameraTarget );
		DeleteObject( mCameraController );
		DeleteObject( mCamera );
		DeleteObject( mSpriteFont );
		DeleteObject( mSpriteBatch );
	}

	void LevelsCommon::Initialize()
	{
		SetCurrentDirectory( Utility::ExecutableDirectory().c_str() );

		mSpriteBatch = new SpriteBatch( mGame->Direct3DDeviceContext() );
		mSpriteFont = new SpriteFont( mGame->Direct3DDevice(), L"Content\\Fonts\\Berlin_Sans_Fb_Demi_24_Bold.spritefont" );

		std::wstring menuBgTextureName = L"Content\\Textures\\menuBgTex.png";
		HRESULT hr = DirectX::CreateWICTextureFromFile( mGame->Direct3DDevice(), mGame->Direct3DDeviceContext(), menuBgTextureName.c_str(), nullptr, &mMenuBgTexture );
		if( FAILED( hr ) )
		{
			throw GameException( "CreateWICTextureFromFile() failed.", hr );
		}

		mKeyboard = (Keyboard*)mGame->Services().GetService( Keyboard::TypeIdClass() );

		mCamera = new Camera( *mGame );
		mCamera->Initialize();

		mCameraController = new ThirdPersonCameraController( *mGame, *mCamera );
		mCameraController->Initialize();
		mCameraController->SetDistance( 160.0f );

		//mCameraController = new FirstPersonCameraController( *mGame, *mCamera );
		//mCameraController->Initialize();

		mAudioPlayer = new AudioPlayer();

		mCameraTarget = new CameraTarget( *mGame, *mCamera );
		mCameraTarget->Initialize();

		mDirectionalLight = new DirectionalLight( *mGame );
		mDirectionalLight->Initialize();
		mDirectionalLight->SetRotation( XMFLOAT3( -15.0f, 0.0f, 0.0f ) );

		mSkybox = new Skybox( *mGame, *mCamera, L"Content\\Textures\\TropicalSunnyDay.dds", 100.0f );
		mSkybox->Initialize();

		//! We create gamegrid first
		//For optimization purposes scene draws one current game grid and three (prev, current and next) of lawn grid.
		for( int it = 0; it < mNumberOfGameGrids; ++it )
		{
			mGameGrids.push_back( new GameGrid( *mGame, *mCamera, *mDirectionalLight, GameGridSize::size3x3 ) );
			mGameGrids.back()->SetPosition( it * mDistancebetweenGrids, 0.0f, 0.0f );
			mGameGrids.back()->Initialize();
			mGameGrids.back()->pAudioPlayer = mAudioPlayer;
		}

		mGameGridIterator = mGameGrids.begin();

		for( int it = -1; it < mNumberOfGameGrids + 1; ++it )
		{
			mLawnBlockPath.push_back( new LawnBlockPath( *mGame, *mCamera, *mDirectionalLight, mLevelNum ) );
			mLawnBlockPath.back()->SetPosition( it * mDistancebetweenGrids, -80.0f, 0.0f );
			mLawnBlockPath.back()->Initialize();

			//Next we create chamomile blocks in Lawn grid. Positions of chamomiles defines as start and finish blocks in gamegrids.
			if( (it > -1) && (it < mNumberOfGameGrids) )
			{
				{
					XMVECTOR chamPosVector = (*mGameGridIterator)->GetStartBlock()->PositionVector() + XMLoadFloat3( &XMFLOAT3( 0.0f, 2.0f, 0.0f ) );
					XMFLOAT3 chamPos;
					XMStoreFloat3( &chamPos, chamPosVector );


					mLawnBlockPath.back()->CreateChamomileBlock( chamPos );
				}
				if( it == (mNumberOfGameGrids - 1) )
				{
					{
						XMVECTOR chamPosVector = (*mGameGridIterator)->GetFinishBlock()->PositionVector() + XMLoadFloat3( &XMFLOAT3( 0.0f, 2.0f, 0.0f ) );
						XMFLOAT3 chamPos;
						XMStoreFloat3( &chamPos, chamPosVector );

						mLawnBlockPath.back()->CreateChamomileBlock( chamPos );
					}
				}
				++mGameGridIterator;
			}
		}

		mGameGridIterator = mGameGrids.begin();

		//Here we start to keep enabled to draw previous, current and next lawn grid
		mLawnBlockPathIterator = mLawnBlockPath.begin();
		(*mLawnBlockPathIterator)->SetEnabled( true );
		mLawnBlockPathIterator++;
		(*mLawnBlockPathIterator)->SetEnabled( true );
		mLawnBlockPathIterator++;
		(*mLawnBlockPathIterator)->SetEnabled( true );
		mLawnBlockPathIterator--;
		////

		mCameraController->SetObservableActor( mCameraTarget );

		mLadybugPlayable = new LadybugPlayable( *mGame, *mCamera, *mDirectionalLight );

		mLadybugPlayable->Initialize();
		mLadybugPlayable->SetBlockPath( *mGameGridIterator );

		mRenderStateHelper = new RenderStateHelper( *mGame );

		mLawns.push_back( new Lawn( *mGame, *mCamera, *mDirectionalLight ) );
		mLawns.back()->SetPosition( 400.0f, -80.0f, 0.0f );
		mLawns.back()->SetScale( XMFLOAT3( 160.0f, 1.0f, 160.0f ) );
		mLawns.back()->Initialize();

		mParticleEffect = new ParticleEffect( *mGame, *mCamera );
		mParticleEffect->Initialize();
	}
	void LevelsCommon::Update( const GameTime & gameTime )
	{
		if( !bGameIsPaused )
		{
			mCamera->Update( gameTime );
			mCameraController->Update( gameTime );
			//mDirectionalLight->Update( gameTime );
			mSkybox->Update( gameTime );

			mCameraTarget->Update( gameTime );
			mLadybugPlayable->Update( gameTime );
			
			for( auto& it : mGameGrids )
			{
				it->Update( gameTime );
			}

			for( auto& it : mLawnBlockPath )
			{
				it->Update( gameTime );
			}

			//Update npc flying insects, if any.
			//Also if any of them set enabled to false (meaning they walking scenario is ended,
			//then delete it.
			for( auto& it = mFlyingInsects.begin(); it != mFlyingInsects.end(); ++it )
			{
				(*it)->Update( gameTime );
				if( !(*it)->Enabled() )
				{
					it = mFlyingInsects.erase( it );
				}
			}
		}
		else
		{
			if( mMenuBgScale <= 10.0f )
			{
				//Increase scale
				mMenuBgScale += 25.0f * gameTime.ElapsedGameTime();
				//Change bg position
				mMenuPosition = XMFLOAT2( (mGame->ScreenWidth() / 2) - (mMenuBgWidth * mMenuBgScale) / 2, (mGame->ScreenHeight() / 2) - (mMenuBgHeight * mMenuBgScale) / 2 );
			}
		}

		if( mCurrentScenarioTerm )
		{
			mCurrentScenarioTerm( gameTime );
		}

		if( mParticleEffect )
		{
			mParticleEffect->Update( gameTime );
		}

		for ( auto& it :mLawns )
		{
			it->Update( gameTime );
		}

	}
	void LevelsCommon::Draw( const GameTime & gameTime )
	{
		mSkybox->Draw( gameTime );

		mLadybugPlayable->Draw( gameTime );

		for ( auto& it : mLawns )
		{
			it->Draw( gameTime );
		}

		for( auto& it : mGameGrids )
		{
			it->Draw( gameTime );
		}

		for( auto& it : mLawnBlockPath )
		{
			it->Draw( gameTime );
		}

		for( auto& it : mFlyingInsects )
		{
			it->Draw( gameTime );
		}

		mParticleEffect->Draw( gameTime );

		mRenderStateHelper->SaveAll();

		std::wostringstream levelName;
		levelName << L"Level " << mLevelNum;
		mSpriteBatch->Begin();
		mSpriteFont->DrawString( mSpriteBatch, levelName.str().c_str(), XMFLOAT2( (int)(mGame->ScreenWidth() - 140), 20 ), XMVECTORF32( /*255 106 0*/{1.0f, 0.416f, 0.0f, 1.0f} ) );
		mSpriteBatch->End();
		
		mRenderStateHelper->RestoreAll();

		if( bGameIsPaused )
		{
			mRenderStateHelper->SaveAll();

			///Draw menu background
			mSpriteBatch->Begin();
			mSpriteBatch->Draw( mMenuBgTexture, mMenuPosition, &mMenuTextureRect, Colors::White, 0.0f, XMFLOAT2( 0.0f, 0.0f ), XMFLOAT2( mMenuBgScale, mMenuBgScale ), SpriteEffects::SpriteEffects_None, 0 );
			mSpriteBatch->End();

			if( mMenuBgScale > 7.0f )
			{
				//Lambda return color for text according to active option
				auto ChoseColorFor = [=]( Menu menuActiveOption )
				{
					if( mMenuActiveOption & menuActiveOption )
					{
						return  XMVECTORF32( /*255 106 0*/{ 1.0f, 0.416f, 0.0f, 1.0f } );
					}
					else
					{
						return  XMVECTORF32( /*23 255 23*/{ 0.09f, 1.0f, 0.09f, 1.0f } );
					}
				};

				mSpriteBatch->Begin();
				mSpriteFont->DrawString( mSpriteBatch, L"Pause", XMFLOAT2( (int)((mGame->ScreenWidth() / 2.0f) - ((16.6f * 5.0f)/ 2.0f)), 150 ), XMVECTORF32( /*255 106 0*/{ 0.9f, 0.9f, 0.9f, 1.0f } ) );
				mSpriteFont->DrawString( mSpriteBatch, L"Return to game", XMFLOAT2( (int)((mGame->ScreenWidth() / 2.0f) - ((16.6f * 14.0f) / 2.0f)), 240 ), ChoseColorFor( Menu::ReturnToGame ) );
				mSpriteFont->DrawString( mSpriteBatch, L"   Start level again", XMFLOAT2( (int)((mGame->ScreenWidth() / 2.0f) - ((16.6f * 17.0f) / 2.0f)), 310 ), ChoseColorFor( Menu::StartLevel ) );
				mSpriteFont->DrawString( mSpriteBatch, L"Start game", XMFLOAT2( (int)((mGame->ScreenWidth() / 2.0f) - ((16.6f * 10.0f) / 2.0f)), 380 ), ChoseColorFor( Menu::StartLevel1 ) );
				mSpriteFont->DrawString( mSpriteBatch, L" Exit to Windows", XMFLOAT2( (int)((mGame->ScreenWidth() / 2.0f) - ((16.6f * 15.0f) / 2.0f)), 450 ), ChoseColorFor( Menu::ExitMenu ) );
				mSpriteBatch->End();

			}
		}

		mRenderStateHelper->RestoreAll();
	}

	void LevelsCommon::SwitchScenarioToTerm( const std::string name )
	{
		mCurrentScenarioTerm = mScenarioTerms[name];
	}
	void LevelsCommon::UpdateInsectSpawner( const GameTime & gameTime )
	{
		//increase timer
		mTimerForFlyingInsectsSpawning += gameTime.ElapsedGameTime();
		//If timer exhausted - create new insect
		if( bIsItSpawning )
		{
			if( mTimerForFlyingInsectsSpawning > mSpawningDelay )
			{
				CreateInsect();
			}
		}
	}
	void LevelsCommon::CreateInsect()
	{
		if( mFlyingInsects.size() < 4 )
		{
			//Create new insect

			mFlyingInsects.push_back( mFlyingInsectsList->GetRandomNode()() );
			mFlyingInsects.back()->Initialize();
			mFlyingInsects.back()->SetBlockPath( *mLawnBlockPathIterator );
			mFlyingInsects.back()->PlayWalkingScenario();

			////Create new random delay and reset timer
			std::random_device randomDevice;
			std::default_random_engine randomGenerator( randomDevice() );
			std::uniform_int_distribution<int> secDistribution( 30, 60 );
			mSpawningDelay = secDistribution( randomGenerator );

			mTimerForFlyingInsectsSpawning = 0.0f;
		}
	}

}