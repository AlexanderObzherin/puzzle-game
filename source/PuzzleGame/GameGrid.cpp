#include "GameGrid.h"
#include "Block.h"

#include "Keyboard.h"
#include "Game.h"
#include "GameException.h"
#include "GameTime.h"
#include "VectorHelper.h"
#include <random>
#include "Scene.h"
#include "Level1.h"
#include "AudioPlayer.h"

namespace Rendering
{
	RTTI_DEFINITIONS( GameGrid )

		int GameGrid::mStartFinishRow;

		GameGrid::GameGrid( Game& game, Camera& camera, const DirectionalLight& directionalLightSource, GameGridSize gridSize )
		:
		BlockPath( game, camera, directionalLightSource ),
		mKeyboard( nullptr ),
		mGridSize( gridSize ),
		mRows( 0 ),
		mColumns( 0 ),
		mGridScale( 20.0f ),
		mBlockIterator( XMINT2( 0, 0 ) ),
		pAudioPlayer( nullptr )
	{
		SetEnabled( false );
		switch( mGridSize )
		{
			case GameGridSize::size3x3 : 
			{
				mRows = 3;
				mColumns = 3;
			};
			break;
			case GameGridSize::size4x4 :
			{
				mRows = 4;
				mColumns = 4;
			};
			break;
		}
	}

	GameGrid::~GameGrid()
	{
		mKeyboard = nullptr;
		pAudioPlayer = nullptr;
	}
	const Keyboard & GameGrid::GetKeyboard() const
	{
		return *mKeyboard;
	}
	void GameGrid::Initialize()
	{
		Actor::Initialize();

		mKeyboard = (Keyboard*)mGame->Services().GetService( Keyboard::TypeIdClass() );

		CreateGrid();
	}
	void GameGrid::Update( const GameTime& gameTime )
	{
		BlockPath::Update( gameTime );

		if( Enabled() )
		{
			SwitchActiveBlock();
			PullToPivot( gameTime );
			MoveBlocks( gameTime );
		}

		
	}
	void GameGrid::Draw( const GameTime& gameTime )
	{
		if( Enabled() )
		{
			BlockPath::Draw( gameTime );
		}
		
	}
	void GameGrid::SetDimensions( int rows, int columns )
	{
		mRows = rows;
		mColumns = columns;
	}
	XMFLOAT3 GameGrid::GetCenter() const
	{
		return XMFLOAT3( Position().x + (mColumns * mGridScale)/2 - (mGridScale/2), Position().y, Position().z + (mRows * mGridScale)/2 - (mGridScale/2) );
	}
	Block * GameGrid::GetStartBlock()
	{
		return GetBlockAtPos( mStartBlockCoord );
	}
	Block * GameGrid::GetFinishBlock()
	{
		return GetBlockAtPos( mFinishBlockCoord );
	}
	bool GameGrid::IsOccupied( XMINT2 pos )
	{
		for( auto& it : mBlocks )
		{
			if( it->GetGridPos().x == pos.x && it->GetGridPos().y == pos.y )
			{
				return true;
			}
		}

		return false;
	}
	XMINT2 GameGrid::FindFreeSpace()
	{
		//If mBlocks is empty then return first pos with ( 0, 0 )
		if( mBlocks.empty() )
		{
			return XMINT2( 0, 0 );
		}

		for( int y = 0; y < mRows; ++y )
		{
			for( int x = 0; x < mColumns; ++x )
			{
				if( !IsOccupied( XMINT2( x, y ) ) )
				{
					return XMINT2( x, y );
				}
			}
		}

		throw GameException( "All positions in defined range is occupied" );
		
	}
	XMINT2 GameGrid::GenerateNewPosition()
	{
		int x;
		int y;

		do
		{
			//Generate new x and y
			//Check if there is any block with grid coord x and y
			std::random_device randomDevice;
			std::default_random_engine randomGenerator( randomDevice() );
			std::uniform_int_distribution<int> distanceDistributionX( 0, mRows - 1 );
			std::uniform_int_distribution<int> distanceDistributionY( 0, mColumns - 1 );

			x = distanceDistributionX( randomGenerator );
			y = distanceDistributionY( randomGenerator );

			if( mBlocks.empty() )
			{
				return XMINT2( x, y );
			}


		} while( IsOccupied( XMINT2( x, y ) ) );

		return XMINT2( x, y );

	}
	void GameGrid::CreateGrid()
	{
		
		CreateStartBlock();
		//Algorythm of random positioning of blocks needed to be improved!!!
		//creation of grid with GanerateNewPosition() doesn't populate all positions
		//So now only first two blocks placing randomly, and the rest acording to free positions

		//Create BlockA 1 ex
		auto pos = GenerateNewPosition();
		mBlocks.push_back( new BlockA( *mGame, *mCamera, mDirectionalLightSource ) );
		mBlocks.back()->Initialize();
		mBlocks.back()->SetGridPos( pos );
		mBlocks.back()->AttachTransitionTo( this, XMFLOAT3( pos.x * mGridScale, 0.0f, pos.y * mGridScale ) );

		////Create BlockB 1 ex
		pos = GenerateNewPosition();
		mBlocks.push_back( new BlockB( *mGame, *mCamera, mDirectionalLightSource ) );
		mBlocks.back()->Initialize();
		mBlocks.back()->SetGridPos( pos );
		mBlocks.back()->AttachTransitionTo( this, XMFLOAT3( pos.x * mGridScale, 0.0f, pos.y * mGridScale ) );

		//Create BlockC 1 ex
		pos = FindFreeSpace();
		mBlocks.push_back( new BlockC( *mGame, *mCamera, mDirectionalLightSource ) );
		mBlocks.back()->Initialize();
		mBlocks.back()->SetGridPos( pos );
		mBlocks.back()->AttachTransitionTo( this, XMFLOAT3( pos.x * mGridScale, 0.0f, pos.y * mGridScale ) );

		//Create BlockD 1 ex
		pos = FindFreeSpace();
		mBlocks.push_back( new BlockD( *mGame, *mCamera, mDirectionalLightSource ) );
		mBlocks.back()->Initialize();
		mBlocks.back()->SetGridPos( pos );
		mBlocks.back()->AttachTransitionTo( this, XMFLOAT3( pos.x * mGridScale, 0.0f, pos.y * mGridScale ) );

		//Create BlockE 2 ex
		pos = FindFreeSpace();
		mBlocks.push_back( new BlockE( *mGame, *mCamera, mDirectionalLightSource ) );
		mBlocks.back()->Initialize();
		mBlocks.back()->SetGridPos( pos );
		mBlocks.back()->AttachTransitionTo( this, XMFLOAT3( pos.x * mGridScale, 0.0f, pos.y * mGridScale ) );

		pos = FindFreeSpace();
		mBlocks.push_back( new BlockE( *mGame, *mCamera, mDirectionalLightSource ) );
		mBlocks.back()->Initialize();
		mBlocks.back()->SetGridPos( pos );
		mBlocks.back()->AttachTransitionTo( this, XMFLOAT3( pos.x * mGridScale, 0.0f, pos.y * mGridScale ) );

		//Create BlockF 2 ex
		pos = FindFreeSpace();
		mBlocks.push_back( new BlockF( *mGame, *mCamera, mDirectionalLightSource ) );
		mBlocks.back()->Initialize();
		mBlocks.back()->SetGridPos( pos );
		mBlocks.back()->AttachTransitionTo( this, XMFLOAT3( pos.x * mGridScale, 0.0f, pos.y * mGridScale ) );

		pos = FindFreeSpace();
		mBlocks.push_back( new BlockF( *mGame, *mCamera, mDirectionalLightSource ) );
		mBlocks.back()->Initialize();
		mBlocks.back()->SetGridPos( pos );
		mBlocks.back()->AttachTransitionTo( this, XMFLOAT3( pos.x * mGridScale, 0.0f, pos.y * mGridScale ) );

		CreateFinishBlock();
	}
	void GameGrid::CreateStartBlock()
	{
		mBlocks.push_back( new BlockG( *mGame, *mCamera, mDirectionalLightSource ) );
		mBlocks.back()->Initialize();
		mBlocks.back()->SetGridPos( XMINT2( -1, mStartFinishRow ) );
		mStartBlockCoord = XMINT2( -1, mStartFinishRow );
		mBlocks.back()->SetGridPos( mStartBlockCoord );
		mBlocks.back()->AttachTransitionTo( this, XMFLOAT3( - 1 * mGridScale, 0.0f, mStartFinishRow * mGridScale ) );
	}
	void GameGrid::CreateFinishBlock()
	{
		std::random_device randomDevice;
		std::default_random_engine randomGenerator( randomDevice() );
		std::uniform_int_distribution<int> distanceDistributionY( 0, mRows - 1 );

		mStartFinishRow = distanceDistributionY( randomGenerator );

		mBlocks.push_back( new BlockG( *mGame, *mCamera, mDirectionalLightSource ) );
		mBlocks.back()->Initialize();
		mBlocks.back()->SetGridPos( XMINT2( mColumns, mStartFinishRow ) );
		mFinishBlockCoord = XMINT2( mColumns, mStartFinishRow );
		mBlocks.back()->AttachTransitionTo( this, XMFLOAT3( mColumns * mGridScale, 0.0f, mStartFinishRow * mGridScale ) );
	}
	void GameGrid::SwitchActiveBlock()
	{
		XMINT2 pos = mBlockIterator;

		if( mKeyboard != nullptr )
		{
			if( mKeyboard->WasKeyPressedThisFrame( DIK_W ) )
			{
				//Up
				do
				{
					if( pos.y > 0 )
					{
						pos.y--;
					}
					if( pos.y == 0 && !IsOccupied( pos ) )
					{
						return;
					}

				} while( !IsOccupied( pos ) );
				if( IsOccupied( pos ) )
				{
					GetBlockAtPos( mBlockIterator )->SetActive( false );
					GetBlockAtPos( pos )->SetActive( true );
					mBlockIterator = pos;

					pAudioPlayer->PlaySample( "Content\\Audio\\StickClicks.mp3" );
				}

			}
			if( mKeyboard->WasKeyPressedThisFrame( DIK_S ) )
			{
				//Down
				do
				{
					if( pos.y < mRows - 1 )
					{
						pos.y++;
					}
					if( pos.y == mRows - 1 && !IsOccupied( pos ) )
					{
						return;
					}
				} while( !IsOccupied( pos ) );
				if( IsOccupied( pos ) )
				{
					GetBlockAtPos( mBlockIterator )->SetActive( false );
					GetBlockAtPos( pos )->SetActive( true );
					mBlockIterator = pos;

					pAudioPlayer->PlaySample( "Content\\Audio\\StickClicks.mp3" );
				}
			}

			if( mKeyboard->WasKeyPressedThisFrame( DIK_A ) )
			{
				//Left
				do
				{
					if( pos.x > 0 )
					{
						pos.x--;
					}
					if( pos.x == 0 && !IsOccupied( pos ) )
					{
						return;
					}
				} while( !IsOccupied( pos ) );
				if( IsOccupied( pos ) )
				{
					GetBlockAtPos( mBlockIterator )->SetActive( false );
					GetBlockAtPos( pos )->SetActive( true );
					mBlockIterator = pos;

					pAudioPlayer->PlaySample( "Content\\Audio\\StickClicks.mp3" );
				}

			}
			if( mKeyboard->WasKeyPressedThisFrame( DIK_D ) )
			{
				//Right
				do
				{
					if( pos.x < mColumns - 1 )
					{
						pos.x++;
					}
					if( pos.x == mColumns - 1 && !IsOccupied( pos ) )
					{
						return;
					}
				} while( !IsOccupied( pos ) );
				if( IsOccupied( pos ) )
				{
					GetBlockAtPos( mBlockIterator )->SetActive( false );
					GetBlockAtPos( pos )->SetActive( true );
					mBlockIterator = pos;

					pAudioPlayer->PlaySample( "Content\\Audio\\StickClicks.mp3" );
				}
			}

		}

	}
	Block* GameGrid::GetBlockAtPos( XMINT2 pos )
	{
		for( auto& it : mBlocks )
		{
			if( it->GetGridPos().x == pos.x && it->GetGridPos().y == pos.y )
			{
				return it;
			}
		}
			return nullptr;
	}
	Block * GameGrid::GetActiveBlock()
	{
		for( auto& it : mBlocks )
		{
			if( it->GetIsActive() )
			{
				return it;
			}
		}
		return nullptr;
	}
	void GameGrid::MoveBlocks( const GameTime& gameTime )
	{
		if( mKeyboard != nullptr && GetActiveBlock() && !GetActiveBlock()->GetIsOwnedByInsect() )
		{
			if( mKeyboard->IsKeyDown( DIK_UPARROW ) )
			{
				XMINT2 targetPos = GetActiveBlock()->GetGridPos();
				targetPos.y--;
				if( targetPos.y >= 0 ) //check if new pos inside the grid
				{
					if( !IsOccupied( targetPos ) )
					{
						XMVECTOR velocityDirection = XMLoadFloat3( &Vector3Helper::Forward );
						velocityDirection *= 13.0f;
						XMFLOAT3 velocity;
						XMStoreFloat3( &velocity, velocityDirection );
						GetActiveBlock()->Move( velocity, gameTime );
						
						if( !(pAudioPlayer->IsSamplePlaying( "Content\\Audio\\Cabasa.mp3" )) )
						{
							pAudioPlayer->PlaySample( "Content\\Audio\\Cabasa.mp3" );
						}

						if( GetActiveBlock()->GetDistanceFloat() > 20.0f )
						{
							GetActiveBlock()->SetGridPos( targetPos );
							mBlockIterator = targetPos;

							pAudioPlayer->PlaySample( "Content\\Audio\\Glockenspiel.mp3" );
						}
					}
				}
			}
			if( mKeyboard->IsKeyDown( DIK_DOWNARROW ) )
			{
				XMINT2 targetPos = GetActiveBlock()->GetGridPos();
				targetPos.y++;
				if( targetPos.y <= mRows - 1 )
				{
					if( !IsOccupied( targetPos ) )
					{
						XMVECTOR velocityDirection = XMLoadFloat3( &Vector3Helper::Backward );
						velocityDirection *= 13.0f;
						XMFLOAT3 velocity;
						XMStoreFloat3( &velocity, velocityDirection );
						GetActiveBlock()->Move( velocity, gameTime );

						if( !(pAudioPlayer->IsSamplePlaying( "Content\\Audio\\Cabasa.mp3" )) )
						{
							pAudioPlayer->PlaySample( "Content\\Audio\\Cabasa.mp3" );
						}

						if( GetActiveBlock()->GetDistanceFloat() > 20.0f )
						{
							GetActiveBlock()->SetGridPos( targetPos );
							mBlockIterator = targetPos;

							pAudioPlayer->PlaySample( "Content\\Audio\\Glockenspiel.mp3" );
						}
					}
				}
			}
			if( mKeyboard->IsKeyDown( DIK_LEFTARROW ) )
			{
				XMINT2 targetPos = GetActiveBlock()->GetGridPos();
				targetPos.x--;
				if( targetPos.x >= 0 )
				{
					if( !IsOccupied( targetPos ) )
					{
						XMVECTOR velocityDirection = XMLoadFloat3( &Vector3Helper::Left );
						velocityDirection *= 13.0f;
						XMFLOAT3 velocity;
						XMStoreFloat3( &velocity, velocityDirection );
						GetActiveBlock()->Move( velocity, gameTime );

						if( !(pAudioPlayer->IsSamplePlaying( "Content\\Audio\\Cabasa.mp3" )) )
						{
							pAudioPlayer->PlaySample( "Content\\Audio\\Cabasa.mp3" );
						}

						if( GetActiveBlock()->GetDistanceFloat() > 20.0f )
						{
							GetActiveBlock()->SetGridPos( targetPos );
							mBlockIterator = targetPos;

							pAudioPlayer->PlaySample( "Content\\Audio\\Glockenspiel.mp3" );
						}
					}
				}
			}
			if( mKeyboard->IsKeyDown( DIK_RIGHTARROW ) )
			{
				XMINT2 targetPos = GetActiveBlock()->GetGridPos();
				targetPos.x++;
				if( targetPos.x <= mColumns - 1 )
				{
					if( !IsOccupied( targetPos ) )
					{
						XMVECTOR velocityDirection = XMLoadFloat3( &Vector3Helper::Right );
						velocityDirection *= 13.0f;
						XMFLOAT3 velocity;
						XMStoreFloat3( &velocity, velocityDirection );
						GetActiveBlock()->Move( velocity, gameTime );

						if( !(pAudioPlayer->IsSamplePlaying( "Content\\Audio\\Cabasa.mp3" )) )
						{
							pAudioPlayer->PlaySample( "Content\\Audio\\Cabasa.mp3" );
						}

						if( GetActiveBlock()->GetDistanceFloat() > 20.0f )
						{
							GetActiveBlock()->SetGridPos( targetPos );
							mBlockIterator = targetPos;

							pAudioPlayer->PlaySample( "Content\\Audio\\Glockenspiel.mp3" );
						}
					}
				}
			}
		}
	}
	void GameGrid::PullToPivot( const GameTime & gameTime )
	{
		for( auto& block : mBlocks )
		{
			XMVECTOR pivotPos = XMLoadFloat3( &XMFLOAT3( block->GetGridPos().x * mGridScale, 0.0f, block->GetGridPos().y * mGridScale ) );
			XMVECTOR distance = (pivotPos + PositionVector()) - block->PositionVector();
			XMVECTOR distanceSq = XMVector3Dot( distance, distance );
			float distanceFloat = sqrtf( distanceSq.m128_f32[0] + distanceSq.m128_f32[1] + distanceSq.m128_f32[2] );
			block->SetDistanceFloat( distanceFloat );
			//if distance more than 0 we need to pull block back to pivot
			if( distanceFloat > 0.0f )
			{
				XMFLOAT3 velocityDirection;
				XMStoreFloat3( &velocityDirection, distance );
				block->Move( velocityDirection, gameTime );
			}
		}
	}

}