#include "AraneusAngulatus.h"

#include "AnimatedModel.h"

namespace Rendering
{
	RTTI_DEFINITIONS( AraneusAngulatus )

		AraneusAngulatus::AraneusAngulatus( Game& game, Camera& camera, const DirectionalLight& directionalLightSource )
		:
		Insect( game, camera, directionalLightSource ),
		bIsRunning( false )

	{}
	AraneusAngulatus::~AraneusAngulatus()
	{
	}

	void AraneusAngulatus::Initialize()
	{
		Insect::Initialize();

		mAnimatedModel = std::unique_ptr<AnimatedModel>( new AnimatedModel( *this, mDirectionalLightSource ) );
		mAnimatedModel->SetModelName( "Content\\Models\\AraneusAngulatus\\AraneusAngulatus.dae" );
		mAnimatedModel->SetColorTexture( L"Content\\Models\\AraneusAngulatus\\AraneusAngulatusColorTexture.png" );
		mAnimatedModel->SetNormalMapTexture( L"Content\\Models\\AraneusAngulatus\\AraneusAngulatusNormalMapTexture.png" );
		mAnimatedModel->Initialize();
		mAnimatedModel->DrawPhongWithNormalMap();

		mAnimatedModel->AddAnimation( "AraneusAngulatusMoving", 0.0f, 0.9f );
		mAnimatedModel->AddAnimation( "AraneusAngulatusAttacking", 1.7f, 5.0f );
		mAnimatedModel->SetAnimation( "AraneusAngulatusMoving" );

		SetScale( XMFLOAT3( 0.7f, 0.7f, 0.7f ) );

		mVelocityFactor = 8.0f;
		mAngularVelocity = 140.0f;
	}
	void AraneusAngulatus::Update( const GameTime & gameTime )
	{
		Insect::Update( gameTime );
		if( !bIsRunning || !mCurrentBlockPath )
		{
			Stop( gameTime );
		}
		else
		{
			Run( gameTime );
		}

	}
	void AraneusAngulatus::Draw( const GameTime & gameTime )
	{
		Insect::Draw( gameTime );

	}

	void AraneusAngulatus::Run( const GameTime & gameTime )
	{
		mAnimatedModel->SetAnimation( "AraneusAngulatusMoving" );
		WalkBetweenPoints();
		Move( gameTime );
		SmoothRotation( gameTime );
	}
	void AraneusAngulatus::Stop( const GameTime & gameTime )
	{
		mAnimatedModel->SetAnimation( "AraneusAngulatusAttacking" );
	}
	bool AraneusAngulatus::GetIsRunning() const
	{
		return bIsRunning;
	}


}