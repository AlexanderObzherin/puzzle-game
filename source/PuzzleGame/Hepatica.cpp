#include "Hepatica.h"
#include "StaticModel.h"

namespace Rendering
{
	RTTI_DEFINITIONS( Hepatica )

		Hepatica::Hepatica( Game& game, Camera& camera, const DirectionalLight& directionalLightSource )
		:
		Actor( game, camera ),
		mDirectionalLightSource( directionalLightSource ),
		mShape( nullptr )
	{
	}
	Hepatica::~Hepatica()
	{
		DeleteObject( mShape );
	}

	void Hepatica::Initialize()
	{
		SetScale( XMFLOAT3( 3.0f, 3.0f, 3.0f ) );
		Actor::Initialize();

		mShape = new StaticModel( *this, mDirectionalLightSource );
		mShape->SetModel( "Content\\Models\\Hepatica\\Hepatica.obj" );
		mShape->SetColorTexture( L"Content\\Models\\Hepatica\\HepaticaColorTexture.png" );

		mShape->Initialize();
		mShape->DrawAmbientDiffuse();
	}
	void Hepatica::Update( const GameTime& gameTime )
	{
		Actor::Update( gameTime );
	}
	void Hepatica::Draw( const GameTime& gameTime )
	{
		mShape->Draw( gameTime );
	}

}