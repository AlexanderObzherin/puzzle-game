#include "CameraTarget.h"
#include "VectorHelper.h"
#include "GameTime.h"
namespace Rendering
{
	RTTI_DEFINITIONS( CameraTarget );

	CameraTarget::CameraTarget( Game& game, Camera& camera )
		:
		Actor( game, camera ),
		mTargetPosition( Vector3Helper::Zero ),
		mVelocityFactor( 1.0f )
	{}

		CameraTarget::~CameraTarget()
	{
	}

	void CameraTarget::Initialize()
	{
		Actor::Initialize();
	}
	void CameraTarget::Update( const GameTime & gameTime )
	{
		Actor::Update( gameTime );
		SmoothFollowing( gameTime );
	}
	void CameraTarget::Draw( const GameTime & gameTime )
	{
		Actor::Draw( gameTime );
	}

	bool CameraTarget::IsMoving()
	{
		XMVECTOR targetPosition = XMLoadFloat3( &mTargetPosition );
		float distance = XMVector3Length( targetPosition - PositionVector() ).m128_f32[0];
		if( (int)distance != 0 )
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	float CameraTarget::GetDistanceBetweenTarget()
	{
		XMVECTOR targetPosition = XMLoadFloat3( &mTargetPosition );
		return XMVector3Length( targetPosition - PositionVector() ).m128_f32[0];
	}
	void CameraTarget::SetNewPosition( XMFLOAT3 newPosition )
	{
		mTargetPosition = newPosition;
	}
	void CameraTarget::SmoothFollowing( const GameTime& gameTime )
	{
		XMVECTOR distance = XMLoadFloat3( &mTargetPosition ) - PositionVector();
		XMVECTOR distanceSq = XMVector3Dot( distance, distance );
		float distanceFloat = sqrtf( distanceSq.m128_f32[0] + distanceSq.m128_f32[1] + distanceSq.m128_f32[2] );

		if( (int)distanceFloat > 0 )
		{
			XMVECTOR position = PositionVector();
			position += distance * mVelocityFactor * gameTime.ElapsedGameTime();
			XMStoreFloat3( &mPosition, position );
		}
	}

}