#include "AudioPlayer.h"

namespace Rendering
{
	AudioPlayer::AudioPlayer()
		:
		mAudioBackground( nullptr ),
		mAudioEngine( nullptr )
	{
		mAudioBackground = createIrrKlangDevice();
		mAudioEngine = createIrrKlangDevice();
	}
	AudioPlayer::~AudioPlayer()
	{
		if( mAudioBackground )
		{
			mAudioBackground->drop();
		}
		if( mAudioEngine )
		{
			mAudioEngine->drop();
		}


	}
	void AudioPlayer::PlayAudioBackground( std::string sampleName, float volume )
	{
		mAudioBackground->play2D( sampleName.c_str(), true );
		mAudioBackground->setSoundVolume( volume );
	}
	void AudioPlayer::StopAudioBackGround()
	{
		mAudioBackground->stopAllSounds();
	}
	void AudioPlayer::PlaySample( std::string sampleName, bool isLooped, float volume )
	{
		mAudioEngine->play2D( sampleName.c_str(), isLooped );
		mAudioEngine->setSoundVolume( volume );
	}
	bool AudioPlayer::IsSamplePlaying( std::string sampleName ) const
	{
		return mAudioEngine->isCurrentlyPlaying( sampleName.c_str() );
	}
	bool AudioPlayer::IsAudioBackgroundPlaying( std::string sampleName ) const
	{
		return mAudioBackground->isCurrentlyPlaying( sampleName.c_str() );
	}
	void AudioPlayer::Initialize()
	{
	}
}