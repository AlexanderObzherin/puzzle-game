#pragma once

#include "LevelsCommon.h"
#include <list>
#include <unordered_map>
#include <functional>
#include "Randomizer.h"

namespace Rendering
{
	class AraneusAngulatus;

	class Level3 : public LevelsCommon
	{
		RTTI_DECLARATIONS( Level3, LevelsCommon )

	public:
		Level3(Game& game, SceneManager& sceneManager);
		~Level3();

		virtual void Initialize() override;
		virtual void Update(const GameTime& gameTime) override;
		virtual void Draw(const GameTime& gameTime) override;

		void ScenarioTerm01(const GameTime& gameTime);
		void ScenarioTerm02(const GameTime& gameTime);
		void ScenarioTerm03(const GameTime& gameTime);
		void ScenarioTerm04(const GameTime& gameTime);
		void ScenarioTerm05(const GameTime& gameTime);
		void ScenarioTerm06(const GameTime& gameTime);

	private:
		//To measure time of ladybug wings moving
		float mLadybugWingMovingTimer;

		std::list<AraneusAngulatus*> mAraneusAngulatuss;
		float mTimer;

		bool bGameIsOver;
		bool bGameIsFinished;
		bool bIsWarning;

	};

}

