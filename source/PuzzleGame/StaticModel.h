#pragma once

#include "Actor.h"

using namespace Library;

namespace Library
{
	class Technique;
	class Effect;
	class DirectionalLight;
	class RenderStateHelper;
}

namespace DirectX
{
	class SpriteBatch;
	class SpriteFont;
}

namespace Rendering
{
	class StaticModelMaterial;

	class StaticModel
	{
	public:
		StaticModel( Actor& actor, const DirectionalLight& directionalLightSource );
		~StaticModel();

		void SetModel( const std::string modelFilenamePath );
		void SetColorTexture( const std::wstring textureFilenamePath );
		void SetNormalMapTexture( const std::wstring textureFilenamePath );
		void DrawWireframeOnly( bool in );
		void SetText( std::wstring in );
		void DrawAmbientOnly();
		void DrawAmbientDiffuse();
		void DrawAmbientDiffuseWithNormalMap();
		void DrawPhongWithNormalMap();

		virtual void Initialize();
		virtual void Update( const GameTime& gameTime );
		virtual void Draw( const GameTime& gameTime );

		XMCOLOR GetAmbientColor()
		{
			return mAmbientColor;
		}

		void SetAmbientColor( XMCOLOR ambientColor )
		{
			mAmbientColor = ambientColor;
		}
		void SetSpecularColor( XMCOLOR specularColor )
		{
			mSpecularColor = specularColor;
		}
		void SetSpecularPower( float specularPower )
		{
			mSpecularPower = specularPower;
		}

	protected:
		Actor& mActor;
		const DirectionalLight& mDirectionalLightSource;

	private:
		//StaticModel();
		//StaticModel( const StaticModel& rhs );
		//StaticModel& operator=( const StaticModel& rhs );

		void UpdateAmbientLight( const GameTime& gameTime );
		void UpdateSpecularLight( const GameTime& gameTime );

		static const float LightModulationRate;
		static const XMFLOAT2 LightRotationRate;

		std::vector<Technique*>::const_iterator techniqueIterator;

		Effect* mEffect;
		StaticModelMaterial* mMaterial;
		ID3D11ShaderResourceView* mColorTextureShaderResourceView;
		ID3D11ShaderResourceView* mNormalMapShaderResourceView;

		ID3D11Buffer* mVertexBuffer;
		ID3D11Buffer* mIndexBuffer;
		UINT mIndexCount;

		XMCOLOR mAmbientColor;
		XMCOLOR mSpecularColor;
		float mSpecularPower;

		std::string mModelFilename;
		std::wstring mColorTextureFilename;
		std::wstring mNormalMapTextureFilename;

		bool bDrawWireframeOnly;

		RenderStateHelper* mRenderStateHelper;
		SpriteBatch* mSpriteBatch;
		SpriteFont* mSpriteFont;

		std::wstring mText;
	};
}