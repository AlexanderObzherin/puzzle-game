#pragma once
#include "Actor.h"

using namespace Library;

namespace Library
{
	class DirectionalLight;
}

namespace Rendering
{
	class StaticModel;

	class CentaureaCyanus : public Actor
	{
		RTTI_DECLARATIONS( CentaureaCyanus, Actor )

	public:
		CentaureaCyanus( Game& game, Camera& camera, const DirectionalLight& directionalLightSource );
		~CentaureaCyanus();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

	private:
		CentaureaCyanus();
		CentaureaCyanus( const CentaureaCyanus& rhs );
		CentaureaCyanus& operator=( const CentaureaCyanus& rhs );

		const DirectionalLight& mDirectionalLightSource;
		StaticModel* mShape;
	};
}