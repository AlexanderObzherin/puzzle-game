#include "include\\Common.fxh"

#define MaxBones 60

/************* Resources *************/
cbuffer CBufferPerFrame
{
	float4 AmbientColor = { 1.0f, 1.0f, 1.0f, 0.0f };
	float4 LightColor = { 1.0f, 1.0f, 1.0f, 1.0f };

	float4 LightDirection = { 0.0f, 0.0f, -1.0f, 0.0f };
	float3 CameraPosition;
}

cbuffer CBufferPerObject
{
	float4x4 WorldViewProjection : WORLDVIEWPROJECTION;
	float4x4 World : WORLD;
	float4 SpecularColor : SPECULAR = { 1.0f, 1.0f, 1.0f, 1.0f };
	float SpecularPower : SPECULARPOWER = 25.0f;
}

cbuffer CBufferSkinning
{
	float4x4 BoneTransforms[MaxBones];
}

Texture2D ColorTexture <
	string ResourceName = "default_color.dds";
	string UIName = "Color Texture";
	string ResourceType = "2D";
> ;

Texture2D NormalMapTexture <
	string ResourceName = "default_bump_normal.dds";
	string UIName = "Normap Map";
	string ResourceType = "2D";
> ;

SamplerState ColorSampler
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = WRAP;
	AddressV = WRAP;
};



/************* Data Structures *************/

struct VS_INPUT
{
	float4 ObjectPosition : POSITION;
	float2 TextureCoordinate : TEXCOORD;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
	uint4 BoneIndices : BONEINDICES;
	float4 BoneWeights : WEIGHTS;
};

struct VS_OUTPUT
{
	float4 Position : SV_Position;
	float3 Normal : NORMAL;
	float3 Tangent : TANGENT;
	float3 Binormal : BINORMAL;
	//
	float2 TextureCoordinate : TEXCOORD0;

	float3 LightDirection : TEXCOORD1;
	float3 ViewDirection : TEXCOORD2;
	//float3 WorldPosition : TEXCOORD1;
	//float Attenuation : TEXCOORD2;
};

/************* Vertex Shader *************/

VS_OUTPUT vertex_shader( VS_INPUT IN )
{
	VS_OUTPUT OUT = (VS_OUTPUT)0;

	float4x4 skinTransform = (float4x4)0;
	skinTransform += BoneTransforms[IN.BoneIndices.x] * IN.BoneWeights.x;
	skinTransform += BoneTransforms[IN.BoneIndices.y] * IN.BoneWeights.y;
	skinTransform += BoneTransforms[IN.BoneIndices.z] * IN.BoneWeights.z;
	skinTransform += BoneTransforms[IN.BoneIndices.w] * IN.BoneWeights.w;

	float4 position = mul( IN.ObjectPosition, skinTransform );
	OUT.Position = mul( position, WorldViewProjection );
	//OUT.WorldPosition = mul( position, World ).xyz;

	float4 normal = mul( float4(IN.Normal, 0), skinTransform );
	OUT.Normal = normalize( mul( normal, World ).xyz );
	//

	//
	OUT.TextureCoordinate = IN.TextureCoordinate;

	OUT.LightDirection = normalize( -LightDirection );

	float3 worldPosition = mul( IN.ObjectPosition, World ).xyz;
	OUT.ViewDirection = normalize( CameraPosition - worldPosition );
	//float3 lightDirection = LightPosition - OUT.WorldPosition;
	//OUT.Attenuation = saturate( 1.0f - (length( lightDirection ) / LightRadius) );

	return OUT;
}

VS_OUTPUT vertex_shader_with_tbn( VS_INPUT IN )
{
	VS_OUTPUT OUT = (VS_OUTPUT)0;

	float4x4 skinTransform = (float4x4)0;
	skinTransform += BoneTransforms[IN.BoneIndices.x] * IN.BoneWeights.x;
	skinTransform += BoneTransforms[IN.BoneIndices.y] * IN.BoneWeights.y;
	skinTransform += BoneTransforms[IN.BoneIndices.z] * IN.BoneWeights.z;
	skinTransform += BoneTransforms[IN.BoneIndices.w] * IN.BoneWeights.w;

	float4 position = mul( IN.ObjectPosition, skinTransform );
	OUT.Position = mul( position, WorldViewProjection );
	

	float4 normal = mul( float4(IN.Normal, 0), skinTransform );
	OUT.Normal = normalize( mul( normal, World ).xyz );

	float4 tangent = mul( float4(IN.Tangent, 0), skinTransform );
	OUT.Tangent = normalize( mul( tangent, World ).xyz );

	OUT.Binormal = cross( OUT.Normal, OUT.Tangent );


	//

	//
	OUT.TextureCoordinate = IN.TextureCoordinate;

	OUT.LightDirection = normalize( -LightDirection );

	float3 worldPosition = mul( IN.ObjectPosition, World ).xyz;
	OUT.ViewDirection = normalize( CameraPosition - worldPosition );
	//float3 lightDirection = LightPosition - OUT.WorldPosition;
	//OUT.Attenuation = saturate( 1.0f - (length( lightDirection ) / LightRadius) );

	return OUT;
}

/************* Pixel Shaders *************/

float4 ambient_diffuse_specular_bump_pixel_shader( VS_OUTPUT IN ) : SV_Target
{
	float4 OUT = (float4)0;

	float3 sampledNormal = (2 * NormalMapTexture.Sample( ColorSampler, IN.TextureCoordinate ).xyz) - 1.0; // Map normal from [0..1] to [-1..1]
	float3x3 tbn = float3x3(IN.Tangent, IN.Binormal, IN.Normal);

	sampledNormal = mul( sampledNormal, tbn ); // Transform normal to world space

	float3 viewDirection = normalize( IN.ViewDirection );
	float4 color = ColorTexture.Sample( ColorSampler, IN.TextureCoordinate );
	float3 ambient = get_vector_color_contribution( AmbientColor, color.rgb );

	LIGHT_CONTRIBUTION_DATA lightContributionData;
	lightContributionData.Color = color;
	lightContributionData.Normal = sampledNormal;
	lightContributionData.ViewDirection = viewDirection;
	lightContributionData.LightDirection = float4(IN.LightDirection, 1);
	lightContributionData.SpecularColor = SpecularColor;
	lightContributionData.SpecularPower = SpecularPower;
	lightContributionData.LightColor = LightColor;
	float3 light_contribution = get_light_contribution( lightContributionData );

	OUT.rgb = ambient + light_contribution;
	OUT.a = 1.0f;

	return OUT;
}

float4 ambient_diffuse_bump_pixel_shader( VS_OUTPUT IN ) : SV_Target
{
	float4 OUT = (float4)0;

	float3 sampledNormal = (2 * NormalMapTexture.Sample( ColorSampler, IN.TextureCoordinate ).xyz) - 1.0; // Map normal from [0..1] to [-1..1]
	float3x3 tbn = float3x3(IN.Tangent, IN.Binormal, IN.Normal);

	sampledNormal = mul( sampledNormal, tbn ); // Transform normal to world space

	float3 normal = normalize( sampledNormal );
	float3 lightDirection = normalize( IN.LightDirection );
	float3 viewDirection = normalize( IN.ViewDirection );
	float n_dot_l = dot( lightDirection, normal );

	float4 color = ColorTexture.Sample( ColorSampler, IN.TextureCoordinate );
	float3 ambient = AmbientColor.rgb * AmbientColor.a * color.rgb;

	float3 diffuse = (float3)0;
	float3 specular = (float3)0;

	if (n_dot_l > 0)
	{
		diffuse = LightColor.rgb * LightColor.a * saturate( n_dot_l ) * color.rgb;
	}

	OUT.rgb = ambient + diffuse;
	OUT.a = 1.0f;

	return OUT;
}

float4 ambient_diffuse_specular_pixel_shader( VS_OUTPUT IN ) : SV_Target
{
	float4 OUT = (float4)0;

	float3 normal = normalize( IN.Normal );
	float3 lightDirection = normalize( IN.LightDirection );
	float3 viewDirection = normalize( IN.ViewDirection );
	float n_dot_l = dot( lightDirection, normal );

	float4 color = ColorTexture.Sample( ColorSampler, IN.TextureCoordinate );
	float3 ambient = AmbientColor.rgb * AmbientColor.a * color.rgb;

	float3 diffuse = (float3)0;
	float3 specular = (float3)0;

	if (n_dot_l > 0)
	{
		diffuse = LightColor.rgb * LightColor.a * saturate( n_dot_l ) * color.rgb;

		// R = 2 * (N.L) * N - L
		float3 reflectionVector = normalize( 2 * n_dot_l * normal - lightDirection );

		// specular = R.V^n with gloss map stored in color texture's alpha channel	
		specular = SpecularColor.rgb * SpecularColor.a * min( pow( saturate( dot( reflectionVector, viewDirection ) ), SpecularPower ), color.w );
	}

	OUT.rgb = ambient + diffuse + specular;
	OUT.a = 1.0f;

	return OUT;
}

float4 ambient_diffuse_pixel_shader( VS_OUTPUT IN ) : SV_Target
{
	float4 OUT = (float4)0;

	float3 normal = normalize( IN.Normal );
	float3 lightDirection = normalize( IN.LightDirection );
	float3 viewDirection = normalize( IN.ViewDirection );
	float n_dot_l = dot( lightDirection, normal );

	float4 color = ColorTexture.Sample( ColorSampler, IN.TextureCoordinate );
	float3 ambient = AmbientColor.rgb * AmbientColor.a * color.rgb;

	float3 diffuse = (float3)0;
	float3 specular = (float3)0;

	if (n_dot_l > 0)
	{
		diffuse = LightColor.rgb * LightColor.a * saturate( n_dot_l ) * color.rgb;

		// R = 2 * (N.L) * N - L
		float3 reflectionVector = normalize( 2 * n_dot_l * normal - lightDirection );

		// specular = R.V^n with gloss map stored in color texture's alpha channel	
		specular = SpecularColor.rgb * SpecularColor.a * min( pow( saturate( dot( reflectionVector, viewDirection ) ), SpecularPower ), color.w );
	}

	OUT.rgb = ambient + diffuse;
	OUT.a = 1.0f;

	return OUT;
}

float4 ambient_pixel_shader( VS_OUTPUT IN ) : SV_Target
{
	float4 OUT = (float4)0;

	float3 normal = normalize( IN.Normal );
	OUT = ColorTexture.Sample( ColorSampler, IN.TextureCoordinate );
	//float3 ambient = AmbientColor.rgb * AmbientColor.a * color.rgb;

	return OUT;
}

technique10 AmbDifSpec
{
	pass p0
	{
		SetVertexShader( CompileShader(vs_4_0, vertex_shader()) );
		SetGeometryShader( NULL );
		SetPixelShader( CompileShader(ps_4_0, ambient_diffuse_specular_pixel_shader()) );

		//SetRasterizerState( DisableCulling );
	}
}

technique10 AmbDif
{
	pass p0
	{
		SetVertexShader( CompileShader(vs_4_0, vertex_shader()) );
		SetGeometryShader( NULL );
		SetPixelShader( CompileShader(ps_4_0, ambient_diffuse_pixel_shader()) );

		//SetRasterizerState( DisableCulling );
	}
}

technique10 Amb
{
	pass p0
	{
		SetVertexShader( CompileShader(vs_4_0, vertex_shader()) );
		SetGeometryShader( NULL );
		SetPixelShader( CompileShader(ps_4_0, ambient_pixel_shader()) );

		//SetRasterizerState( DisableCulling );
	}
}

technique10 AmbDifSpecBump
{
	pass p0
	{
		SetVertexShader( CompileShader(vs_4_0, vertex_shader_with_tbn()) );
		SetGeometryShader( NULL );
		SetPixelShader( CompileShader(ps_4_0, ambient_diffuse_specular_bump_pixel_shader()) );

		//SetRasterizerState( DisableCulling );
	}
}

technique10 AmbDifBump
{
	pass p0
	{
		SetVertexShader( CompileShader(vs_4_0, vertex_shader_with_tbn()) );
		SetGeometryShader( NULL );
		SetPixelShader( CompileShader(ps_4_0, ambient_diffuse_bump_pixel_shader()) );

		//SetRasterizerState( DisableCulling );
	}
}
