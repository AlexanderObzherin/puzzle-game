#pragma once

#include "Actor.h"

using namespace Library;

namespace Library
{
	class DirectionalLight;
}

namespace Rendering
{
	class StaticModel;

	class Taraxacum : public Actor
	{
		RTTI_DECLARATIONS( Taraxacum, Actor )

	public:
		Taraxacum( Game& game, Camera& camera, const DirectionalLight& directionalLightSource );
		~Taraxacum();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

	private:
		Taraxacum();
		Taraxacum( const Taraxacum& rhs );
		Taraxacum& operator=( const Taraxacum& rhs );

		const DirectionalLight& mDirectionalLightSource;
		StaticModel* mShape;
	};
}