#include "RedCloverShort.h"
#include "StaticModel.h"

namespace Rendering
{
	RTTI_DEFINITIONS( RedCloverShort )

		RedCloverShort::RedCloverShort( Game& game, Camera& camera, const DirectionalLight& directionalLightSource )
		:
		Actor( game, camera ),
		mDirectionalLightSource( directionalLightSource ),
		mShape( nullptr )
	{
	}
	RedCloverShort::~RedCloverShort()
	{
		DeleteObject( mShape );
	}

	void RedCloverShort::Initialize()
	{
		SetScale( XMFLOAT3( 8.0f, 8.0f, 8.0f ) );
		Actor::Initialize();

		mShape = new StaticModel( *this, mDirectionalLightSource );
		mShape->SetModel( "Content\\Models\\RedClover\\RedCloverShort.obj" );
		mShape->SetColorTexture( L"Content\\Models\\RedClover\\RedCloverColorTexture.png" );
		mShape->SetNormalMapTexture( L"Content\\Models\\RedClover\\RedCloverNormalMapTexture.png" );

		mShape->Initialize();
		mShape->DrawAmbientDiffuseWithNormalMap();
	}
	void RedCloverShort::Update( const GameTime& gameTime )
	{
		Actor::Update( gameTime );
	}
	void RedCloverShort::Draw( const GameTime& gameTime )
	{
		mShape->Draw( gameTime );
	}

}