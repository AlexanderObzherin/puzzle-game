#pragma once

#include "Scene.h"
#include <list>
#include <unordered_map>
#include <functional>
#include "Randomizer.h"

using namespace Library;

namespace Library
{
	class Keyboard;
	class Mouse;
	class Camera;
	class FirstPersonCameraController;
	class Skybox;
	class DirectionalLight;
	class RenderStateHelper;
}

namespace DirectX
{
	class SpriteBatch;
	class SpriteFont;
}

namespace Rendering
{
	class ThirdPersonCameraController;
	class CameraTarget;
	class Insect;
	class LadybugPlayable;
	class GameGrid;
	class LawnBlockPath;
	class ScenarioTerm;
	class Lawn;
	class ParticleEffect;
	class AudioPlayer;
	class LevelLabel;


	//Intermediate class to store basic level staff
	class LevelsCommon : public Scene
	{
		RTTI_DECLARATIONS( LevelsCommon, Scene )
	protected:

		enum Menu
		{
			ReturnToGame = 0b0001,
			StartLevel = 0b0010,
			StartLevel1 = 0b0100,
			ExitMenu = 0b1000
		};

	public:
		LevelsCommon( Game& game, SceneManager& sceneManager );
		~LevelsCommon();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		void SwitchScenarioToTerm( const std::string name );

	protected:
		static const XMVECTORF32 BackgroundColor;
		static const float LightModulationRate;
		static const XMFLOAT2 LightRotationRate;

		SpriteBatch* mSpriteBatch;
		SpriteFont* mSpriteFont;

		Keyboard* mKeyboard;

		Camera* mCamera;
		ThirdPersonCameraController* mCameraController;
		//just for edit mode
		//FirstPersonCameraController* mCameraController;

		CameraTarget* mCameraTarget;

		DirectionalLight* mDirectionalLight;
		RenderStateHelper* mRenderStateHelper;
		Skybox* mSkybox;

		LadybugPlayable* mLadybugPlayable;

		int mNumberOfGameGrids;
		const float mDistancebetweenGrids;

		std::list<GameGrid*> mGameGrids;
		std::list<GameGrid*>::iterator mGameGridIterator;

		std::list<LawnBlockPath*> mLawnBlockPath;
		std::list<LawnBlockPath*>::iterator mLawnBlockPathIterator;

		//Map of pointers to functions to play level's scenario
		std::unordered_map<std::string, std::function<void( const GameTime& gameTime )> > mScenarioTerms;
		std::function< void( const GameTime& gameTime ) > mCurrentScenarioTerm;

		//To spawn npc insects
		std::list<Insect*> mFlyingInsects;
		float mTimerForFlyingInsectsSpawning;
		float mSpawningDelay;
		bool bIsItSpawning;

		void UpdateInsectSpawner( const GameTime& gameTime );
		void CreateInsect();
		
		Randomizer<Insect>* mFlyingInsectsList;

		//Flag for pause game
		bool bGameIsPaused;
		//Bool flag helps cameracontroller switch distance then game is turning from previous to new gamegrid
		bool bGridIsPlaying;

		//Menu background texture
		ID3D11ShaderResourceView* mMenuBgTexture;
		RECT mMenuTextureRect;
		XMFLOAT2 mMenuPosition;
		const unsigned int mMenuBgWidth;
		const unsigned int mMenuBgHeight;
		float mMenuBgScale;

		//Regular menu. Activates according to bGameIsPaused
		Menu mMenuActiveOption;

		std::vector<Lawn*> mLawns;

		ParticleEffect* mParticleEffect;
		AudioPlayer* mAudioPlayer;

		int mLevelNum;
	};

}