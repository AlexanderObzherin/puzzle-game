#include "CentaureaCyanus.h"
#include "StaticModel.h"

namespace Rendering
{
	RTTI_DEFINITIONS( CentaureaCyanus )

	CentaureaCyanus::CentaureaCyanus( Game& game, Camera& camera, const DirectionalLight& directionalLightSource )
		:
		Actor( game, camera ),
		mDirectionalLightSource( directionalLightSource ),
		mShape( nullptr )
	{
	}
	CentaureaCyanus::~CentaureaCyanus()
	{
		DeleteObject( mShape );
	}

	void CentaureaCyanus::Initialize()
	{
		SetScale( XMFLOAT3( 4.0f, 4.0f, 4.0f ) );
		Actor::Initialize();

		mShape = new StaticModel( *this, mDirectionalLightSource );
		mShape->SetModel( "Content\\Models\\CentaureaCyanus\\CentaureaCyanus.obj" );
		mShape->SetColorTexture( L"Content\\Models\\CentaureaCyanus\\CentaureaCyanusColorTexture.png" );

		mShape->Initialize();
		mShape->DrawAmbientDiffuse();
	}
	void CentaureaCyanus::Update( const GameTime& gameTime )
	{
		Actor::Update( gameTime );
	}
	void CentaureaCyanus::Draw( const GameTime& gameTime )
	{
		mShape->Draw( gameTime );
	}

}