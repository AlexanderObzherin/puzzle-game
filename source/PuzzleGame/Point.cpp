#include "Point.h"
#include "StaticModel.h"
#include "Block.h"

namespace Rendering
{
	RTTI_DEFINITIONS( Point )

		Point::Point( Game& game, Camera& camera, const DirectionalLight& directionalLightSource, Block& block )
		:
		Actor( game, camera ),
		mDirectionalLightSource( directionalLightSource ),
		mBlock( block ),
		mShape( nullptr )
	{
	}

	Point::~Point()
	{
		DeleteObject( mShape );
	}

	void Point::Initialize()
	{
		mShape = new StaticModel( *this, mDirectionalLightSource );
		mShape->SetModel( "Content\\Models\\Proxy\\Sphere.obj" );

		mShape->Initialize();
		SetScale( XMFLOAT3( 0.1f, 0.1f, 0.1f ) );

		Actor::Initialize();
	}

	void Point::Update( const GameTime& gameTime )
	{
		Actor::Update( gameTime );

	}

	void Point::Draw( const GameTime& gameTime )
	{
		mShape->Draw( gameTime );
	}

	Block & Point::GetBlock()
	{
		return mBlock;
	}

}