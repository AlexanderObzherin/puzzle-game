#pragma once

#include "DrawableGameComponent.h"
#include "SceneManager.h"
using namespace Library;

namespace Rendering
{


	class Scene : public DrawableGameComponent
	{
		RTTI_DECLARATIONS( Scene, DrawableGameComponent )

	public:
		enum LevelName
		{
			LevelName1 = 1,
			LevelName2 = 2,
			LevelName3 = 3
		}; static LevelName mLevelName;

	public:
		Scene( Game& game, SceneManager& sceneManager );
		~Scene();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		void SceneSwitching( Scene* newScene );

	protected:
		SceneManager& mSceneManager;

		static const XMVECTORF32 BackgroundColor;
		static const float LightModulationRate;
		static const XMFLOAT2 LightRotationRate;

		
		
	};
}