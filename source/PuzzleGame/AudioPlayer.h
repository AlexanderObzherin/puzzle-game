#pragma once

#include <irrKlang.h>
#include <string>

using namespace irrklang;

#pragma comment(lib, "irrKlang.lib")

namespace Rendering
{
	class AudioPlayer
	{
	public:
		AudioPlayer();
		~AudioPlayer();

		void PlayAudioBackground( std::string sampleName, float volume );
		void StopAudioBackGround();
		void PlaySample( std::string sampleName, bool isLooped = false, float volume = 1.0f );
		bool IsSamplePlaying( std::string sampleName ) const;
		bool IsAudioBackgroundPlaying( std::string sampleName ) const;

		void Initialize();

	private:
		AudioPlayer( const AudioPlayer& rhs );
		AudioPlayer& operator=( const AudioPlayer& rhs );

	private:
		ISoundEngine* mAudioBackground;
		ISoundEngine* mAudioEngine;
	};

}