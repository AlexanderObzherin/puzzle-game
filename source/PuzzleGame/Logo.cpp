#include "Logo.h"
#include "PointSprite.h"

namespace Rendering
{
	RTTI_DEFINITIONS( Logo )

		Logo::Logo( Game& game, Camera& camera )
		:
		Actor( game, camera ),
		mPointSprite( nullptr )
	{}
	Logo::~Logo() 
	{
		DeleteObject( mPointSprite );
	}

	void Logo::Initialize()
	{
		mPointSprite = new PointSprite( *this );
		mPointSprite->SetTexture( L"Content\\Textures\\Logo.png" );
		mPointSprite->Initialize();

		Actor::Initialize();
	}

	void Logo::Update( const GameTime & gameTime )
	{
		mPointSprite->Update( gameTime );
		Actor::Update( gameTime );
	}

	void Logo::Draw( const GameTime & gameTime )
	{
		mPointSprite->Draw( gameTime );
	}



}