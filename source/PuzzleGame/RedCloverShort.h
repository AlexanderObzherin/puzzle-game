#pragma once

#include "Actor.h"

using namespace Library;

namespace Library
{
	class DirectionalLight;
}

namespace Rendering
{
	class StaticModel;

	class RedCloverShort : public Actor
	{
		RTTI_DECLARATIONS( RedCloverShort, Actor )

	public:
		RedCloverShort( Game& game, Camera& camera, const DirectionalLight& directionalLightSource );
		~RedCloverShort();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

	private:
		RedCloverShort();
		RedCloverShort( const RedCloverShort& rhs );
		RedCloverShort& operator=( const RedCloverShort& rhs );

		const DirectionalLight& mDirectionalLightSource;
		StaticModel* mShape;
	};
}