#include "IntermediateScene.h"
#include "RenderStateHelper.h"
#include <sstream>
#include <iomanip>
#include <SpriteBatch.h>
#include <SpriteFont.h>
#include "Game.h"
#include "Utility.h"
#include "Keyboard.h"
#include "Level1.h"
#include "Level2.h"
#include "Level3.h"
#include "ColorHelper.h"

namespace Rendering
{
	RTTI_DEFINITIONS( IntermediateScene )

	IntermediateScene::IntermediateScene( Game& game, SceneManager& sceneManager )
		:
		Scene( game, sceneManager ),
		mSpriteBatch( nullptr ),
		mSpriteFont( nullptr ),
		mRenderStateHelper( nullptr ),
		mKeyboard( nullptr ),
		mTimer( 0.0f )
	{
	}
	IntermediateScene::~IntermediateScene()
	{
		DeleteObject( mRenderStateHelper );
		DeleteObject( mSpriteFont );
		DeleteObject( mSpriteBatch );
	}

	void IntermediateScene::Initialize()
	{
		SetCurrentDirectory( Utility::ExecutableDirectory().c_str() );

		mSpriteBatch = new SpriteBatch( mGame->Direct3DDeviceContext() );
		mSpriteFont = new SpriteFont( mGame->Direct3DDevice(), L"Content\\Fonts\\Berlin_Sans_Fb_Demi_24_Bold.spritefont" );
		mRenderStateHelper = new RenderStateHelper( *mGame );
		mKeyboard = (Keyboard*)mGame->Services().GetService( Keyboard::TypeIdClass() );
	}

	void IntermediateScene::Update( const GameTime & gameTime )
	{
		mTimer += gameTime.ElapsedGameTime();

		if (mTimer >= 6.0f)
		{
			switch ( mLevelName )
			{
				case LevelName1:
				{
					SceneSwitching( new Level1( *mGame, mSceneManager ) );
				} break;
				case LevelName2:
				{
					SceneSwitching( new Level2( *mGame, mSceneManager ) );
				} break;
				case LevelName3:
				{
					SceneSwitching( new Level3( *mGame, mSceneManager ) );
				} break;
			}
		}
	}

	void IntermediateScene::Draw( const GameTime & gameTime )
	{
		mRenderStateHelper->SaveAll();
		mSpriteBatch->Begin();

		mSpriteFont->DrawString( mSpriteBatch, L"Please, wait...", XMFLOAT2( (int)((mGame->ScreenWidth() / 2.0f) - ((16.6f * 15.0f) / 2.0f)), 200 ), XMVECTORF32( /*0 153 25*/{ 0.0f, 0.599f, 0.098f, 1.0f } ) );


		if ( mLevelName == Scene::LevelName::LevelName1 )
		{
			mSpriteFont->DrawString( mSpriteBatch, L"W", XMFLOAT2( (int)(((mGame->ScreenWidth() / 2.0f) - (mGame->ScreenWidth() / 4.0f)) -
				 10.0f), 400 ), XMVECTORF32( /*240 240 240*/{ 0.9f, 0.9f, 0.9f, 1.0f } ) );
			mSpriteFont->DrawString( mSpriteBatch, L"A", XMFLOAT2( (int)(((mGame->ScreenWidth() / 2.0f) - (mGame->ScreenWidth() / 4.0f)) -
				100.0f), 450 ), XMVECTORF32( /*240 240 240*/{ 0.9f, 0.9f, 0.9f, 1.0f } ) );
			mSpriteFont->DrawString( mSpriteBatch, L"S", XMFLOAT2( (int)(((mGame->ScreenWidth() / 2.0f) - (mGame->ScreenWidth() / 4.0f)) -
				0.0f), 450 ), XMVECTORF32( /*240 240 240*/{ 0.9f, 0.9f, 0.9f, 1.0f } ) );
			mSpriteFont->DrawString( mSpriteBatch, L"D", XMFLOAT2( (int)(((mGame->ScreenWidth() / 2.0f) - (mGame->ScreenWidth() / 4.0f)) +
				80.0f), 450 ), XMVECTORF32( /*240 240 240*/{ 0.9f, 0.9f, 0.9f, 1.0f } ) );


			mSpriteFont->DrawString( mSpriteBatch, L"Up", XMFLOAT2( (int)(((mGame->ScreenWidth() / 2.0f) + (mGame->ScreenWidth() / 4.0f)) -
				((16.6f * 0.0f) / 2.0f)), 400 ), XMVECTORF32( /*240 240 240*/{ 0.9f, 0.9f, 0.9f, 1.0f } ) );
			mSpriteFont->DrawString( mSpriteBatch, L"Left", XMFLOAT2( (int)(((mGame->ScreenWidth() / 2.0f) + (mGame->ScreenWidth() / 4.0f)) -
				100.0f), 450 ), XMVECTORF32( /*240 240 240*/{ 0.9f, 0.9f, 0.9f, 1.0f } ) );
			mSpriteFont->DrawString( mSpriteBatch, L"Down", XMFLOAT2( (int)(((mGame->ScreenWidth() / 2.0f) + (mGame->ScreenWidth() / 4.0f)) -
				20.0f), 450 ), XMVECTORF32( /*240 240 240*/{ 0.9f, 0.9f, 0.9f, 1.0f } ) );
			mSpriteFont->DrawString( mSpriteBatch, L"Right", XMFLOAT2( (int)(((mGame->ScreenWidth() / 2.0f) + (mGame->ScreenWidth() / 4.0f)) +
				80.0f), 450 ), XMVECTORF32( /*240 240 240*/{ 0.9f, 0.9f, 0.9f, 1.0f } ) );
		}
		mSpriteBatch->End();

		mRenderStateHelper->RestoreAll();

	}
}