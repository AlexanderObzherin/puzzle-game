#include "LawnBlockPath.h"
//#include "Block.h"
#include <random>

using namespace std::placeholders;
namespace Rendering
{
	RTTI_DEFINITIONS( LawnBlockPath )

	LawnBlockPath::LawnBlockPath( Game& game, Camera& camera, const DirectionalLight& directionalLightSource, int level )
		:
		BlockPath( game, camera, directionalLightSource ),
		//mLevelName( levelName )
		mRandomizerList1( nullptr ),
		mRandomizerList2( nullptr ),
		mLevel( level )
	{
		SetEnabled( false );
	}
	LawnBlockPath::~LawnBlockPath()
	{
		//DeleteObject( mRandomizerList2 );
		//DeleteObject( mRandomizerList1 );
		for( auto& it : mListsOfPlants )
		{
			DeleteObject( it.second );
		}
		mListsOfPlants.clear();

		mRandomizerList1 = nullptr;
		mRandomizerList2 = nullptr;
	}

	void LawnBlockPath::Initialize()
	{
		mRandomizerList1 = new Randomizer<Block>();
		mRandomizerList2 = new Randomizer<Block>();
		CreateGrid();
	}
	void LawnBlockPath::Update( const GameTime& gameTime )
	{
		BlockPath::Update( gameTime );
	}
	void LawnBlockPath::Draw( const GameTime& gameTime )
	{
		if( Enabled() )
		{
			BlockPath::Draw( gameTime );
		}

	}
	XMFLOAT3 LawnBlockPath::GetCenter() const
	{
		return XMFLOAT3( Position().x + (1 * 20), Position().y + (1 * 20), Position().z + (1 * 20) );
	}
	void LawnBlockPath::CreateGrid()
	{
		//Create lambdas which creates blocks and put them into randomizer;
		//Here is defined entire set of plants
		auto GetGrassBlock = [&](){ return new GrassBlock( *mGame, *mCamera, mDirectionalLightSource ); };
		auto GetHepaticaBlock = [&]() { return new BlockHepatica( *mGame, *mCamera, mDirectionalLightSource ); };
		auto GetCloverLeafBlock = [&]() { return new RedCloverLeafBlock( *mGame, *mCamera, mDirectionalLightSource ); };
		auto GetCloverShortBlock = [&]() { return new RedCloverShortBlock( *mGame, *mCamera, mDirectionalLightSource ); };
		auto GetCloverBlock = [&]() { return new RedCloverBlock( *mGame, *mCamera, mDirectionalLightSource ); };
		auto GetCentaureaCyanusBlock = [&]() { return new CentaureaCyanusBlock( *mGame, *mCamera, mDirectionalLightSource ); };
		auto GetCentaureaCyanusShortBlock = [&]() { return new CentaureaCyanusShortBlock( *mGame, *mCamera, mDirectionalLightSource ); };
		auto GetTaraxacumBlock = [&]() { return new TaraxacumBlock( *mGame, *mCamera, mDirectionalLightSource ); };

		//Next we define list with sets of plants
		//with couple for each level. One with tall flowers (on perimeter of grid) and second without tall.

		mListsOfPlants.emplace( 1, new Randomizer<Block> );
		mListsOfPlants[1]->AddToSetList( 60, GetGrassBlock );
		mListsOfPlants[1]->AddToSetList( 6, GetHepaticaBlock );
		mListsOfPlants[1]->AddToSetList( 6, GetCloverLeafBlock );
		mListsOfPlants[1]->AddToSetList( 6, GetCloverShortBlock );
		mListsOfPlants[1]->AddToSetList( 5, GetTaraxacumBlock );

		mListsOfPlants.emplace( 2, new Randomizer<Block> );
		mListsOfPlants[2]->AddToSetList( 60, GetGrassBlock );
		mListsOfPlants[2]->AddToSetList( 7, GetHepaticaBlock );
		mListsOfPlants[2]->AddToSetList( 10, GetCloverLeafBlock );
		mListsOfPlants[2]->AddToSetList( 10, GetCloverShortBlock );
		mListsOfPlants[2]->AddToSetList( 2, GetCloverBlock );
		mListsOfPlants[2]->AddToSetList( 5, GetTaraxacumBlock );

		mListsOfPlants.emplace( 3, new Randomizer<Block> );
		mListsOfPlants[3]->AddToSetList( 65, GetGrassBlock );
		mListsOfPlants[3]->AddToSetList( 6, GetHepaticaBlock );
		mListsOfPlants[3]->AddToSetList( 6, GetCloverLeafBlock );
		mListsOfPlants[3]->AddToSetList( 3, GetCloverShortBlock );
		mListsOfPlants[3]->AddToSetList( 3, GetCentaureaCyanusShortBlock );

		mListsOfPlants.emplace( 4, new Randomizer<Block> );
		mListsOfPlants[4]->AddToSetList( 60, GetGrassBlock );
		mListsOfPlants[4]->AddToSetList( 7, GetHepaticaBlock );
		mListsOfPlants[4]->AddToSetList( 10, GetCloverLeafBlock );
		//mListsOfPlants[4]->AddToSetList( 2, GetCloverShortBlock );
		//mListsOfPlants[4]->AddToSetList( 2, GetCloverBlock );
		mListsOfPlants[4]->AddToSetList( 3, GetCentaureaCyanusShortBlock );
		mListsOfPlants[4]->AddToSetList( 1, GetCentaureaCyanusBlock );

		//According to number of level we set defined couple of lists.
		switch( mLevel )
		{
			case 1:
			{
				mRandomizerList1 = mListsOfPlants[1];
				mRandomizerList2 = mListsOfPlants[2];
				break;
			}
			case 2:
			{
				mRandomizerList1 = mListsOfPlants[3];
				mRandomizerList2 = mListsOfPlants[4];
				break;
			}
			case 3:
			{
				mRandomizerList1 = mListsOfPlants[3];
				mRandomizerList2 = mListsOfPlants[2];
				break;
			}
		}


		XMINT2 pos;
		for( int z = -1; z < 4; ++z )
		{
			for( int x = -1; x < 4; ++x )
			{
				pos = XMINT2( x, z );

				std::random_device randomDevice;
				std::default_random_engine randomGenerator( randomDevice() );
				std::uniform_int_distribution<int> distanceDistributionX( -5, 5 );
				std::uniform_int_distribution<int> distanceDistributionZ( -5, 5 );

				int dx = distanceDistributionX( randomGenerator );
				int dz = distanceDistributionZ( randomGenerator );
				
				if( z == -1 || z == 3 )
				{
					Block* block = mRandomizerList2->GetRandomNode()();
					if( block )
					{
						mBlocks.push_back( block );
						mBlocks.back()->Initialize();
						mBlocks.back()->SetGridPos( pos );
						mBlocks.back()->AttachTransitionTo( this, XMFLOAT3( pos.x * 20.0f + dx, 0.0f, pos.y * 20.0f + dz ) );
					}
				}
				else
				{
					Block* block = mRandomizerList1->GetRandomNode()();
					if( block )
					{
						mBlocks.push_back( block );
						mBlocks.back()->Initialize();
						mBlocks.back()->SetGridPos( pos );
						mBlocks.back()->AttachTransitionTo( this, XMFLOAT3( pos.x * 20.0f + dx, 0.0f, pos.y * 20.0f + dz ) );
					}
				}
			}
		}

	}
	void LawnBlockPath::CreateChamomileBlock( XMFLOAT3 position )
	{
		mBlocks.push_back( new ChamomileBlock( *mGame, *mCamera, mDirectionalLightSource ) );
		mBlocks.back()->Initialize();
		mBlocks.back()->SetPosition( position );
	}
}