#include "StartScene.h"
#include "RenderStateHelper.h"
#include <sstream>
#include <iomanip>
#include <SpriteBatch.h>
#include <SpriteFont.h>
#include "Game.h"
#include "Utility.h"
#include "Keyboard.h"
#include "Camera.h"
#include "ThirdPersonCameraController.h"
#include "FirstPersonCameraController.h"
#include "DirectionalLight.h"
#include "IntermediateScene.h"
#include "Level1.h"
#include "Level2.h"
#include "Level3.h"
#include "ColorHelper.h"

#include "Logo.h"
#include "LadybugNPC.h"

namespace Rendering
{
	RTTI_DEFINITIONS( StartScene )

	StartScene::StartScene( Game& game, SceneManager& sceneManager )
		:
		Scene( game, sceneManager ),
		mSpriteBatch( nullptr ),
		mSpriteFont( nullptr ),
		mRenderStateHelper( nullptr ),
		mKeyboard( nullptr ),
		mCamera( nullptr ),
		mCameraController( nullptr ),
		mDirectionalLight( nullptr ),
		mTimer( 0.0f ),
		mLogo( nullptr ),
		mLadybugNPC( nullptr )
	{
	}
	StartScene::~StartScene()
	{
		DeleteObject( mLadybugNPC );
		DeleteObject( mLogo );
		DeleteObject( mCameraController );
		DeleteObject( mCamera );
		DeleteObject( mRenderStateHelper );
		DeleteObject( mSpriteFont );
		DeleteObject( mSpriteBatch );
	}

	void StartScene::Initialize()
	{
		SetCurrentDirectory( Utility::ExecutableDirectory().c_str() );

		mSpriteBatch = new SpriteBatch( mGame->Direct3DDeviceContext() );
		mSpriteFont = new SpriteFont( mGame->Direct3DDevice(), L"Content\\Fonts\\Berlin_Sans_FB_Demi_14_Bold.spritefont" );
		mRenderStateHelper = new RenderStateHelper( *mGame );
		mKeyboard = (Keyboard*)mGame->Services().GetService( Keyboard::TypeIdClass() );

		mCamera = new Camera( *mGame );
		mCamera->Initialize();

		mCameraController = new FirstPersonCameraController( *mGame, *mCamera );
		mCameraController->Initialize();
		mCameraController->SetPosition( 0.0f, 5.0f, 0.0f );
		mCameraController->ApplyRotation( XMMatrixRotationX( -XM_PIDIV2 ) );
	
		mDirectionalLight = new DirectionalLight( *mGame );
		mDirectionalLight->Initialize();
		mDirectionalLight->SetRotation( XMFLOAT3( -15.0f, 0.0f, 0.0f ) );

		mLogo = new Logo( *mGame, *mCamera );
		mLogo->Initialize();
		mLogo->SetScale( XMFLOAT3(-2.0f, 1.0f, -2.0f) );

		mLadybugNPC = new LadybugNPC( *mGame, *mCamera, *mDirectionalLight );

		mLadybugNPC->Initialize();
		mLadybugNPC->SetPosition( 0.54f, 0.0f, -0.2f );
		mLadybugNPC->SetScale( XMFLOAT3( 0.04f, 0.04f, 0.04f ) );
		mLadybugNPC->SetRotation( XMFLOAT3( 0.0f, 0.0f, 0.0f ) );
		mLadybugNPC->SetIsRunning( true );
	}

	void StartScene::Update( const GameTime & gameTime )
	{
		mCamera->Update( gameTime );

		mLadybugNPC->Update( gameTime );

		mTimer += gameTime.ElapsedGameTime();
		mLogo->Update( gameTime );
		if( mTimer >= 10.0f )
		{
			mLevelName = Scene::LevelName::LevelName1;
			SceneSwitching( new IntermediateScene( *mGame, mSceneManager ) );
		}
	}

	void StartScene::Draw( const GameTime & gameTime )
	{
		mLogo->Draw( gameTime );
		mLadybugNPC->Draw( gameTime );
		mRenderStateHelper->SaveAll();
		mSpriteBatch->Begin();

		mSpriteFont->DrawString( mSpriteBatch, L"www.alexanderobzherin.info", 
								XMFLOAT2( (int)((mGame->ScreenWidth() / 2.0f) - 130.0f ), (mGame->ScreenHeight() - 80)), 
											/*Color*/ XMVECTORF32( /*240 240 240*/{ 0.9f, 0.9f, 0.9f, 1.0f } ) );

		mSpriteBatch->End();

		mRenderStateHelper->RestoreAll();



	}
}