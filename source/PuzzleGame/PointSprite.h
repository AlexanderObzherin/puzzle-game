#pragma once

#include "RenderStateHelper.h"
#include "Actor.h"

using namespace Library;

namespace Library
{
	class Effect;
	class Pass;
}

namespace DirectX
{
	class SpriteBatch;
	class SpriteFont;
}

namespace Rendering
{
	class PointSpriteMaterial;

	class PointSprite
	{
	public:
		PointSprite( Actor& actor );
		~PointSprite();

		void Initialize();
		void Update( const GameTime& gameTime );
		void Draw( const GameTime& gameTime );

		void SetTexture( std::wstring textureName );

	protected:
		Effect* mEffect;
		PointSpriteMaterial* mMaterial;
		Pass* mPass;
		ID3D11InputLayout* mInputLayout;
		ID3D11Buffer* mVertexBuffer;
		UINT mVertexCount;
		ID3D11ShaderResourceView* mColorTexture;
		SpriteBatch* mSpriteBatch;

		std::wstring mTextureName;

	private:
		Actor& mActor;
	};
}