#pragma once
#include "Insect.h"
#include "VectorHelper.h"
#include "Level1.h"

using namespace Library;

namespace Library
{
	class DirectionalLight;
}

namespace Rendering
{
	class LadybugNPC : public Insect
	{
		RTTI_DECLARATIONS( LadybugNPC, Insect )

	public:
		LadybugNPC( Game& game, Camera& camera, const DirectionalLight& directionalLightSource );
		~LadybugNPC();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		void Run( const GameTime& gameTime );
		void Stop( const GameTime& gameTime );
		bool GetIsRunning() const;
		void SetIsRunning( bool in )
		{
			bIsRunning = in;
		}

	private:
		bool bIsRunning;


	};
}
