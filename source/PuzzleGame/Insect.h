#pragma once

#include "Actor.h"
#include <list>
#include "VectorHelper.h"
#include "Level1.h"

using namespace Library;

namespace Library
{
	class DirectionalLight;
}

namespace Rendering
{
	class AnimatedModel;
	class BlockPath;
	class Block;
	class Point;

	class Insect : public Actor
	{
		RTTI_DECLARATIONS( Insect, Actor )

	public:
		Insect( Game& game, Camera& camera, const DirectionalLight& directionalLightSource );
		~Insect();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		void SetBlockPath( BlockPath* blockPath );
		void SetTargetPoint( Point* targetPoint )
		{
			mTargetPoint = targetPoint;
		}
		float GetDistanceFromTargetPoint() const
		{
			return mDistanceFromTargetPoint;
		}
		void TurnBack()
		{
			auto func = [&]()
			{
				mTargetPoint = mLastPoint;
				mTurnBack = nullptr;
			};
			mTurnBack = func;
		}

		virtual void PlayWalkingScenario() {}

		//Creates random pos over the current lawn
		XMFLOAT3 CreateRandomPosition();

	private:
		Insect();
		Insect( const Insect& rhs );
		Insect& operator=( const Insect& rhs );

	protected:
		static const XMVECTOR CrossProduct( XMVECTOR v1, XMVECTOR v2 );

		//Increase position with mVelocityDirection
		void Move( const GameTime& gameTime );
		Point* SearchForClosestFreePoint( float closestDistance = D3D11_FLOAT32_MAX );

		//Method find new target point afterthat define velocity direction, target angle, etc.
		//For playable characters who walks on game grids
		void WalkBetweenPoints();
		virtual void SmoothRotation( const GameTime& gameTime );

		//Normalize vectors before passing
		float GetAngle( const XMVECTOR& a, const XMVECTOR& b );
		XMFLOAT3 GetRotator( XMFLOAT3 vec );

	protected:
		const DirectionalLight& mDirectionalLightSource;

		std::unique_ptr<AnimatedModel> mAnimatedModel;
		BlockPath* mCurrentBlockPath;

		XMFLOAT3 mVelocityDirection;
		float mVelocityFactor;

		float mTargetYaw;
		float mCurrentYaw;
		float mAngularVelocity;

		float mDistanceFromTargetPoint;
		std::function< void() > mTurnBack;

		//Variables to store queue of visited points
		Point* mBeforeLast;
		Point* mLastPoint;
		Point* mCurrentPoint;
		Point* mTargetPoint;

		std::function< void( const GameTime& gameTime ) > mActionState;

	};
}