#include "GastrophysaPolygoni.h"

#include "AnimatedModel.h"

namespace Rendering
{
	RTTI_DEFINITIONS( GastrophysaPolygoni )

		GastrophysaPolygoni::GastrophysaPolygoni( Game& game, Camera& camera, const DirectionalLight& directionalLightSource )
		:
		Insect( game, camera, directionalLightSource ),
		bIsRunning( false )
	{}
	GastrophysaPolygoni::~GastrophysaPolygoni()
	{
	}

	void GastrophysaPolygoni::Initialize()
	{
		Insect::Initialize();

		mAnimatedModel = std::unique_ptr<AnimatedModel>( new AnimatedModel( *this, mDirectionalLightSource ) );
		mAnimatedModel->SetModelName( "Content\\Models\\GastrophysaPolygoni\\GastrophysaPolygoni.dae" );
		mAnimatedModel->SetColorTexture( L"Content\\Models\\GastrophysaPolygoni\\GastrophysaPolygoniColorTexture.png" );
		mAnimatedModel->SetNormalMapTexture( L"Content\\Models\\GastrophysaPolygoni\\GastrophysaPolygoniNormalMapTexture.png" );
		mAnimatedModel->Initialize();
		mAnimatedModel->DrawPhongWithNormalMap();

		mAnimatedModel->AddAnimation( "GastrophysaPolygoniMoving", 0.0f, 1.1f );
		mAnimatedModel->AddAnimation( "GastrophysaPolygoniWingsMoving", 1.1f, 5.0f );
		mAnimatedModel->SetAnimation( "GastrophysaPolygoniMoving" );

		SetScale( XMFLOAT3( 0.6f, 0.6f, 0.6f ) );
	}
	void GastrophysaPolygoni::Update( const GameTime & gameTime )
	{
		Insect::Update( gameTime );
		if( !bIsRunning || !mCurrentBlockPath )
		{
			Stop( gameTime );
		}
		else
		{
			Run( gameTime );
		}

	}
	void GastrophysaPolygoni::Draw( const GameTime & gameTime )
	{
		Insect::Draw( gameTime );

	}

	void GastrophysaPolygoni::Run( const GameTime & gameTime )
	{
		mAnimatedModel->SetAnimation( "GastrophysaPolygoniMoving" );
		WalkBetweenPoints();
		Move( gameTime );
		SmoothRotation( gameTime );
	}
	void GastrophysaPolygoni::Stop( const GameTime & gameTime )
	{
		mAnimatedModel->SetAnimation( "GastrophysaPolygoniWingsMoving" );
	}
	bool GastrophysaPolygoni::GetIsRunning() const
	{
		return bIsRunning;
	}


}