#pragma once

#include "RenderStateHelper.h"
#include "Actor.h"
#include <list>
#include <functional>

using namespace Library;



namespace Rendering
{
	class PointSprite;

	class SunDotStar : public Actor
	{
		RTTI_DECLARATIONS( SunDotStar, Actor )

	public:
		SunDotStar( Game& game, Camera& camera, std::wstring texture );
		~SunDotStar();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		void SetVelocity( XMFLOAT3 velocity )
		{
			mVelocity = velocity;
		}

	protected:
		PointSprite* mPointSprite;
		std::wstring mTexture;

		XMFLOAT3 mVelocity;
	};

	//To add this PE in level:
	//We do as we do for any other drawable actors:
	//mPE->Initialize();
	//mPE->Update( gameTime );
	//mPE->Draw( gameTime );
	//Use method PlayEffect() to play it once.

	class ParticleEffect : public Actor
	{
		RTTI_DECLARATIONS( ParticleEffect, Actor )

	public:
		ParticleEffect( Game& game, Camera& camera );
		~ParticleEffect();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		void PlayEffect();

	protected:
		void CreateParticles( const GameTime& gameTime );
		void DestroyParticles();
		void AdvanceEffect( const GameTime& gameTime );

		float mRunningTime;

		SunDotStar* mSunDotStar1;
		float mStar1Angle;
		float mStar1Scale;

		SunDotStar* mSunDotStar2;
		float mStar2Angle;
		float mStar2Scale;

		std::function< void( const GameTime& gameTime ) > mEffectState;

	};
}
