#pragma once

#include "Actor.h"

using namespace Library;

namespace Library
{
	class DirectionalLight;
}

namespace Rendering
{
	class StaticModel;

	class Chamomile : public Actor
	{
		RTTI_DECLARATIONS( Chamomile, Actor )

	public:
		Chamomile( Game& game, Camera& camera, const DirectionalLight& directionalLightSource );
		~Chamomile();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

	private:
		Chamomile();
		Chamomile( const Chamomile& rhs );
		Chamomile& operator=( const Chamomile& rhs );

		const DirectionalLight& mDirectionalLightSource;
		StaticModel* mShape;
	};
}