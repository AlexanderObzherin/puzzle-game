#pragma once

#include "BlockPath.h"
#include <vector>

using namespace Library;

namespace Library
{
	class Keyboard;
	class DirectionalLight;
}

namespace Rendering
{
	class Shape;
	class Block;
	class AudioPlayer;

	enum GameGridSize 
	{
		size3x3 = 0,
		size4x4
	};

	class GameGrid : public BlockPath
	{
		RTTI_DECLARATIONS( GameGrid, BlockPath )

	public:
		GameGrid( Game& game, Camera& camera, const DirectionalLight& directionalLightSource, GameGridSize gridSize );
		~GameGrid();

		const Keyboard& GetKeyboard() const;

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		XMFLOAT3 GetCenter() const;
		Block* GetStartBlock();
		Block* GetFinishBlock();

		AudioPlayer* pAudioPlayer;
	protected:
		void SetDimensions( int rows, int columns );
		bool IsOccupied( XMINT2 pos ); //Return true if pos is occupied
		XMINT2 FindFreeSpace(); //returns 2d coordinate which is not occupied
		XMINT2 GenerateNewPosition();
		void CreateGrid();
		void CreateStartBlock();
		void CreateFinishBlock();
		void SwitchActiveBlock();
		Block* GetBlockAtPos( XMINT2 pos );
		Block* GetActiveBlock();
		void MoveBlocks( const GameTime& gameTime );
		void PullToPivot( const GameTime& gameTime );

	private:
		GameGrid();
		GameGrid( const GameGrid& rhs );
		GameGrid& operator=( const GameGrid& rhs );
	
	protected:
		Keyboard* mKeyboard;
		
		GameGridSize mGridSize;

		static int mStartFinishRow;

		//Num of rows and columns
		int mRows;
		int mColumns;

		//Uses at grid creating. It defines distance between blocks
		float mGridScale;
		//Uses to store coordinate of active block. Sometimes there is needed to point to block by grid coordinate.
		XMINT2 mBlockIterator;
		XMINT2 mStartBlockCoord;
		XMINT2 mFinishBlockCoord;

		

	};
}