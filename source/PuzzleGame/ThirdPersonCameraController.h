#pragma once

#include "Game.h"
#include "Camera.h"
#include "CameraController.h"
#include "Actor.h"

using namespace Library;

namespace Rendering
{
	enum CameraViewAngle
	{
		bottom = 0,
		angleMinus45deg,
		side,
		angle45deg,
		top
	};

	class ThirdPersonCameraController : public CameraController
	{
		RTTI_DECLARATIONS( ThirdPersonCameraController, CameraController )

	public:
		ThirdPersonCameraController( Game& game, Camera& camera );
		~ThirdPersonCameraController();

		virtual void Update( const GameTime& gameTime ) override;

		void SetObservableActor( Actor* observableActor );
		void SwitchAngleView( const GameTime & gameTime );
		void SetAngleView( CameraViewAngle newAngleView );
		//Mutator to set new target distance
		void SetDistance( float distance );
		void HandleCameraManipulation();

	protected:
		void RadialRotation( const GameTime& gameTime );
		void LinearMovingToTargetPoint( const GameTime& gameTime );
		
		Actor* mObservableActor;

		float mCurrentPolarAngle;
		float mTargetPolarAngle;
		float mAngularVelocity;
		float mLinearVelocityFactor;

		//Vector variable to store distance and view vector between camera point and observable point 
		XMFLOAT3 mViewVector;
		CameraViewAngle mCameraViewAngle;
	};
}