#include "LadybugPlayable.h"

#include "AnimatedModel.h"

namespace Rendering
{
	RTTI_DEFINITIONS( LadybugPlayable )

	LadybugPlayable::LadybugPlayable( Game& game, Camera& camera, const DirectionalLight& directionalLightSource )
		:
		Insect( game, camera, directionalLightSource ),
		bIsRunning( false )
	{}
	LadybugPlayable::~LadybugPlayable()
	{
	}

	void LadybugPlayable::Initialize()
	{
		Insect::Initialize();

		SetScale( XMFLOAT3( 0.6f, 0.6f, 0.6f ) );

		mAnimatedModel = std::unique_ptr<AnimatedModel>( new AnimatedModel( *this, mDirectionalLightSource ) );
		mAnimatedModel->SetModelName( "Content\\Models\\Ladybug\\Ladybug.dae" );
		mAnimatedModel->SetColorTexture( L"Content\\Models\\Ladybug\\LadybugColorTexture.png" );
		mAnimatedModel->SetNormalMapTexture( L"Content\\Models\\Ladybug\\LadybugNormalMapTexture.png" );
		mAnimatedModel->Initialize();
		mAnimatedModel->DrawPhongWithNormalMap();

		mAnimatedModel->AddAnimation( "LadybugMoving", 0.0f, 0.7f );
		mAnimatedModel->AddAnimation( "LadybugWingsMoving", 1.1f, 7.0f );
		mAnimatedModel->SetAnimation( "LadybugMoving" );
	}
	void LadybugPlayable::Update( const GameTime & gameTime )
	{
		Insect::Update( gameTime );
		if( !bIsRunning || !mCurrentBlockPath )
		{
			Stop( gameTime );
		}
		else
		{
			Run( gameTime );
		}
		
	}
	void LadybugPlayable::Draw( const GameTime & gameTime )
	{
		Insect::Draw( gameTime );
		
	}

	void LadybugPlayable::Run( const GameTime & gameTime )
	{
		mAnimatedModel->SetAnimation( "LadybugMoving" );
		WalkBetweenPoints();
		Move( gameTime );
		SmoothRotation( gameTime );
	}
	void LadybugPlayable::Stop( const GameTime & gameTime )
	{
		mAnimatedModel->SetAnimation( "LadybugWingsMoving" );
	}
	bool LadybugPlayable::GetIsRunning() const
	{
		return bIsRunning;
	}


}