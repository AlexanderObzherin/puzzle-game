#include "BlockPath.h"
#include "Block.h"

namespace Rendering
{
	RTTI_DEFINITIONS( BlockPath );

	BlockPath::BlockPath( Game& game, Camera& camera, const DirectionalLight& directionalLightSource )
		:
		Actor( game, camera ),
		mDirectionalLightSource( directionalLightSource )
	{}


	BlockPath::~BlockPath()
	{
		for( auto& block : mBlocks )
		{
			DeleteObject( block );
		}
	}

	void BlockPath::Initialize()
	{
		Actor::Initialize();
	}

	void BlockPath::Update( const GameTime & gameTime )
	{
		Actor::Update( gameTime );

		for( auto& block : mBlocks )
		{
			block->Update( gameTime );
		}
	}

	void BlockPath::Draw( const GameTime & gameTime )
	{
		for( auto& block : mBlocks )
		{
			block->Draw( gameTime );
		}
	}

	std::list<Block*>& BlockPath::GetBlocks()
	{
		return mBlocks;
	}

}