#pragma once

#include "Actor.h"

using namespace Library;

namespace Library
{
	class DirectionalLight;
}

namespace Rendering
{
	class StaticModel;

	class RedClover : public Actor
	{
		RTTI_DECLARATIONS( RedClover, Actor )

	public:
		RedClover( Game& game, Camera& camera, const DirectionalLight& directionalLightSource );
		~RedClover();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

	private:
		RedClover();
		RedClover( const RedClover& rhs );
		RedClover& operator=( const RedClover& rhs );

		const DirectionalLight& mDirectionalLightSource;
		StaticModel* mShape;
	};
}