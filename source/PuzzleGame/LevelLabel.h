#pragma once

#include "Actor.h"
#include <string>

using namespace Library;

namespace Rendering
{
	class PointSprite;

	class LevelLabel : public Actor
	{
		RTTI_DECLARATIONS( LevelLabel, Actor )

	public:
		LevelLabel( Game& game, Camera& camera, std::wstring texture );
		~LevelLabel();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		void SetVelocity( XMFLOAT3 velocity )
		{
			mVelocity = velocity;
		}

	protected:
		PointSprite* mPointSprite;
		std::wstring mTexture;

		XMFLOAT3 mVelocity;
	};

}