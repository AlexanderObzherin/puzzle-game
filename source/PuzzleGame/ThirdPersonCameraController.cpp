#include "ThirdPersonCameraController.h"
#include "GameTime.h"
#include "Mouse.h"
#include "Keyboard.h"
#include "VectorHelper.h"

namespace Rendering
{
	RTTI_DEFINITIONS( ThirdPersonCameraController )

		ThirdPersonCameraController::ThirdPersonCameraController( Game& game, Camera& camera )
		:
		CameraController( game, camera ),
		mObservableActor( nullptr ),
		mCurrentPolarAngle( 0.0f ),
		mTargetPolarAngle( 0.0f ),
		mAngularVelocity( 0.4f ),
		mLinearVelocityFactor( 3.0f ),
		mViewVector( XMFLOAT3( 0.0f, 0.0f, 100.0f ) ),
		mCameraViewAngle( CameraViewAngle::side )
	{
	}

	ThirdPersonCameraController::~ThirdPersonCameraController()
	{

	}

	void ThirdPersonCameraController::Update( const GameTime & gameTime )
	{
		//Uncomment for edit purpose
		//if( Enabled() )
		//{
		//	HandleCameraManipulation();
		//}

		SwitchAngleView( gameTime );
		LinearMovingToTargetPoint( gameTime );
	}

	void ThirdPersonCameraController::SetObservableActor( Actor * observableActor )
	{
		mObservableActor = observableActor;
	}
	void ThirdPersonCameraController::SwitchAngleView( const GameTime & gameTime ) 
	{
		if( mCameraViewAngle == bottom )
		{
			mTargetPolarAngle = XM_PIDIV2;
		}
		if( mCameraViewAngle == angleMinus45deg )
		{
			mTargetPolarAngle = XM_PIDIV4;
		}
		if( mCameraViewAngle == side )
		{
			mTargetPolarAngle = 0.0f;
		}
		if( mCameraViewAngle == angle45deg )
		{
			mTargetPolarAngle = -XM_PIDIV4;
		}
		if( mCameraViewAngle == top )
		{
			mTargetPolarAngle = -XM_PIDIV2;
		}
		RadialRotation( gameTime );
	}
	void ThirdPersonCameraController::SetAngleView( CameraViewAngle newAngleView )
	{
		mCameraViewAngle = newAngleView;
	}
	void ThirdPersonCameraController::RadialRotation( const GameTime& gameTime )
	{
		XMFLOAT2 orbitMovementAmount = Vector2Helper::Zero;

		if( (int)(mTargetPolarAngle * 180 / XM_PI) != (int)(mCurrentPolarAngle * 180 / XM_PI) )
		{
			if( (mCurrentPolarAngle) < (mTargetPolarAngle) )
			{
				orbitMovementAmount.x = 1.0f;
			}
			else
			{
				orbitMovementAmount.x = -1.0f;
			}
		}

		XMMATRIX rotationAroundX = XMMatrixRotationX( orbitMovementAmount.x * mAngularVelocity * fabs( mCurrentPolarAngle - mTargetPolarAngle ) * gameTime.ElapsedGameTime() );
		mCurrentPolarAngle += orbitMovementAmount.x * mAngularVelocity * fabs( mCurrentPolarAngle - mTargetPolarAngle ) * gameTime.ElapsedGameTime();

		XMVECTOR viewVector = XMLoadFloat3( &mViewVector );
		viewVector = XMVector3Transform( viewVector, rotationAroundX );
		XMStoreFloat3( &mViewVector, viewVector );
		ApplyRotation( rotationAroundX );
	}
	void ThirdPersonCameraController::SetDistance( float distance )
	{
		XMVECTOR viewVector = XMLoadFloat3( &mViewVector );
		float currentDistance = XMVector3Length( viewVector ).m128_f32[0];

		viewVector *= ( distance / currentDistance );
		XMStoreFloat3( &mViewVector, viewVector );
	}
	void ThirdPersonCameraController::HandleCameraManipulation()
	{
		if( mKeyboard != nullptr )
		{
			if( mKeyboard->WasKeyPressedThisFrame( DIK_T ) )
			{
				if( mCameraViewAngle != top )
				{
					mCameraViewAngle = CameraViewAngle( mCameraViewAngle + 1 );
				}
			}
			if( mKeyboard->WasKeyPressedThisFrame( DIK_G ) )
			{
				if( mCameraViewAngle != bottom )
				{
					mCameraViewAngle = CameraViewAngle( mCameraViewAngle - 1 );
				}
			}


			//switch distance
			if( mKeyboard->WasKeyPressedThisFrame( DIK_Y ) )
			{
				float distance = XMVector3Length( XMLoadFloat3( &mViewVector ) ).m128_f32[0];
				distance -= 30.0f;
				SetDistance( distance );
			}
			if( mKeyboard->WasKeyPressedThisFrame( DIK_H ) )
			{
				float distance = XMVector3Length( XMLoadFloat3( &mViewVector ) ).m128_f32[0];
				distance += 30.0f;
				SetDistance( distance );
			}
		}
	}
	void ThirdPersonCameraController::LinearMovingToTargetPoint( const GameTime & gameTime )
	{
		XMVECTOR currentPosition = mCamera.PositionVector();
		XMVECTOR targetPosition = mObservableActor->PositionVector() + XMLoadFloat3( &mViewVector );

		XMVECTOR distanceVector = targetPosition - currentPosition;
		float distanceFloat = XMVector3Length( distanceVector ).m128_f32[0];
		
		if( (int)distanceFloat > 0 )
		{
			SetPosition( currentPosition + mLinearVelocityFactor * distanceVector * gameTime.ElapsedGameTime() );
		}
	}
}