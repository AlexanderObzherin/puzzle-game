#pragma once
#include "Actor.h"

using namespace Library;

namespace Library
{
	class DirectionalLight;
}

namespace Rendering
{
	class StaticModel;

	class CentaureaCyanusShort : public Actor
	{
		RTTI_DECLARATIONS( CentaureaCyanusShort, Actor )

	public:
		CentaureaCyanusShort( Game& game, Camera& camera, const DirectionalLight& directionalLightSource );
		~CentaureaCyanusShort();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

	private:
		CentaureaCyanusShort();
		CentaureaCyanusShort( const CentaureaCyanusShort& rhs );
		CentaureaCyanusShort& operator=( const CentaureaCyanusShort& rhs );

		const DirectionalLight& mDirectionalLightSource;
		StaticModel* mShape;
	};
}