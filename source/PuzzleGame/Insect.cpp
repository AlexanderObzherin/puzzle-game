#include "Insect.h"

#include "AnimatedModel.h"
#include "LawnBlockPath.h"
#include "GameGrid.h"
#include "Block.h"
#include "Point.h"
#include "GameException.h"
#include "GameTime.h"
#include "VectorHelper.h"

namespace Rendering
{
	RTTI_DEFINITIONS( Insect )

		Insect::Insect( Game& game, Camera& camera, const DirectionalLight& directionalLightSource )
		:
		Actor( game, camera ),
		mDirectionalLightSource( directionalLightSource ),
		mAnimatedModel( nullptr ),
		mCurrentBlockPath( nullptr ),
		mVelocityDirection( Vector3Helper::Zero ),
		mVelocityFactor( 6.0f ),
		mTargetYaw( -90.0f ),
		mCurrentYaw( -90.0f ),
		mAngularVelocity( 85.0f ),
		mDistanceFromTargetPoint( 0.0f ),
		mBeforeLast( nullptr ),
		mLastPoint( nullptr ),
		mCurrentPoint( nullptr ),
		mTargetPoint( nullptr )
	{
	}
	Insect::~Insect()
	{
	}

	void Insect::Initialize()
	{
		Actor::Initialize();
		SetRotation( XMFLOAT3( 0.0f, mCurrentYaw, 0.0f ) );
	}
	void Insect::Update( const GameTime& gameTime )
	{
		if ( mAnimatedModel.get() )
		{
			mAnimatedModel->Update( gameTime );
		}
		if( mTurnBack )
		{
			mTurnBack();
		}
		
		if( mActionState )
		{
			mActionState( gameTime );
		}
		Actor::Update( gameTime );
	}
	void Insect::Draw( const GameTime& gameTime )
	{
		if ( mAnimatedModel.get() )
		{
			mAnimatedModel->Draw( gameTime );
		}
	}

	void Insect::SetBlockPath( BlockPath * blockPath )
	{
		mBeforeLast = nullptr;
		mLastPoint = nullptr;
		mCurrentPoint = nullptr;
		mTargetPoint = nullptr;
		mCurrentBlockPath = blockPath;
	}
	XMFLOAT3 Insect::CreateRandomPosition()
	{
		std::random_device randomDevice;
		std::default_random_engine randomGenerator( randomDevice() );
		std::uniform_int_distribution<int> distanceDistributionX( -100, 100 );
		std::uniform_int_distribution<int> distanceDistributionZ( -100, 100 );

		int xDeviation = distanceDistributionX( randomGenerator );
		int zDeviation = distanceDistributionZ( randomGenerator );

		return XMFLOAT3(
			dynamic_cast<LawnBlockPath*>(mCurrentBlockPath)->GetCenter().x + xDeviation,
			dynamic_cast<LawnBlockPath*>(mCurrentBlockPath)->GetCenter().y + 150,
			dynamic_cast<LawnBlockPath*>(mCurrentBlockPath)->GetCenter().z + zDeviation
		);
	}
	const XMVECTOR Insect::CrossProduct( XMVECTOR v1, XMVECTOR v2 )
	{
		float i = (v1.m128_f32[1] * v2.m128_f32[2]) - (v1.m128_f32[2] * v2.m128_f32[1]);
		float j = (v1.m128_f32[2] * v2.m128_f32[0]) - (v1.m128_f32[0] * v2.m128_f32[2]);
		float k = (v1.m128_f32[0] * v2.m128_f32[1]) - (v1.m128_f32[1] * v2.m128_f32[0]);
		
		return XMLoadFloat3(&XMFLOAT3(i, j, k));
	}
	void Insect::Move( const GameTime& gameTime )
	{
		XMVECTOR curPosition = PositionVector();
		XMVECTOR velocity = XMLoadFloat3( &mVelocityDirection ) * mVelocityFactor * (float)gameTime.ElapsedGameTime();
		SetPosition( curPosition + velocity );
	}
	Point* Insect::SearchForClosestFreePoint( float closestDistance )
	{
		Point* closestPoint = nullptr;

		for( auto& blockIterator : mCurrentBlockPath->GetBlocks() )
		{
			if( !blockIterator->GetIsOwnedByInsect() )
			{
				for( auto& pointIterator : blockIterator->GetPointPath() )
				{
					XMVECTOR distanceVector = this->PositionVector() - pointIterator->PositionVector();
					float distance = XMVector3Length( distanceVector ).m128_f32[0];
					if( distance < closestDistance )
					{
						closestDistance = distance;
						closestPoint = pointIterator;
					}
				}
			}
		}
		return closestPoint;
	}
	void Insect::WalkBetweenPoints()
	{
		if( mCurrentBlockPath )
		{
			//If mTargetPoint has not been set - search closest point to Insect position
			//this statement assumed to be executed only once in the start of the game
			if( mCurrentPoint == nullptr )
			{
				Point* closestPoint = nullptr;
				float closestDistance = D3D11_FLOAT32_MAX;

				for( auto& blockIterator : mCurrentBlockPath->GetBlocks() )
				{
					for( auto& pointIterator : blockIterator->GetPointPath() )
					{
						XMVECTOR distanceVector = this->PositionVector() - pointIterator->PositionVector();
						XMVECTOR distanceVectorSq = XMVector3Dot( distanceVector, distanceVector );
						float distance = sqrtf( distanceVectorSq.m128_f32[0] + distanceVectorSq.m128_f32[1] + distanceVectorSq.m128_f32[2] );
						mDistanceFromTargetPoint = distance;

						if( distance < closestDistance )
						{
							closestDistance = distance;
							closestPoint = pointIterator;
						}
					}
				}
				//after we find closest point, we set it as mTargetPoint
				mTargetPoint = closestPoint;
			}

			//now we move to target point until gain it
			//first check if distance between insect and target point more than 0, if so - move to that
			XMVECTOR distanceVector = mTargetPoint->PositionVector() - this->PositionVector();
			XMVECTOR distanceVectorSq = XMVector3Dot( distanceVector, distanceVector );
			float distance = sqrtf( distanceVectorSq.m128_f32[0] + distanceVectorSq.m128_f32[1] + distanceVectorSq.m128_f32[2] );
			mDistanceFromTargetPoint = distance;
			if( (int)distance > 0 )
			{
				XMStoreFloat3( &mVelocityDirection, XMVector3Normalize( distanceVector ) );

				if( mTargetPoint && mLastPoint )
				{
					mLastPoint->GetBlock().SetIsOwnedByInsect( false );
					mTargetPoint->GetBlock().SetIsOwnedByInsect( true );
				}
			}
			else
			{
				mBeforeLast = mLastPoint;
				mLastPoint = mCurrentPoint;
				mCurrentPoint = mTargetPoint;
				mTargetPoint = nullptr;
			}

			//Now we need to search new target point
			if( mTargetPoint == nullptr )
			{
				Point* closestPoint = nullptr;
				float closestDistance = 15.0f;

				for( auto& blockIterator : mCurrentBlockPath->GetBlocks() )
				{
					for( auto& pointIterator : blockIterator->GetPointPath() )
					{
						if( pointIterator != mBeforeLast && pointIterator != mLastPoint && pointIterator != mCurrentPoint )
						{
							XMVECTOR distanceVector = this->PositionVector() - pointIterator->PositionVector();
							XMVECTOR distanceVectorSq = XMVector3Dot( distanceVector, distanceVector );
							float distance = sqrtf( distanceVectorSq.m128_f32[0] + distanceVectorSq.m128_f32[1] + distanceVectorSq.m128_f32[2] );
							mDistanceFromTargetPoint = distance;
							if( distance <= closestDistance )
							{
								closestDistance = distance;
								closestPoint = pointIterator;

							}
						}

					}
				}
				//If we didn't find new position in alowwable radius, then we set target as last point,
				//i.e. character turns and go back
				if( closestPoint == nullptr )
				{
					//mTargetPoint = mLastPoint;
					TurnBack();

				}
				else
				{
					mTargetPoint = closestPoint;

				}
			}

		}
	}
	void Insect::SmoothRotation( const GameTime& gameTime )
	{
		//Cross product between target and current doesn't work for now
		//Needed to be emproved to make minimal rotation

		mTargetYaw = GetRotator( mVelocityDirection ).y;
		
		if(  (int)mCurrentYaw > (int)mTargetYaw )
		{
			mCurrentYaw -= mAngularVelocity * gameTime.ElapsedGameTime();
		}
		else
		{
			mCurrentYaw += mAngularVelocity * gameTime.ElapsedGameTime();
		}

		SetRotation( XMFLOAT3( 0.0f, mCurrentYaw, 0.0f ) );
	}
	float Insect::GetAngle( const XMVECTOR & a, const XMVECTOR & b )
	{
		//XMVECTOR dot = XMVector3Dot( a, b );
		//float dotLength = dot.m128_f32[0];
		//XMVECTOR dotA = XMVector3Dot( a, a );
		//XMVECTOR dotB = XMVector3Dot( b, b );
		//float lengthA = dotA.m128_f32[0];
		//float lengthB = dotB.m128_f32[0];

		//return  (int)(acosf( dotLength / (lengthA * lengthB) ) * 180.0f / XM_PI);


		float dotAB = (a.m128_f32[0] * b.m128_f32[0]) + (a.m128_f32[1] * b.m128_f32[1]) + (a.m128_f32[2] * b.m128_f32[2]);
		float lengthA = sqrtf((a.m128_f32[0] * a.m128_f32[0]) + (a.m128_f32[1] * a.m128_f32[1]) + (a.m128_f32[2] * a.m128_f32[2]));
		float lengthB = sqrtf((b.m128_f32[0] * b.m128_f32[0]) + (b.m128_f32[1] * b.m128_f32[1]) + (b.m128_f32[2] * b.m128_f32[2]));
		
		return (int)( acosf( dotAB / ( lengthA * lengthB ) ) * 180.0f / XM_PI );
	}
	XMFLOAT3 Insect::GetRotator( XMFLOAT3 vec )
	{
		XMVECTOR dir = XMLoadFloat3( &vec );
		XMVECTOR forward = XMLoadFloat3( &Vector3Helper::Forward );

		//Detect Yaw
		float yaw;
		int yawSign = 1;
		//1. Normalize velocity Direction vector
		dir = XMVector3Normalize( dir );
		yaw = GetAngle( dir, forward );
		
		//Detect quarter for velocity vector
		yawSign = dir.m128_f32[0] < 0 ? 1 : -1;
		yaw *= yawSign;	

		return XMFLOAT3( 0 , yaw, 0  );
	}


}