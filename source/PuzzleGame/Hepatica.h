#pragma once
#include "Actor.h"

using namespace Library;

namespace Library
{
	class DirectionalLight;
}

namespace Rendering
{
	class StaticModel;

	class Hepatica : public Actor
	{
		RTTI_DECLARATIONS( Hepatica, Actor )

	public:
		Hepatica( Game& game, Camera& camera, const DirectionalLight& directionalLightSource );
		~Hepatica();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

	private:
		Hepatica();
		Hepatica( const Hepatica& rhs );
		Hepatica& operator=( const Hepatica& rhs );

		const DirectionalLight& mDirectionalLightSource;
		StaticModel* mShape;
	};
}