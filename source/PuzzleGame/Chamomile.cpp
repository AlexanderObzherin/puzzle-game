#include "Chamomile.h"
#include "StaticModel.h"

namespace Rendering
{
	RTTI_DEFINITIONS( Chamomile )

		Chamomile::Chamomile( Game& game, Camera& camera, const DirectionalLight& directionalLightSource )
		:
		Actor( game, camera ),
		mDirectionalLightSource( directionalLightSource ),
		mShape( nullptr )
	{
	}
	Chamomile::~Chamomile()
	{
		DeleteObject( mShape );
	}

	void Chamomile::Initialize()
	{
		SetScale( XMFLOAT3( 10.0f, 10.0f, 10.0f ) );
		Actor::Initialize();

		mShape = new StaticModel( *this, mDirectionalLightSource );
		mShape->SetModel( "Content\\Models\\Chamomile\\Chamomile.obj" );
		mShape->SetColorTexture( L"Content\\Models\\Chamomile\\ChamomileColorTexture.png" );
		mShape->SetNormalMapTexture( L"Content\\Models\\Chamomile\\ChamomileNormalMapTexture.png" );

		mShape->Initialize();
		mShape->DrawPhongWithNormalMap();
	}
	void Chamomile::Update( const GameTime& gameTime )
	{
		Actor::Update( gameTime );
	}
	void Chamomile::Draw( const GameTime& gameTime )
	{
		mShape->Draw( gameTime );
	}

}