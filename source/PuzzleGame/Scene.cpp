#include "Scene.h"

#include "ColorHelper.h"

namespace Rendering
{
	RTTI_DEFINITIONS( Scene )

	const XMVECTORF32 Scene::BackgroundColor = ColorHelper::CornflowerBlue;
	Scene::LevelName Scene::mLevelName;

	Scene::Scene( Game& game, SceneManager& sceneManager )
		:
		DrawableGameComponent( game ),
		mSceneManager( sceneManager )
	{
	}
	Scene::~Scene()
	{
	}
	void Scene::Initialize()
	{
	}
	void Scene::Update( const GameTime & gameTime )
	{
	}
	void Scene::Draw( const GameTime & gameTime )
	{
	}

	void Scene::SceneSwitching( Scene * newScene )
	{
		mSceneManager.SetCurrentScene( newScene );
		mSceneManager.GetCurrentScene().Initialize();
		delete this;
	}

}