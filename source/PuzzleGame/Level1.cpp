#include "Level1.h"
#include "GameException.h"
#include "WICTextureLoader.h"
#include "Level1.h"
#include "RenderStateHelper.h"
#include <sstream>
#include <iomanip>
#include <SpriteBatch.h>
#include <SpriteFont.h>
#include "Game.h"
#include "Utility.h"
#include "ColorHelper.h"
#include "Keyboard.h"
#include "Camera.h"
#include "ThirdPersonCameraController.h"
#include "FirstPersonCameraController.h"
#include "CameraTarget.h"

#include "LadybugPlayable.h"
#include "GameGrid.h"
#include "LawnBlockPath.h"

#include "Bumblebee.h"
#include "AglaisUrticae.h"
#include "NymphalisAntiopa.h"
#include "PierisBrassicae.h"
#include "PolyommatusSemiargus.h"
#include "PyrgusAndromedae.h"

#include "Level2.h"
#include "IntermediateScene.h"
#include "StartScene.h"

#include "Block.h"
#include "Point.h"
//
#include "Lawn.h"
#include "ParticleEffect.h"
#include "AudioPlayer.h"

#include "LevelLabel.h"

//for std::bind stuff. Is needed to bind functions with parameters.
using namespace std::placeholders;

namespace Rendering
{
	RTTI_DEFINITIONS( Level1 )

	Level1::Level1( Game& game, SceneManager& sceneManager )
		:
		LevelsCommon( game, sceneManager ),
	
		mStartMenuActiveOption( StartMenu::StartGame ),
		bStartMenuIsActive( false )
	{
	}
	Level1::~Level1()
	{
		DeleteObject( mFlyingInsectsList );
	}

	void Level1::Initialize()
	{
		//Set level's name displayed on screen
		mLevelNum = 1;

		//Set num of grids in level
		mNumberOfGameGrids = 13;

		//Level scenario defined as map of void func.
		//We walk through them in level by switching to determined function.
		mScenarioTerms["01"] = std::bind( &Level1::ScenarioTerm01, this, _1 );
		mScenarioTerms["02"] = std::bind( &Level1::ScenarioTerm02, this, _1 );
		mScenarioTerms["03"] = std::bind( &Level1::ScenarioTerm03, this, _1 );
		mScenarioTerms["04"] = std::bind( &Level1::ScenarioTerm04, this, _1 );
		mScenarioTerms["05"] = std::bind( &Level1::ScenarioTerm05, this, _1 );

		//Set start scenario term
		mCurrentScenarioTerm = mScenarioTerms["01"];

		//

		LevelsCommon::Initialize();

		////Create list for randomizer of Insect Spawner
		mFlyingInsectsList = new Randomizer<Insect>();
		auto CreateBumblebee = [&]() { return new Bumblebee( *mGame, *mCamera, *mDirectionalLight ); };
		mFlyingInsectsList->AddToSetList( 30, CreateBumblebee );
		auto CreateAglaisUrticae = [&]() { return new AglaisUrticae( *mGame, *mCamera, *mDirectionalLight ); };
		mFlyingInsectsList->AddToSetList( 30, CreateAglaisUrticae );
		auto CreateNymphalisAntiopa = [&]() { return new NymphalisAntiopa( *mGame, *mCamera, *mDirectionalLight ); };
		mFlyingInsectsList->AddToSetList( 30, CreateNymphalisAntiopa );
		auto CreatePierisBrassicae = [&]() { return new PierisBrassicae( *mGame, *mCamera, *mDirectionalLight ); };
		mFlyingInsectsList->AddToSetList( 30, CreatePierisBrassicae );
		auto CreatePolyommatusSemiargus = [&]() { return new PolyommatusSemiargus( *mGame, *mCamera, *mDirectionalLight ); };
		mFlyingInsectsList->AddToSetList( 30, CreatePolyommatusSemiargus );
		auto CreatePyrgusAndromedae = [&]() { return new PyrgusAndromedae( *mGame, *mCamera, *mDirectionalLight ); };
		mFlyingInsectsList->AddToSetList( 30, CreatePyrgusAndromedae );

		//Set levels label texture
		mAudioPlayer->PlayAudioBackground("Content\\Audio\\MorningInTheMagicalForest.mp3", 0.075f);

	}
	void Level1::Update( const GameTime & gameTime )
	{
		LevelsCommon::Update( gameTime );
	}
	void Level1::Draw( const GameTime & gameTime )
	{
		LevelsCommon::Draw( gameTime );

		mRenderStateHelper->SaveAll();
		if( bStartMenuIsActive )
		{
			///Start menu
			//Lambda return color for text according to active option
			auto ChoseColorFor = [=]( StartMenu menuActiveOption )
			{
				if( mStartMenuActiveOption & menuActiveOption )
				{
					return  XMVECTORF32( /*255 106 0*/{ 1.0f, 0.416f, 0.0f, 1.0f } );
				}
				else
				{
					return  XMVECTORF32( /*23 255 23*/{ 0.09f, 0.6f, 0.09f, 1.0f } );
				}
			};

			mSpriteBatch->Begin();
			mSpriteFont->DrawString( mSpriteBatch, L"Start Game", XMFLOAT2( (int)((mGame->ScreenWidth() / 2) - (16.6f * 10 / 2))/* - (letter w * num of let / 2)*/, 
																				(int)(mGame->ScreenHeight() / 5) ), ChoseColorFor( StartMenu::StartGame ) );
			mSpriteFont->DrawString( mSpriteBatch, L"Exit", XMFLOAT2( (int)((mGame->ScreenWidth() / 2) - (16.6f * 4 / 2))/* - (letter w * num of let / 2)*/,
				(int)((mGame->ScreenHeight() / 5) + 50) ), ChoseColorFor( StartMenu::ExitStartMenu ) );
			mSpriteBatch->End();
			////
		}
		mRenderStateHelper->RestoreAll();

	}

	void Level1::ScenarioTerm01( const GameTime & gameTime )
	{
		bIsItSpawning = false;

		//Start menu is activated
		bStartMenuIsActive = true;
		(*mGameGridIterator)->SetEnabled( false );

		mLadybugPlayable->SetPosition( (*mGameGridIterator)->GetStartBlock()->GetPointPath().at( 1 )->Position() );
		mLadybugPlayable->SetIsRunning( false );

		mCameraTarget->SetPosition( mLadybugPlayable->Position() );
		mCameraTarget->SetNewPosition( mLadybugPlayable->Position() );

		mCameraController->SetEnabled( false );
		mCameraController->SetDistance( 50.0f );
		mCameraController->SetAngleView( CameraViewAngle::angle45deg );
	
		//Switch to next
		SwitchScenarioToTerm( "02" );
	}
	void Level1::ScenarioTerm02( const GameTime & gameTime )
	{
		if( mKeyboard->WasKeyPressedThisFrame( DIK_UPARROW ) && !(mStartMenuActiveOption & StartMenu::StartGame) )
		{
			mStartMenuActiveOption = (StartMenu)(mStartMenuActiveOption - 1);
		}
		if( mKeyboard->WasKeyPressedThisFrame( DIK_DOWNARROW ) && !(mStartMenuActiveOption & StartMenu::ExitStartMenu) )
		{
			mStartMenuActiveOption = (StartMenu)(mStartMenuActiveOption + 1);
		}

		if( mKeyboard->WasKeyPressedThisFrame( DIK_RETURN ) )
		{
			if( (mStartMenuActiveOption & StartMenu::StartGame) )
			{
				SwitchScenarioToTerm( "03" );
			}
			if( (mStartMenuActiveOption & StartMenu::ExitStartMenu) )
			{
				mGame->Exit();
			}
		}
	}
	void Level1::ScenarioTerm03( const GameTime & gameTime )
	{
		bStartMenuIsActive = false;
		bGridIsPlaying = false;
		mLadybugWingMovingTimer = 0.0f;

		if( mGameGridIterator != mGameGrids.end() && mLawnBlockPathIterator != mLawnBlockPath.end() )
		{
			//Camera
			mCameraController->SetEnabled( true );
			mCameraController->SetDistance( 100.0f );
			mCameraController->SetAngleView( CameraViewAngle::top );

			//Set new game grid as enabled
			(*mGameGridIterator)->SetEnabled( true );

			//Set cameraTarget to move to this new grid
			mCameraTarget->SetNewPosition( dynamic_cast<GameGrid*>(*mGameGridIterator)->GetCenter() );

			//Switch Ladybug to walk through new gameGrid	
			mLadybugPlayable->SetBlockPath( *mGameGridIterator );
			mLadybugPlayable->SetPosition( (*mGameGridIterator)->GetStartBlock()->GetPointPath().at( 1 )->Position() );
			mLadybugPlayable->SetIsRunning( true );
		}

		bIsItSpawning = true;
		
		SwitchScenarioToTerm( "04" );
	}
	void Level1::ScenarioTerm04( const GameTime& gameTime )
	{
		//Menu
		if( mKeyboard->WasKeyPressedThisFrame( DIK_ESCAPE ) )
		{
			bGameIsPaused = !bGameIsPaused;

			mMenuBgScale = 1.0f;
		}
		if( mKeyboard->WasKeyPressedThisFrame( DIK_UPARROW ) && !(mMenuActiveOption & Menu::ReturnToGame) && bGameIsPaused )
		{
			mMenuActiveOption = (Menu)(mMenuActiveOption >> 1);
		}
		if( mKeyboard->WasKeyPressedThisFrame( DIK_DOWNARROW ) && !(mMenuActiveOption & Menu::ExitMenu) && bGameIsPaused )
		{
			mMenuActiveOption = (Menu)(mMenuActiveOption << 1);
		}
		if( mKeyboard->WasKeyPressedThisFrame( DIK_RETURN ) )
		{
			if( (mMenuActiveOption & Menu::ReturnToGame) )
			{
				bGameIsPaused = !bGameIsPaused;
			}
			if( (mMenuActiveOption & Menu::StartLevel) )
			{
				//Start this level again
				mLevelName = Scene::LevelName::LevelName1;
				SceneSwitching( new IntermediateScene( *mGame, mSceneManager ) );
				return;
			}
			if( (mMenuActiveOption & Menu::StartLevel1) )
			{
				//Start game from first level
				mLevelName = Scene::LevelName::LevelName1;
				SceneSwitching( new IntermediateScene( *mGame, mSceneManager ) );
				return;
			}
			if( (mMenuActiveOption & Menu::ExitMenu) )
			{
				//Exit to Windows
				mGame->Exit();
			}
		}
		///

		//Check if Ladybug collided with middle point of Finish Block
		XMVECTOR distanceVector = XMVector3Length( mLadybugPlayable->PositionVector()
			- (dynamic_cast<GameGrid*>(*mGameGridIterator))->GetBlocks().back()->GetPointPath().at( 1 )->PositionVector() );
		float distance = distanceVector.m128_f32[0];
		if( distance <= 2.0f )
		{
			if( *mGameGridIterator != mGameGrids.back() )
			{
				//Play audio eggect and particle effect once,
				//Wings moving for a few seconds and
				//Switch to next GameGrid
				if ( mLadybugPlayable->GetIsRunning() )
				{
					mParticleEffect->SetPosition(mLadybugPlayable->PositionVector());
					mParticleEffect->PlayEffect();

					if (!(mAudioPlayer->IsSamplePlaying("Content\\Audio\\BellTree.mp3")))
					{
						mAudioPlayer->PlaySample("Content\\Audio\\BellTree.mp3");
					}
				}

				mLadybugPlayable->SetIsRunning( false );
				mLadybugWingMovingTimer += gameTime.ElapsedGameTime();
			}
			else
			{
				mAudioPlayer->StopAudioBackGround();
				mAudioPlayer->PlaySample( "Content\\Audio\\Finish.mp3", false, 0.075f );
				SwitchScenarioToTerm( "05" );
			}

		}

		if( !bGridIsPlaying )
		{
			if( mCameraTarget->GetDistanceBetweenTarget() > 8.0f )
			{
				mCameraController->SetDistance( 160.0f );
			}
			else
			{
				mCameraController->SetDistance( 100.0f );
				bGridIsPlaying = true;
				if( mGameGridIterator != mGameGrids.begin() )
				{
					mLawnBlockPathIterator--;
					mLawnBlockPathIterator--;
					(*mLawnBlockPathIterator)->SetEnabled( false );
					mLawnBlockPathIterator++;
					mLawnBlockPathIterator++;
				}
			}
		}

		if( mLadybugWingMovingTimer >= 6.0f )
		{
			(*mGameGridIterator)->SetEnabled( false );
			mGameGridIterator++;

			//Here we start to keep enabled to draw previous, current and next lawn grid
			mLawnBlockPathIterator++;
			(*mLawnBlockPathIterator)->SetEnabled( true );
			mLawnBlockPathIterator++;
			if( mLawnBlockPathIterator != mLawnBlockPath.end() )
			{
				(*mLawnBlockPathIterator)->SetEnabled( true );
			}
			mLawnBlockPathIterator--;
			////

			if( mGameGridIterator != mGameGrids.end() )
			{
				SwitchScenarioToTerm( "03" );
			}
			else
			{
				//If we get end of gameGrid then we need to switch to next scene
				mGameGridIterator--;
				mLawnBlockPathIterator--;
			}
		}

		UpdateInsectSpawner( gameTime );
	}
	void Level1::ScenarioTerm05( const GameTime & gameTime )
	{
		bIsItSpawning = false;

		//Start menu is activated
		(*mGameGridIterator)->SetEnabled( false );

		mLadybugPlayable->SetIsRunning( false );


		mCameraTarget->SetPosition( mLadybugPlayable->Position()/*(*mGameGridIterator)->GetCenter()*/ );
		mCameraTarget->SetNewPosition( mLadybugPlayable->Position()/*(*mGameGridIterator)->GetCenter()*/ );

		mCameraController->SetEnabled( false );
		mCameraController->SetDistance( 50.0f );
		mCameraController->SetAngleView( CameraViewAngle::angle45deg );

		//mCurrentScenarioTerm = nullptr;

		mLadybugWingMovingTimer += gameTime.ElapsedGameTime();
		if( mLadybugWingMovingTimer > 20.0f )
		{
			mLevelName = Scene::LevelName::LevelName2;
			SceneSwitching( new IntermediateScene( *mGame, mSceneManager ) );
		}
	}


}