#include "RedClover.h"
#include "StaticModel.h"

namespace Rendering
{
	RTTI_DEFINITIONS( RedClover )

		RedClover::RedClover( Game& game, Camera& camera, const DirectionalLight& directionalLightSource )
		:
		Actor( game, camera ),
		mDirectionalLightSource( directionalLightSource ),
		mShape( nullptr )
	{
	}
	RedClover::~RedClover()
	{
		DeleteObject( mShape );
	}

	void RedClover::Initialize()
	{
		SetScale( XMFLOAT3( 8.0f, 8.0f, 8.0f ) );
		Actor::Initialize();

		mShape = new StaticModel( *this, mDirectionalLightSource );
		mShape->SetModel( "Content\\Models\\RedClover\\RedClover.obj" );
		mShape->SetColorTexture( L"Content\\Models\\RedClover\\RedCloverColorTexture.png" );
		mShape->SetNormalMapTexture( L"Content\\Models\\RedClover\\RedCloverNormalMapTexture.png" );
		
		mShape->Initialize();
		mShape->DrawAmbientDiffuse();
	}
	void RedClover::Update( const GameTime& gameTime )
	{
		Actor::Update( gameTime );
	}
	void RedClover::Draw( const GameTime& gameTime )
	{
		mShape->Draw( gameTime );
	}

}