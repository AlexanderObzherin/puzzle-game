#pragma once

#include "Insect.h"
#include "VectorHelper.h"


using namespace Library;

namespace Library
{
	class DirectionalLight;
}

namespace Rendering
{
	class AraneusAngulatus : public Insect
	{
		RTTI_DECLARATIONS( AraneusAngulatus, Insect )

	public:
		AraneusAngulatus( Game& game, Camera& camera, const DirectionalLight& directionalLightSource );
		~AraneusAngulatus();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		void Run( const GameTime& gameTime );
		void Stop( const GameTime& gameTime );
		bool GetIsRunning() const;
		void SetIsRunning( bool in )
		{
			bIsRunning = in;
		}

	private:
		bool bIsRunning;


	};
}