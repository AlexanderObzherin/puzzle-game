#include "LevelLabel.h"
#include "Game.h"
#include "GameException.h"
#include "Camera.h"
#include "Utility.h"
#include "Effect.h"

#include <WICTextureLoader.h>
#include "VectorHelper.h"
#include "MatrixHelper.h"
#include <SpriteBatch.h>
#include <SpriteFont.h>

#include "PointSprite.h"

//for std::bind stuff. Is needed to bind functions with parameters.
using namespace std::placeholders;

namespace Rendering
{
	RTTI_DEFINITIONS( LevelLabel )
		LevelLabel::LevelLabel( Game& game, Camera& camera, std::wstring texture )
		:
		Actor( game, camera ),
		mPointSprite( nullptr ),
		mTexture( texture ),
		mVelocity( Vector3Helper::Zero )
	{}
	LevelLabel::~LevelLabel()
	{
		DeleteObject( mPointSprite );
	}

	void LevelLabel::Initialize()
	{
		mPointSprite = new PointSprite( *this );
		mPointSprite->SetTexture( mTexture );
		mPointSprite->Initialize();

		Actor::Initialize();
	}
	void LevelLabel::Update( const GameTime & gameTime )
	{
		mPointSprite->Update( gameTime );

		XMVECTOR velocity = XMLoadFloat3( &mVelocity );
		XMVECTOR newPosition = PositionVector() + velocity;
		SetPosition( newPosition );


		Actor::Update( gameTime );
	}
	void LevelLabel::Draw( const GameTime & gameTime )
	{
		mPointSprite->Draw( gameTime );
	}

}