#pragma once

#include "Scene.h"

using namespace Library;

namespace Library
{
	class Keyboard;
	class Mouse;
	class RenderStateHelper;
}

namespace DirectX
{
	class SpriteBatch;
	class SpriteFont;
}

namespace Rendering
{
	class IntermediateScene : public Scene
	{
		RTTI_DECLARATIONS( IntermediateScene, Scene )

	public:
		IntermediateScene( Game& game, SceneManager& sceneManager );
		~IntermediateScene();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

	private:
		static const XMVECTORF32 BackgroundColor;

		SpriteBatch* mSpriteBatch;
		SpriteFont* mSpriteFont;
		RenderStateHelper* mRenderStateHelper;
		Keyboard* mKeyboard;

		float mTimer;
	};

}