#pragma once

#include "Actor.h"
#include <unordered_map>
#include <sstream>

using namespace Library;

namespace Library
{
	class Technique;
	class Effect;
	class DirectionalLight;
	class Model;
	class AnimationPlayer;
}

namespace DirectX
{
	class SpriteBatch;
	class SpriteFont;
}

namespace Rendering
{
	class AnimatedModelMaterial;

	class AnimatedModel
	{
	public:
		AnimatedModel( Actor& actor, const DirectionalLight& directionalLightSource );
		~AnimatedModel();

		virtual void Initialize();
		virtual void Update( const GameTime& gameTime );
		virtual void Draw( const GameTime& gameTime );

		void SetModelName( std::string modelFilename );
		void SetColorTexture( const std::wstring textureFilenamePath );
		void SetNormalMapTexture( const std::wstring textureFilenamePath );

		void DrawAmbientOnly();
		void DrawAmbientDiffuse();
		void DrawAmbientDiffuseWithNormalMap();
		void DrawPhongWithNormalMap();

		//This method to add animation clip by name and timeframe
		void AddAnimation( std::string name, float startTime, float endTime );
		//Method to set animation clip as current playing
		void SetAnimation( std::string name );

	private:
		AnimatedModel();
		AnimatedModel( const AnimatedModel& rhs );
		AnimatedModel& operator=( const AnimatedModel& rhs );

	protected:
		Actor& mActor;
		const DirectionalLight& mDirectionalLightSource;

		static const float LightModulationRate;
		static const float LightMovementRate;

		std::vector<Technique*>::const_iterator techniqueIterator;

		Effect* mEffect;
		AnimatedModelMaterial* mMaterial;

		XMCOLOR mAmbientColor;

		XMCOLOR mSpecularColor;
		float mSpecularPower;

		std::vector<ID3D11Buffer*> mVertexBuffers;
		std::vector<ID3D11Buffer*> mIndexBuffers;
		std::vector<UINT> mIndexCounts;
		std::vector<ID3D11ShaderResourceView*> mColorTextures;
		std::vector<ID3D11ShaderResourceView*> mNormalMapTextures;

	private:
		Model* mSkinnedModel;
		std::string mModelFilename;
		std::wstring mColorTextureFilename;
		std::wstring mNormalMapTextureFilename;

		std::unordered_map< std::string, std::unique_ptr<AnimationPlayer>> mAnimations;

		AnimationPlayer* mAnimationIterator;
	};
}