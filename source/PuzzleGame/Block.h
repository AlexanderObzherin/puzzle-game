#pragma once

#include "Actor.h"

using namespace Library;

namespace Library
{
	class DirectionalLight;
}

namespace Rendering
{
	class StaticModel;
	class Point;

	class Block : public Actor
	{
		RTTI_DECLARATIONS( Block, Actor )

	public:
		Block( Game& game, Camera& camera, const DirectionalLight& directionalLightSource );
		~Block();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		std::vector<Point*>& GetPointPath();

		void SetActive( bool in );
		bool GetIsActive() const;

		void SetIsOwnedByInsect( bool in );
		bool GetIsOwnedByInsect() const;

		XMINT2 GetGridPos() const;
		void SetGridPos( XMINT2 in );

		void SetDistanceFloat( float in )
		{
			mDistanceFloat = in;
		}
		float GetDistanceFloat() const;
		void Move( XMFLOAT3 movementDirection, const GameTime& gameTime );

	private:
		Block();
		Block( const Block& rhs );
		Block& operator=( const Block& rhs );

	protected:
		std::vector<Point*> mPath;
		
		//Active block is lightened as active and can be moved by input controller
		bool bIsActive;
		bool bIsOwnedByInsect;

		const DirectionalLight& mDirectionalLightSource;
		StaticModel* mShape;

		//unique grid coordinate. Is needed to be changed then we move block to another grid pos.
		XMINT2 mGridPos;
		//Variable to store distance between grid pos and derivation
		float mDistanceFloat;
		float mVelocityFactor;
	};

	class GameBlock : public Block
	{
		RTTI_DECLARATIONS( GameBlock, Block )

	public:
		GameBlock( Game& game, Camera& camera, const DirectionalLight& directionalLightSource );
		~GameBlock();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		void UpdateAmbientLight( const GameTime& gameTime );

	protected:
		StaticModel* mBlockFrame;
	};

	class BlockA : public GameBlock
	{
		RTTI_DECLARATIONS( BlockA, GameBlock )

	public:
		BlockA( Game& game, Camera& camera, const DirectionalLight& directionalLightSource );
		~BlockA();

		virtual void Initialize() override;
		virtual void Draw( const GameTime& gameTime ) override;
	};
	class BlockB : public GameBlock
	{
		RTTI_DECLARATIONS( BlockB, GameBlock )

	public:
		BlockB( Game& game, Camera& camera, const DirectionalLight& directionalLightSource );
		~BlockB();

		virtual void Initialize() override;
		virtual void Draw( const GameTime& gameTime ) override;
	};
	class BlockC : public GameBlock
	{
		RTTI_DECLARATIONS( BlockC, GameBlock )

	public:
		BlockC( Game& game, Camera& camera, const DirectionalLight& directionalLightSource );
		~BlockC();

		virtual void Initialize() override;
		virtual void Draw( const GameTime& gameTime ) override;
	};
	class BlockD : public GameBlock
	{
		RTTI_DECLARATIONS( BlockD, GameBlock )

	public:
		BlockD( Game& game, Camera& camera, const DirectionalLight& directionalLightSource );
		~BlockD();

		virtual void Initialize() override;
		virtual void Draw( const GameTime& gameTime ) override;
	};
	class BlockE : public GameBlock
	{
		RTTI_DECLARATIONS( BlockE, GameBlock )

	public:
		BlockE( Game& game, Camera& camera, const DirectionalLight& directionalLightSource );
		~BlockE();

		virtual void Initialize() override;
		virtual void Draw( const GameTime& gameTime ) override;
	};
	class BlockF : public GameBlock
	{
		RTTI_DECLARATIONS( BlockF, GameBlock )

	public:
		BlockF( Game& game, Camera& camera, const DirectionalLight& directionalLightSource );
		~BlockF();

		virtual void Initialize() override;
		virtual void Draw( const GameTime& gameTime ) override;
	};

	//Has no shape, just target points
	class BlockG : public Block
	{
		RTTI_DECLARATIONS( BlockG, Block )

	public:
		BlockG( Game& game, Camera& camera, const DirectionalLight& directionalLightSource );
		~BlockG();

		virtual void Initialize() override;
		virtual void Draw( const GameTime& gameTime ) override;
	};

	class Chamomile;
	class ChamomileBlock : public Block
	{
		RTTI_DECLARATIONS( ChamomileBlock, Block )

	public:
		ChamomileBlock( Game& game, Camera& camera, const DirectionalLight& directionalLightSource );
		~ChamomileBlock();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

	private:
		std::unique_ptr<Chamomile> mChamomile;
	};

	class Hepatica;
	class BlockHepatica : public Block
	{
		RTTI_DECLARATIONS( BlockHepatica, Block )
	public:
		BlockHepatica( Game& game, Camera& camera, const DirectionalLight& directionalLightSource );
		~BlockHepatica();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;
	private:
		std::unique_ptr<Hepatica> mHepatica;
	};

	class Taraxacum;
	class TaraxacumBlock : public Block
	{
		RTTI_DECLARATIONS( TaraxacumBlock, Block )

	public:
		TaraxacumBlock( Game& game, Camera& camera, const DirectionalLight& directionalLightSource );
		~TaraxacumBlock();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;
	private:
		std::unique_ptr<Taraxacum> mTaraxacum;
	};

	class RedCloverShort;
	class RedCloverShortBlock : public Block
	{
		RTTI_DECLARATIONS( RedCloverShortBlock, Block )

	public:
		RedCloverShortBlock( Game& game, Camera& camera, const DirectionalLight& directionalLightSource );
		~RedCloverShortBlock();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;
	private:
		std::unique_ptr<RedCloverShort> mRedCloverShort;
	};

	class RedCloverLeaf;
	class RedCloverLeafBlock : public Block
	{
		RTTI_DECLARATIONS( RedCloverLeafBlock, Block )

	public:
		RedCloverLeafBlock( Game& game, Camera& camera, const DirectionalLight& directionalLightSource );
		~RedCloverLeafBlock();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;
	private:
		std::unique_ptr<RedCloverLeaf> mRedCloverLeaf;
	};

	class RedClover;
	class RedCloverBlock : public Block
	{
		RTTI_DECLARATIONS( RedCloverBlock, Block )

	public:
		RedCloverBlock( Game& game, Camera& camera, const DirectionalLight& directionalLightSource );
		~RedCloverBlock();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;
	private:
		std::unique_ptr<RedClover> mRedClover;
	};

	class CentaureaCyanusShort;
	class CentaureaCyanusShortBlock : public Block
	{
		RTTI_DECLARATIONS( CentaureaCyanusShortBlock, Block )

	public:
		CentaureaCyanusShortBlock( Game& game, Camera& camera, const DirectionalLight& directionalLightSource );
		~CentaureaCyanusShortBlock();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;
	private:
		std::unique_ptr<CentaureaCyanusShort> mCentaureaCyanusShort;
	};

	class CentaureaCyanus;
	class CentaureaCyanusBlock : public Block
	{
		RTTI_DECLARATIONS( CentaureaCyanusBlock, Block )

	public:
		CentaureaCyanusBlock( Game& game, Camera& camera, const DirectionalLight& directionalLightSource );
		~CentaureaCyanusBlock();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;
	private:
		std::unique_ptr<CentaureaCyanus> mCentaureaCyanus;
	};

	class Grass;
	class GrassBlock : public Block
	{
		RTTI_DECLARATIONS( GrassBlock, Block )

	public:
		GrassBlock( Game& game, Camera& camera, const DirectionalLight& directionalLightSource );
		~GrassBlock();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;
	private:
		std::unique_ptr<Grass> mGrass;
	};
}