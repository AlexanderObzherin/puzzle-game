#pragma once

#include "Insect.h"
#include "VectorHelper.h"
#include "Level1.h"

using namespace Library;

namespace Library
{
	class DirectionalLight;
}

namespace Rendering
{
	class LadybugPlayable : public Insect
	{
		RTTI_DECLARATIONS( LadybugPlayable, Insect )

	public:
		LadybugPlayable( Game& game, Camera& camera, const DirectionalLight& directionalLightSource );
		~LadybugPlayable();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		void Run( const GameTime& gameTime );
		void Stop( const GameTime& gameTime );
		bool GetIsRunning() const;
		void SetIsRunning( bool in )
		{ 
			bIsRunning = in;
		}

	private:
		bool bIsRunning;
		

	};
}
