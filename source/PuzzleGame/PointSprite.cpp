#include "PointSprite.h"

#include "Game.h"
#include "GameException.h"
#include "Camera.h"
#include "Utility.h"
#include "Effect.h"
#include "Keyboard.h"

#include <WICTextureLoader.h>
#include "VectorHelper.h"
#include "MatrixHelper.h"
#include <SpriteBatch.h>
#include <SpriteFont.h>

#include "PointSpriteMaterial.h"

namespace Rendering
{
		PointSprite::PointSprite( Actor& actor )
		:
		mActor( actor ),
		mEffect( nullptr ),
		mMaterial( nullptr ),
		mPass( nullptr ),
		mInputLayout( nullptr ),
		mVertexBuffer( nullptr ),
		mColorTexture( nullptr ),
		mSpriteBatch( nullptr )
	{
			mTextureName = L"Content\\Textures\\WhiteDot.png";
	}
	PointSprite::~PointSprite()
	{
		DeleteObject( mSpriteBatch );
		ReleaseObject( mColorTexture );
		ReleaseObject( mVertexBuffer );
		DeleteObject( mMaterial );
		DeleteObject( mEffect );
	}

	void PointSprite::Initialize()
	{
		SetCurrentDirectory( Utility::ExecutableDirectory().c_str() );

		// Initialize the material
		mEffect = new Effect( *mActor.GetGame() );
		mEffect->LoadCompiledEffect( L"Content\\Effects\\PointSprite.cso" );
		mMaterial = new PointSpriteMaterial();
		mMaterial->Initialize( *mEffect );

		Technique* technique = mEffect->TechniquesByName().at( "main11" );
		mMaterial->SetCurrentTechnique( *technique );

		mPass = mMaterial->CurrentTechnique()->Passes().at( 0 );
		mInputLayout = mMaterial->InputLayouts().at( mPass );

		std::vector<VertexPositionSize> vertices;
		vertices.reserve( 1 );
		vertices.push_back( VertexPositionSize( XMFLOAT4( 0.0f, 0.0f, 0.0f, 1.0f ), XMFLOAT2( 1.0f, 1.0f ) ) );

		mVertexCount = vertices.size();

		ReleaseObject( mVertexBuffer );
		mMaterial->CreateVertexBuffer( mActor.GetGame()->Direct3DDevice(), &vertices[0], mVertexCount, &mVertexBuffer );

		
		HRESULT hr = DirectX::CreateWICTextureFromFile( mActor.GetGame()->Direct3DDevice(), mActor.GetGame()->Direct3DDeviceContext(), mTextureName.c_str(), nullptr, &mColorTexture );
		if( FAILED( hr ) )
		{
			throw GameException( "CreateWICTextureFromFile() failed.", hr );
		}

		mSpriteBatch = new SpriteBatch( mActor.GetGame()->Direct3DDeviceContext() );


	}

	void PointSprite::Update( const GameTime & gameTime )
	{

	}

	void PointSprite::Draw( const GameTime & gameTime )
	{
		ID3D11DeviceContext* direct3DDeviceContext = mActor.GetGame()->Direct3DDeviceContext();
		direct3DDeviceContext->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_POINTLIST );
		direct3DDeviceContext->IASetInputLayout( mInputLayout );

		UINT stride = mMaterial->VertexSize();
		UINT offset = 0;
		direct3DDeviceContext->IASetVertexBuffers( 0, 1, &mVertexBuffer, &stride, &offset );

		XMMATRIX worldMatrix = XMLoadFloat4x4( &mActor.WorldMatrix() );
		XMMATRIX wvp = worldMatrix *  mActor.GetCamera()->ViewMatrix() * mActor.GetCamera()->ProjectionMatrix();
		
		mMaterial->WorldViewProjection() << wvp;
		mMaterial->ViewProjection() << mActor.GetCamera()->ViewMatrix() * mActor.GetCamera()->ProjectionMatrix();
		mMaterial->ColorTexture() << mColorTexture;

		mPass->Apply( 0, direct3DDeviceContext );

		direct3DDeviceContext->Draw( mVertexCount, 0 );

		direct3DDeviceContext->GSSetShader( nullptr, nullptr, 0 );
	}

	void PointSprite::SetTexture( std::wstring textureName )
	{
		mTextureName = textureName;
	}


}