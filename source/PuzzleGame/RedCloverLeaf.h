#pragma once
#include "Actor.h"

using namespace Library;

namespace Library
{
	class DirectionalLight;
}

namespace Rendering
{
	class StaticModel;

	class RedCloverLeaf : public Actor
	{
		RTTI_DECLARATIONS( RedCloverLeaf, Actor )

	public:
		RedCloverLeaf( Game& game, Camera& camera, const DirectionalLight& directionalLightSource );
		~RedCloverLeaf();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

	private:
		RedCloverLeaf();
		RedCloverLeaf( const RedCloverLeaf& rhs );
		RedCloverLeaf& operator=( const RedCloverLeaf& rhs );

		const DirectionalLight& mDirectionalLightSource;
		StaticModel* mShape;
	};
}