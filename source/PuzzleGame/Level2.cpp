#include "Level2.h"
#include "GameException.h"
#include "WICTextureLoader.h"
#include "Level1.h"
#include "RenderStateHelper.h"
#include <sstream>
#include <iomanip>
#include <SpriteBatch.h>
#include <SpriteFont.h>
#include "Game.h"
#include "Utility.h"
#include "ColorHelper.h"
#include "Keyboard.h"
#include "Camera.h"
#include "ThirdPersonCameraController.h"
#include "FirstPersonCameraController.h"
#include "CameraTarget.h"

#include "LadybugPlayable.h"
#include "GameGrid.h"
#include "LawnBlockPath.h"

#include "Bumblebee.h"
#include "AglaisUrticae.h"
#include "NymphalisAntiopa.h"
#include "PierisBrassicae.h"
#include "PolyommatusSemiargus.h"
#include "PyrgusAndromedae.h"

#include "StartScene.h"
#include "IntermediateScene.h"
#include "Level3.h"

#include "Block.h"
#include "Point.h"
//
#include "Lawn.h"
#include "ParticleEffect.h"
#include "AudioPlayer.h"

#include "GastrophysaPolygoni.h"

//for std::bind stuff. Is needed to bind functions with parameters.
using namespace std::placeholders;

namespace Rendering
{
	RTTI_DEFINITIONS( Level2 )

	Level2::Level2( Game& game, SceneManager& sceneManager )
		:
		LevelsCommon( game, sceneManager ),
		mTimer( 0.0f )
	{
	}
	Level2::~Level2()
	{

		for( auto& it : mGastrophysaPolygonis )
		{
			DeleteObject( it );
		}
	}

	void Level2::Initialize()
	{
		//Set level's name displayed on screen
		mLevelNum = 2;

		//Set num of grids in level
		mNumberOfGameGrids = 13;

		//Level scenario defined as map of void func.
		//We iterate through them in level by switching to determined function.
		mScenarioTerms["01"] = std::bind( &Level2::ScenarioTerm01, this, _1 );
		mScenarioTerms["02"] = std::bind( &Level2::ScenarioTerm02, this, _1 );
		mScenarioTerms["03"] = std::bind( &Level2::ScenarioTerm03, this, _1 );
		mScenarioTerms["04"] = std::bind( &Level2::ScenarioTerm04, this, _1 );
		mScenarioTerms["05"] = std::bind( &Level2::ScenarioTerm05, this, _1 );

		//Set start scenario term
		mCurrentScenarioTerm = mScenarioTerms["01"];

		LevelsCommon::Initialize();

		////Create list for randomizer of Insect Spawner
		mFlyingInsectsList = new Randomizer<Insect>();
		auto CreateBumblebee = [&]() { return new Bumblebee( *mGame, *mCamera, *mDirectionalLight ); };
		mFlyingInsectsList->AddToSetList( 30, CreateBumblebee );
		auto CreateAglaisUrticae = [&]() { return new AglaisUrticae( *mGame, *mCamera, *mDirectionalLight ); };
		mFlyingInsectsList->AddToSetList( 30, CreateAglaisUrticae );
		auto CreateNymphalisAntiopa = [&]() { return new NymphalisAntiopa( *mGame, *mCamera, *mDirectionalLight ); };
		mFlyingInsectsList->AddToSetList( 30, CreateNymphalisAntiopa );
		auto CreatePierisBrassicae = [&]() { return new PierisBrassicae( *mGame, *mCamera, *mDirectionalLight ); };
		mFlyingInsectsList->AddToSetList( 30, CreatePierisBrassicae );
		auto CreatePolyommatusSemiargus = [&]() { return new PolyommatusSemiargus( *mGame, *mCamera, *mDirectionalLight ); };
		mFlyingInsectsList->AddToSetList( 30, CreatePolyommatusSemiargus );
		auto CreatePyrgusAndromedae = [&]() { return new PyrgusAndromedae( *mGame, *mCamera, *mDirectionalLight ); };
		mFlyingInsectsList->AddToSetList( 30, CreatePyrgusAndromedae );

		mAudioPlayer->PlayAudioBackground("Content\\Audio\\MorningInTheMagicalForest.mp3", 0.075f);

		mGastrophysaPolygonis.push_back(new GastrophysaPolygoni(*mGame, *mCamera, *mDirectionalLight));
		mGastrophysaPolygonis.back()->Initialize();
		mGastrophysaPolygonis.back()->SetBlockPath(*mGameGridIterator);

	}
	void Level2::Update( const GameTime & gameTime )
	{
		if ( !bGameIsPaused )
		{
			for (auto& it : mGastrophysaPolygonis)
			{
				it->Update(gameTime);
			}
		}
		LevelsCommon::Update( gameTime );
	}
	void Level2::Draw( const GameTime & gameTime )
	{
		for (auto& it : mGastrophysaPolygonis)
		{
			it->Draw( gameTime );
		}

		LevelsCommon::Draw( gameTime );
	}

	void Level2::ScenarioTerm01( const GameTime & gameTime )
	{
		bIsItSpawning = false;

		(*mGameGridIterator)->SetEnabled( false );

		mLadybugPlayable->SetPosition( (*mGameGridIterator)->GetStartBlock()->GetPointPath().at( 1 )->Position() );
		mLadybugPlayable->SetIsRunning( false );

		mGastrophysaPolygonis.back()->SetPosition((*mGameGridIterator)->GetFinishBlock()->GetPointPath().at(1)->Position());
		mGastrophysaPolygonis.back()->SetIsRunning( false );

		if ( mTimer < 1.0f )
		{
			//Set camera target to ladybug
			mCameraTarget->SetPosition(mLadybugPlayable->Position()/*(*mGameGridIterator)->GetCenter()*/);
			mCameraTarget->SetNewPosition(mLadybugPlayable->Position()/*(*mGameGridIterator)->GetCenter()*/);
		}
		if ( mTimer > 10.0f )
		{
			//Set camera target to gastrophysa
			mCameraTarget->SetPosition( mGastrophysaPolygonis.back()->Position() );
			mCameraTarget->SetNewPosition( mGastrophysaPolygonis.back()->Position() );
		}

		mCameraController->SetEnabled( false );
		mCameraController->SetDistance( 50.0f );
		mCameraController->SetAngleView( CameraViewAngle::angle45deg );

		//Switch to next
		SwitchScenarioToTerm( "02" );
	}
	void Level2::ScenarioTerm02( const GameTime & gameTime )
	{
		mTimer += gameTime.ElapsedGameTime();
		if (mTimer > 10.0f && mTimer < 10.5f )
		{
			mTimer++;
			SwitchScenarioToTerm( "01" );
		}

		if ( mTimer > 25.0f )
		{
			mTimer = 0.0f;
			SwitchScenarioToTerm( "03" );
		}
	}
	void Level2::ScenarioTerm03( const GameTime & gameTime )
	{
		bGridIsPlaying = false;
		mLadybugWingMovingTimer = 0.0f;

		if( mGameGridIterator != mGameGrids.end() && mLawnBlockPathIterator != mLawnBlockPath.end() )
		{
			//Camera
			mCameraController->SetEnabled( true );
			mCameraController->SetDistance( 100.0f );
			mCameraController->SetAngleView( CameraViewAngle::top );

			//Set new game grid as enabled
			(*mGameGridIterator)->SetEnabled( true );

			//Set cameraTarget to move to this new grid
			mCameraTarget->SetNewPosition( dynamic_cast<GameGrid*>(*mGameGridIterator)->GetCenter() );

			//Switch characters to walk through new gameGrid

			mLadybugPlayable->SetBlockPath( *mGameGridIterator );
			mLadybugPlayable->SetPosition( (*mGameGridIterator)->GetStartBlock()->GetPointPath().at( 1 )->Position() );
			mLadybugPlayable->SetIsRunning( true );

			mGastrophysaPolygonis.back()->SetBlockPath( *mGameGridIterator );
			mGastrophysaPolygonis.back()->SetPosition( (*mGameGridIterator)->GetFinishBlock()->GetPointPath().at( 1 )->Position() );
			mGastrophysaPolygonis.back()->SetIsRunning( true );
		}

		bIsItSpawning = true;

		SwitchScenarioToTerm( "04" );
	}
	void Level2::ScenarioTerm04( const GameTime& gameTime )
	{
		//Menu
		if( mKeyboard->WasKeyPressedThisFrame( DIK_ESCAPE ) )
		{
			bGameIsPaused = !bGameIsPaused;

			mMenuBgScale = 1.0f;
		}
		if( mKeyboard->WasKeyPressedThisFrame( DIK_UPARROW ) && !(mMenuActiveOption & Menu::ReturnToGame) && bGameIsPaused )
		{
			mMenuActiveOption = (Menu)(mMenuActiveOption >> 1);
		}
		if( mKeyboard->WasKeyPressedThisFrame( DIK_DOWNARROW ) && !(mMenuActiveOption & Menu::ExitMenu) && bGameIsPaused )
		{
			mMenuActiveOption = (Menu)(mMenuActiveOption << 1);
		}
		if( mKeyboard->WasKeyPressedThisFrame( DIK_RETURN ) )
		{
			if( (mMenuActiveOption & Menu::ReturnToGame) )
			{
				bGameIsPaused = !bGameIsPaused;
			}
			if( (mMenuActiveOption & Menu::StartLevel) )
			{
				//Start this level again
				mLevelName = Scene::LevelName::LevelName2;
				SceneSwitching( new IntermediateScene( *mGame, mSceneManager ) );
				return;
			}
			if( (mMenuActiveOption & Menu::StartLevel1) )
			{
				//Start game from first level
				mLevelName = Scene::LevelName::LevelName1;
				SceneSwitching( new IntermediateScene( *mGame, mSceneManager ) );
				return;
			}
			if( (mMenuActiveOption & Menu::ExitMenu) )
			{
				//Exit to Windows
				mGame->Exit();
			}
		}
		///

		{
			//Check if Ladybug collided with middle point of finish block
			XMVECTOR distanceVector = XMVector3Length( mLadybugPlayable->PositionVector()
				- (dynamic_cast<GameGrid*>(*mGameGridIterator))->GetFinishBlock()->GetPointPath().at( 1 )->PositionVector() );
			float distance = distanceVector.m128_f32[0];
			if (distance <= 2.0f)
			{
				//Play audio effect and particle effect once,
				//Wings moving for a few seconds and
				//Switch to next GameGrid
				if (mLadybugPlayable->GetIsRunning())
				{
					mParticleEffect->SetPosition(mLadybugPlayable->PositionVector());
					mParticleEffect->PlayEffect();

					if (!(mAudioPlayer->IsSamplePlaying("Content\\Audio\\BellTree.mp3")))
					{
						mAudioPlayer->PlaySample("Content\\Audio\\BellTree.mp3");
					}
				}

				mLadybugPlayable->SetIsRunning(false);
			}
		}

		{
			//Check if Gastrophysa Polygoni collided with middle point of Start Block
			XMVECTOR distanceVector = XMVector3Length(mGastrophysaPolygonis.back()->PositionVector()
				- (dynamic_cast<GameGrid*>(*mGameGridIterator))->GetStartBlock()->GetPointPath().at(1)->PositionVector());
			float distance = distanceVector.m128_f32[0];
			if (distance <= 2.0f)
			{
				mGastrophysaPolygonis.back()->SetIsRunning(false);
			}
		}

		if( !bGridIsPlaying )
		{
			if( mCameraTarget->GetDistanceBetweenTarget() > 8.0f )
			{
				mCameraController->SetDistance( 160.0f );
			}
			else
			{
				//delete previous gastrophysa
				if (*mGameGridIterator != mGameGrids.front())
				{
					mGastrophysaPolygonis.pop_front();
				}

				mCameraController->SetDistance( 100.0f );
				bGridIsPlaying = true;
				if( mGameGridIterator != mGameGrids.begin() )
				{
					mLawnBlockPathIterator--;
					mLawnBlockPathIterator--;
					(*mLawnBlockPathIterator)->SetEnabled( false );
					mLawnBlockPathIterator++;
					mLawnBlockPathIterator++;
				}
			}
		}

		//In this level we switch to next gamegrid then both characters are set to IsRunning false,
		//i.e. both of them gain opposite finish block
		if( !(mLadybugPlayable->GetIsRunning()) && !(mGastrophysaPolygonis.back()->GetIsRunning()) )
		{
			(*mGameGridIterator)->SetEnabled( false );
			mGameGridIterator++;

			//Here we start to keep enabled to draw previous, current and next lawn grid
			mLawnBlockPathIterator++;
			(*mLawnBlockPathIterator)->SetEnabled( true );
			mLawnBlockPathIterator++;
			if( mLawnBlockPathIterator != mLawnBlockPath.end() )
			{
				(*mLawnBlockPathIterator)->SetEnabled( true );
			}
			mLawnBlockPathIterator--;
			////

			if( mGameGridIterator != mGameGrids.end() )
			{
				//Here we need to create new Gastrophysa on new gamegrid.
				mGastrophysaPolygonis.push_back(new GastrophysaPolygoni(*mGame, *mCamera, *mDirectionalLight));
				mGastrophysaPolygonis.back()->Initialize();
				mGastrophysaPolygonis.back()->SetBlockPath(*mGameGridIterator);
				SwitchScenarioToTerm( "03" );
			}
			else
			{
				//If we get end of gameGrid then we need to switch to next scene
				mGameGridIterator--;
				mLawnBlockPathIterator--;


				mAudioPlayer->StopAudioBackGround();
				mAudioPlayer->PlaySample( "Content\\Audio\\Finish.mp3", false, 0.075f );
				SwitchScenarioToTerm( "05" );
			}
		}

		UpdateInsectSpawner( gameTime );

		//Check if two playable characters on gamegrid is collided, them turn them back
		auto distanceVectorBetChar = mLadybugPlayable->PositionVector() - mGastrophysaPolygonis.back()->PositionVector();
		auto distanceVectorSq = XMVector3Dot( distanceVectorBetChar, distanceVectorBetChar );
		float distanceBetweenCharacters = sqrtf( distanceVectorSq.m128_f32[0] + distanceVectorSq.m128_f32[1] + distanceVectorSq.m128_f32[2] );

		if( (int)distanceBetweenCharacters < 6 )
		{
			mLadybugPlayable->TurnBack();
			mGastrophysaPolygonis.back()->TurnBack();
		}

	}
	void Level2::ScenarioTerm05( const GameTime & gameTime )
	{
		bIsItSpawning = false;

		//Start menu is activated
		(*mGameGridIterator)->SetEnabled( false );

		mLadybugPlayable->SetIsRunning( false );

		mCameraTarget->SetPosition( mLadybugPlayable->Position()/*(*mGameGridIterator)->GetCenter()*/ );
		mCameraTarget->SetNewPosition( mLadybugPlayable->Position()/*(*mGameGridIterator)->GetCenter()*/ );

		mCameraController->SetEnabled( false );
		mCameraController->SetDistance( 50.0f );
		mCameraController->SetAngleView( CameraViewAngle::angle45deg );

		//mCurrentScenarioTerm = nullptr;

		mLadybugWingMovingTimer += gameTime.ElapsedGameTime();
		if( mLadybugWingMovingTimer > 20.0f )
		{
			mLevelName = Scene::LevelName::LevelName3;
			SceneSwitching( new IntermediateScene( *mGame, mSceneManager ) );
		}
	}


}