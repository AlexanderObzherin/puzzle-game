#include "AnimatedModel.h"
#include "AnimatedModelMaterial.h"
#include "Game.h"
#include "GameException.h"
#include "VectorHelper.h"
#include "MatrixHelper.h"
#include "ColorHelper.h"
#include "Camera.h"
#include "Model.h"
#include "Mesh.h"
#include "ModelMaterial.h"
#include "AnimationPlayer.h"
#include "AnimationClip.h"
#include "Utility.h"
#include "DirectionalLight.h"
#include "Keyboard.h"
#include <WICTextureLoader.h>

#include "RenderStateHelper.h"

#include <iomanip>
#include <SpriteBatch.h>
#include <SpriteFont.h>
#include <sstream>
#include <iomanip>
#include "Shlwapi.h"

using namespace Library;

namespace Rendering
{
	const float AnimatedModel::LightModulationRate = UCHAR_MAX;
	const float AnimatedModel::LightMovementRate = 10.0f;

		AnimatedModel::AnimatedModel( Actor& actor, const DirectionalLight& directionalLightSource )
		:
		
		mActor( actor ),
		mDirectionalLightSource( directionalLightSource ),

		mEffect( nullptr ),
		mMaterial( nullptr ),

		mAmbientColor(reinterpret_cast<const float*>(&ColorHelper::White)),
		mSpecularColor( 1.0f, 1.0f, 1.0f, 1.0f ),
		mSpecularPower( 25.0f ),

		
		mVertexBuffers(),
		mIndexBuffers(),
		mIndexCounts(),
		mColorTextures(),
		mSkinnedModel( nullptr ),
			mAnimationIterator( nullptr )

	{
		SetNormalMapTexture( L"Content\\Textures\\DefaultNormalMap.png" );
			
	}

	AnimatedModel::~AnimatedModel()
	{
		for( auto* vertexBuffer : mVertexBuffers )
		{
			ReleaseObject( vertexBuffer );
		}
		for( auto* indexBuffer : mIndexBuffers )
		{
			ReleaseObject( indexBuffer );
		}
		for( auto* colorTexture : mColorTextures )
		{
			ReleaseObject( colorTexture );
		}
		for( auto* normalMapTexture : mNormalMapTextures )
		{
			ReleaseObject( normalMapTexture );
		}
		DeleteObject( mSkinnedModel );

		DeleteObject( mMaterial );
		DeleteObject( mEffect );

	}

	void AnimatedModel::Initialize()
	{
		SetCurrentDirectory( Utility::ExecutableDirectory().c_str() );

		// Load the model
		mSkinnedModel = new Model( *mActor.GetGame(), mModelFilename/**/, true );

		// Initialize the material
		mEffect = new Effect( *mActor.GetGame() );
		mEffect->LoadCompiledEffect( L"Content\\Effects\\AnimatedModel.cso" );
		mMaterial = new AnimatedModelMaterial();
		mMaterial->Initialize( *mEffect );

		techniqueIterator = mMaterial->GetEffect()->Techniques().begin();

		// Create the vertex and index buffers
		mVertexBuffers.resize( mSkinnedModel->Meshes().size() );
		mIndexBuffers.resize( mSkinnedModel->Meshes().size() );
		mIndexCounts.resize( mSkinnedModel->Meshes().size() );
		mColorTextures.resize( mSkinnedModel->Meshes().size() );
		mNormalMapTextures.resize( mSkinnedModel->Meshes().size() );
		for( UINT i = 0; i < mSkinnedModel->Meshes().size(); i++ )
		{
			Mesh* mesh = mSkinnedModel->Meshes().at( i );

			ID3D11Buffer* vertexBuffer = nullptr;
			mMaterial->CreateVertexBuffer( mActor.GetGame()->Direct3DDevice(), *mesh, &vertexBuffer );
			mVertexBuffers[i] = vertexBuffer;

			ID3D11Buffer* indexBuffer = nullptr;
			mesh->CreateIndexBuffer( &indexBuffer );
			mIndexBuffers[i] = indexBuffer;

			mIndexCounts[i] = mesh->Indices().size();

			ID3D11ShaderResourceView* colorTexture = nullptr;
			ModelMaterial* material = mesh->GetMaterial();

			HRESULT hr = DirectX::CreateWICTextureFromFile( mActor.GetGame()->Direct3DDevice(), mActor.GetGame()->Direct3DDeviceContext(), mColorTextureFilename.c_str(), nullptr, &colorTexture );
			if( FAILED( hr ) )
			{
				throw GameException( "CreateWICTextureFromFile() failed.", hr );
			}
			mColorTextures[i] = colorTexture;

			ID3D11ShaderResourceView* normalMapTexture = nullptr;
			hr = DirectX::CreateWICTextureFromFile( mActor.GetGame()->Direct3DDevice(), mActor.GetGame()->Direct3DDeviceContext(), mNormalMapTextureFilename.c_str(), nullptr, &normalMapTexture );
			if( FAILED( hr ) )
			{
				throw GameException( "CreateWICTextureFromFile() failed.", hr );
			}
			mNormalMapTextures[i] = normalMapTexture;

		}

		//If nothing was added and selected, play default
		AddAnimation( "Default", 0.0f, 5.0f );
		mAnimationIterator = mAnimations["Default"].get();
	}

	void AnimatedModel::Update( const GameTime& gameTime )
	{
		mAnimationIterator->Update( gameTime );
	}

	void AnimatedModel::Draw( const GameTime & gameTime )
	{
		ID3D11DeviceContext* direct3DDeviceContext = mActor.GetGame()->Direct3DDeviceContext();
		direct3DDeviceContext->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST );

		Pass* pass = mMaterial->CurrentTechnique()->Passes().at( 0 );
		ID3D11InputLayout* inputLayout = mMaterial->InputLayouts().at( pass );
		direct3DDeviceContext->IASetInputLayout( inputLayout );

		XMMATRIX worldMatrix = XMLoadFloat4x4( &mActor.WorldMatrix() );
		XMMATRIX wvp = worldMatrix * mActor.GetCamera()->ViewMatrix() * mActor.GetCamera()->ProjectionMatrix();
		XMVECTOR ambientColor = XMLoadColor( &mAmbientColor );
		XMVECTOR specularColor = XMLoadColor( &mSpecularColor );

		UINT stride = mMaterial->VertexSize();
		UINT offset = 0;

		for( UINT i = 0; i < mVertexBuffers.size(); i++ )
		{
			ID3D11Buffer* vertexBuffer = mVertexBuffers[i];
			ID3D11Buffer* indexBuffer = mIndexBuffers[i];
			UINT indexCount = mIndexCounts[i];
			ID3D11ShaderResourceView* colorTexture = mColorTextures[i];
			ID3D11ShaderResourceView* normalMapTexture = mNormalMapTextures[i];

			direct3DDeviceContext->IASetVertexBuffers( 0, 1, &vertexBuffer, &stride, &offset );
			direct3DDeviceContext->IASetIndexBuffer( indexBuffer, DXGI_FORMAT_R32_UINT, 0 );

			mMaterial->WorldViewProjection() << wvp;
			mMaterial->World() << worldMatrix;
			mMaterial->SpecularColor() << specularColor;
			mMaterial->SpecularPower() << mSpecularPower;
			mMaterial->AmbientColor() << ambientColor;
			mMaterial->LightColor() << mDirectionalLightSource.ColorVector();
			mMaterial->LightDirection() << mDirectionalLightSource.DirectionVector();
			mMaterial->ColorTexture() << colorTexture;
			mMaterial->NormalMapTexture() << normalMapTexture;
			mMaterial->CameraPosition() << mActor.GetCamera()->PositionVector();
			mMaterial->BoneTransforms() << mAnimationIterator->BoneTransforms();

			pass->Apply( 0, direct3DDeviceContext );

			direct3DDeviceContext->DrawIndexed( indexCount, 0, 0 );
		}

	}

	void AnimatedModel::SetModelName( std::string modelFilename )
	{
		mModelFilename = modelFilename;
	}

	void AnimatedModel::SetColorTexture( const std::wstring textureFilenamePath )
	{
		mColorTextureFilename = textureFilenamePath;
	}

	void AnimatedModel::SetNormalMapTexture( const std::wstring textureFilenamePath )
	{
		mNormalMapTextureFilename = textureFilenamePath;
	}

	void AnimatedModel::DrawAmbientOnly()
	{
		techniqueIterator = mMaterial->GetEffect()->Techniques().begin();
		techniqueIterator++;
		techniqueIterator++;
		mMaterial->SetCurrentTechnique( *techniqueIterator );
	}

	void AnimatedModel::DrawAmbientDiffuse()
	{
		techniqueIterator = mMaterial->GetEffect()->Techniques().begin();
		techniqueIterator++;
		mMaterial->SetCurrentTechnique( *techniqueIterator );
	}

	void AnimatedModel::DrawAmbientDiffuseWithNormalMap()
	{
		techniqueIterator = mMaterial->GetEffect()->Techniques().begin();
		techniqueIterator++;
		techniqueIterator++;
		techniqueIterator++;
		techniqueIterator++;
		mMaterial->SetCurrentTechnique( *techniqueIterator );
	}

	void AnimatedModel::DrawPhongWithNormalMap()
	{
		techniqueIterator = mMaterial->GetEffect()->Techniques().begin();
		techniqueIterator++;
		techniqueIterator++;
		techniqueIterator++;
		mMaterial->SetCurrentTechnique( *techniqueIterator );
	}

	void AnimatedModel::AddAnimation( std::string name, float startTime, float endTime )
	{
		mAnimations.emplace( name, new AnimationPlayer( *mActor.GetGame(), *mSkinnedModel, true ) );
		mAnimations[name]->SetTimeFrame( startTime, endTime );
		mAnimations[name]->StartClip( *mSkinnedModel->Animations().at( 0 ) );
	}

	void AnimatedModel::SetAnimation( std::string name )
	{
		mAnimationIterator = mAnimations[name].get();
	}



}