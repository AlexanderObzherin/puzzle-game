#pragma once

#include "Actor.h"

using namespace Library;

namespace Library
{
	class DirectionalLight;
}

namespace Rendering
{
	class StaticModel;
	class Block;

	class Point : public Actor
	{
		RTTI_DECLARATIONS( Point, Actor )

	public:
		Point( Game& game, Camera& camera, const DirectionalLight& directionalLightSource, Block& block );
		~Point();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		Block& GetBlock();

	private:
		const DirectionalLight& mDirectionalLightSource;
		Block& mBlock;
		StaticModel* mShape;
	};
}