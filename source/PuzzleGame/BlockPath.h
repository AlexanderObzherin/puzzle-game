#pragma once

#include "Actor.h"
#include <list>

using namespace Library;

namespace Library
{
	class DirectionalLight;
}

namespace Rendering
{
	class Shape;
	class Block;

	class BlockPath : public Actor
	{
		RTTI_DECLARATIONS( BlockPath, Actor )

	public:
		BlockPath( Game& game, Camera& camera, const DirectionalLight& directionalLightSource );
		~BlockPath();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		std::list<Block*>& GetBlocks();

	private:
		BlockPath();
		BlockPath( const BlockPath& rhs );
		BlockPath& operator=( const BlockPath& rhs );

	protected:
		const DirectionalLight& mDirectionalLightSource;
		std::list<Block*> mBlocks;
	};
}