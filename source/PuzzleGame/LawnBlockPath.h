#pragma once

#include <unordered_map>
#include "BlockPath.h"
#include <vector>
#include "Block.h"
#include "Randomizer.h"
using namespace Library;

namespace Library
{
	class Keyboard;
	class DirectionalLight;
}

namespace Rendering
{
	class Shape;
	class Block;

	//LawnBlockPath classes creates sets of defined plants
	class LawnBlockPath : public BlockPath
	{
		RTTI_DECLARATIONS( LawnBlockPath, BlockPath )

	public:
		LawnBlockPath( Game& game, Camera& camera, const DirectionalLight& directionalLightSourc, int level );
		~LawnBlockPath();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		XMFLOAT3 GetCenter() const;
		void CreateGrid();

		void CreateChamomileBlock( XMFLOAT3 position );

	private:
		LawnBlockPath();
		LawnBlockPath( const LawnBlockPath& rhs );
		LawnBlockPath& operator=( const LawnBlockPath& rhs );

		//std::wstring mLevelName;
		int mLevel;
		//Here we store list of plants which will be generated in defined level
		//we will call it by name, for ex 1, 2 and so on.
		std::unordered_map<int, Randomizer<Block>*> mListsOfPlants;

		Randomizer<Block>* mRandomizerList1;
		Randomizer<Block>* mRandomizerList2;
	};
}