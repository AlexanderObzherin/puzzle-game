#pragma once

#include "Common.h"
#include "Game.h"

using namespace Library;

namespace Library
{
	class Keyboard;
	class Mouse;

	class FpsComponent;
	class RenderStateHelper;
}

namespace DirectX
{
	class SpriteBatch;
	class SpriteFont;
}

namespace Rendering
{
	class SceneManager;

	class RenderingGame : public Game
	{
	public:
		RenderingGame( HINSTANCE instance, const std::wstring& windowClass, const std::wstring& windowTitle, int showCommand );
		~RenderingGame();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

	protected:
		virtual void Shutdown() override;

	private:
		static /*const*/ XMVECTORF32 BackgroundColor;

		static const float LightModulationRate;
		static const XMFLOAT2 LightRotationRate;

		LPDIRECTINPUT8 mDirectInput;
		Keyboard* mKeyboard;
		Mouse* mMouse;

		FpsComponent* mFpsComponent;
		RenderStateHelper* mRenderStateHelper;

		SceneManager* mSceneManager;
	};
}