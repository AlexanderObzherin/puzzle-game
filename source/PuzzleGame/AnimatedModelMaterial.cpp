#include "AnimatedModelMaterial.h"
#include "GameException.h"
#include "Mesh.h"
#include "Bone.h"

#include "VectorHelper.h"

using namespace Library;

namespace Rendering
{
	RTTI_DEFINITIONS( AnimatedModelMaterial )

		AnimatedModelMaterial::AnimatedModelMaterial()
		: Material( "AmbDifSpec" ),
		MATERIAL_VARIABLE_INITIALIZATION( WorldViewProjection ), 
		MATERIAL_VARIABLE_INITIALIZATION( World ),
		MATERIAL_VARIABLE_INITIALIZATION( SpecularColor ), 
		MATERIAL_VARIABLE_INITIALIZATION( SpecularPower ),
		MATERIAL_VARIABLE_INITIALIZATION( AmbientColor ), 
		MATERIAL_VARIABLE_INITIALIZATION( LightColor ),

		MATERIAL_VARIABLE_INITIALIZATION( LightDirection ),
		MATERIAL_VARIABLE_INITIALIZATION( CameraPosition ), 
		MATERIAL_VARIABLE_INITIALIZATION( BoneTransforms ),
		MATERIAL_VARIABLE_INITIALIZATION( ColorTexture ),
		MATERIAL_VARIABLE_INITIALIZATION( NormalMapTexture )
	{
	}

		MATERIAL_VARIABLE_DEFINITION( AnimatedModelMaterial, WorldViewProjection )
		MATERIAL_VARIABLE_DEFINITION( AnimatedModelMaterial, World )
		MATERIAL_VARIABLE_DEFINITION( AnimatedModelMaterial, SpecularColor )
		MATERIAL_VARIABLE_DEFINITION( AnimatedModelMaterial, SpecularPower )
		MATERIAL_VARIABLE_DEFINITION( AnimatedModelMaterial, AmbientColor )
		MATERIAL_VARIABLE_DEFINITION( AnimatedModelMaterial, LightColor )

		MATERIAL_VARIABLE_DEFINITION( AnimatedModelMaterial ,LightDirection )
		MATERIAL_VARIABLE_DEFINITION( AnimatedModelMaterial, CameraPosition )
		MATERIAL_VARIABLE_DEFINITION( AnimatedModelMaterial, BoneTransforms )
		MATERIAL_VARIABLE_DEFINITION( AnimatedModelMaterial, ColorTexture )
		MATERIAL_VARIABLE_DEFINITION( AnimatedModelMaterial, NormalMapTexture )

		void AnimatedModelMaterial::Initialize( Effect& effect )
	{
		Material::Initialize( effect );

			MATERIAL_VARIABLE_RETRIEVE( WorldViewProjection )
			MATERIAL_VARIABLE_RETRIEVE( World )
			MATERIAL_VARIABLE_RETRIEVE( SpecularColor )
			MATERIAL_VARIABLE_RETRIEVE( SpecularPower )
			MATERIAL_VARIABLE_RETRIEVE( AmbientColor )
			MATERIAL_VARIABLE_RETRIEVE( LightColor )

			MATERIAL_VARIABLE_RETRIEVE( LightDirection )
			MATERIAL_VARIABLE_RETRIEVE( CameraPosition )
			MATERIAL_VARIABLE_RETRIEVE( BoneTransforms )
			MATERIAL_VARIABLE_RETRIEVE( ColorTexture )
			MATERIAL_VARIABLE_RETRIEVE( NormalMapTexture )

			D3D11_INPUT_ELEMENT_DESC inputElementDescriptions[] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "BONEINDICES", 0, DXGI_FORMAT_R32G32B32A32_UINT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "WEIGHTS", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
		};

		CreateInputLayout( "AmbDifSpec", "p0", inputElementDescriptions, ARRAYSIZE( inputElementDescriptions ) );
		CreateInputLayout( "AmbDif", "p0", inputElementDescriptions, ARRAYSIZE( inputElementDescriptions ) );
		CreateInputLayout( "Amb", "p0", inputElementDescriptions, ARRAYSIZE( inputElementDescriptions ) );
		CreateInputLayout( "AmbDifSpecBump", "p0", inputElementDescriptions, ARRAYSIZE( inputElementDescriptions ) );
		CreateInputLayout( "AmbDifBump", "p0", inputElementDescriptions, ARRAYSIZE( inputElementDescriptions ) );

	}

	void AnimatedModelMaterial::CreateVertexBuffer( ID3D11Device* device, const Mesh& mesh, ID3D11Buffer** vertexBuffer ) const
	{
		const std::vector<XMFLOAT3>& sourceVertices = mesh.Vertices();
		std::vector<XMFLOAT3>* textureCoordinates = mesh.TextureCoordinates().at( 0 );
		assert( textureCoordinates->size() == sourceVertices.size() );
		const std::vector<XMFLOAT3>& normals = mesh.Normals();
		assert( normals.size() == sourceVertices.size() );
		const std::vector<XMFLOAT3>& tangents = mesh.Tangents();
		assert( tangents.size() == sourceVertices.size() );
		
		const std::vector<BoneVertexWeights>& boneWeights = mesh.BoneWeights();
		assert( boneWeights.size() == sourceVertices.size() );

		std::vector<VertexSkinnedPositionTextureNormalTangent> vertices;
		vertices.reserve( sourceVertices.size() );
		for( UINT i = 0; i < sourceVertices.size(); i++ )
		{
			XMFLOAT3 position = sourceVertices.at( i );
			XMFLOAT3 uv = textureCoordinates->at( i );
			XMFLOAT3 normal = normals.at( i );
			XMFLOAT3 tangent = tangents.at( i );

			BoneVertexWeights vertexWeights = boneWeights.at( i );

			float weights[BoneVertexWeights::MaxBoneWeightsPerVertex];
			UINT indices[BoneVertexWeights::MaxBoneWeightsPerVertex];
			ZeroMemory( weights, sizeof( float ) * ARRAYSIZE( weights ) );
			ZeroMemory( indices, sizeof( UINT ) * ARRAYSIZE( indices ) );
			for( UINT i = 0; i < vertexWeights.Weights().size(); i++ )
			{
				BoneVertexWeights::VertexWeight vertexWeight = vertexWeights.Weights().at( i );
				weights[i] = vertexWeight.Weight;
				indices[i] = vertexWeight.BoneIndex;
			}

			vertices.push_back( VertexSkinnedPositionTextureNormalTangent( XMFLOAT4( position.x, position.y, position.z, 1.0f ), XMFLOAT2( uv.x, uv.y ), normal, tangent, XMUINT4( indices ), XMFLOAT4( weights ) ) );
		}

		CreateVertexBuffer( device, &vertices[0], vertices.size(), vertexBuffer );
	}

	void AnimatedModelMaterial::CreateVertexBuffer( ID3D11Device* device, VertexSkinnedPositionTextureNormalTangent* vertices, UINT vertexCount, ID3D11Buffer** vertexBuffer ) const
	{
		D3D11_BUFFER_DESC vertexBufferDesc;
		ZeroMemory( &vertexBufferDesc, sizeof( vertexBufferDesc ) );
		vertexBufferDesc.ByteWidth = VertexSize() * vertexCount;
		vertexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
		vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

		D3D11_SUBRESOURCE_DATA vertexSubResourceData;
		ZeroMemory( &vertexSubResourceData, sizeof( vertexSubResourceData ) );
		vertexSubResourceData.pSysMem = vertices;
		if( FAILED( device->CreateBuffer( &vertexBufferDesc, &vertexSubResourceData, vertexBuffer ) ) )
		{
			throw GameException( "ID3D11Device::CreateBuffer() failed." );
		}
	}

	UINT AnimatedModelMaterial::VertexSize() const
	{
		return sizeof( VertexSkinnedPositionTextureNormalTangent );
	}
}