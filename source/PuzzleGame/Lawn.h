#pragma once

#include "Actor.h"

using namespace Library;

namespace Library
{
	class DirectionalLight;
}

namespace Rendering
{
	class StaticModel;

	class Lawn : public Actor
	{
		RTTI_DECLARATIONS( Lawn, Actor )

	public:
		Lawn( Game& game, Camera& camera, const DirectionalLight& directionalLightSource );
		~Lawn();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

	private:
		Lawn();
		Lawn( const Lawn& rhs );
		Lawn& operator=( const Lawn& rhs );

		const DirectionalLight& mDirectionalLightSource;
		StaticModel* mShape;
	};
}