#include "Taraxacum.h"
#include "StaticModel.h"

namespace Rendering
{
	RTTI_DEFINITIONS( Taraxacum )

		Taraxacum::Taraxacum( Game& game, Camera& camera, const DirectionalLight& directionalLightSource )
		:
		Actor( game, camera ),
		mDirectionalLightSource( directionalLightSource ),
		mShape( nullptr )
	{
	}
	Taraxacum::~Taraxacum()
	{
		DeleteObject( mShape );
	}

	void Taraxacum::Initialize()
	{
		SetScale( XMFLOAT3( 3.0f, 3.0f, 3.0f ) );
		Actor::Initialize();

		mShape = new StaticModel( *this, mDirectionalLightSource );
		mShape->SetModel( "Content\\Models\\Taraxacum\\Taraxacum.obj" );
		mShape->SetColorTexture( L"Content\\Models\\Taraxacum\\TaraxacumColorTexture.png" );
	
		mShape->Initialize();
		mShape->DrawAmbientDiffuse();
	}
	void Taraxacum::Update( const GameTime& gameTime )
	{
		Actor::Update( gameTime );
	}
	void Taraxacum::Draw( const GameTime& gameTime )
	{
		mShape->Draw( gameTime );
	}

}