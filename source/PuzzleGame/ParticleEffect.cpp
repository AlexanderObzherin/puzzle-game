#include "ParticleEffect.h"
#include "Game.h"
#include "GameException.h"
#include "Camera.h"
#include "Utility.h"
#include "Effect.h"

#include <WICTextureLoader.h>
#include "VectorHelper.h"
#include "MatrixHelper.h"
#include <SpriteBatch.h>
#include <SpriteFont.h>

#include "PointSprite.h"

//for std::bind stuff. Is needed to bind functions with parameters.
using namespace std::placeholders;

namespace Rendering
{
	RTTI_DEFINITIONS( SunDotStar )
		SunDotStar::SunDotStar( Game& game, Camera& camera, std::wstring texture )
		:
		Actor( game, camera ),
		mPointSprite( nullptr ),
		mTexture( texture ),
		mVelocity( Vector3Helper::Zero )
	{}
	SunDotStar::~SunDotStar()
	{
		DeleteObject( mPointSprite );
	}

	void SunDotStar::Initialize()
	{
		mPointSprite = new PointSprite( *this );
		mPointSprite->SetTexture( mTexture );
		mPointSprite->Initialize();

		Actor::Initialize();
	}
	void SunDotStar::Update( const GameTime & gameTime )
	{
		mPointSprite->Update( gameTime );

		XMVECTOR velocity = XMLoadFloat3( &mVelocity );
		XMVECTOR newPosition = PositionVector() + velocity;
		SetPosition( newPosition );


		Actor::Update( gameTime );
	}
	void SunDotStar::Draw( const GameTime & gameTime )
	{
		mPointSprite->Draw( gameTime );
	}

	RTTI_DEFINITIONS( ParticleEffect )

		ParticleEffect::ParticleEffect( Game& game, Camera& camera )
		:
		Actor( game, camera ),
		mSunDotStar1( nullptr ),
		mSunDotStar2( nullptr ),
		mRunningTime( 0.0f ),
		mStar1Angle( 0.0f ),
		mStar1Scale( 10.0f ),
		mStar2Angle( 0.0f ),
		mStar2Scale( 10.0f )
	{}

	ParticleEffect::~ParticleEffect()
	{
		DestroyParticles();
	}

	void ParticleEffect::Initialize()
	{
		Actor::Initialize();
	}
	void ParticleEffect::Update( const GameTime & gameTime )
	{
		if( mEffectState )
		{
			mEffectState( gameTime );
		}

		Actor::Update( gameTime );
	}
	void ParticleEffect::Draw( const GameTime & gameTime )
	{
		if( mSunDotStar1 )
		{
			mSunDotStar1->Draw( gameTime );
		}
		if( mSunDotStar2 )
		{
			mSunDotStar2->Draw( gameTime );
		}

	}

	void ParticleEffect::PlayEffect()
	{
		if( !mEffectState )
		{
			mEffectState = std::bind( &ParticleEffect::CreateParticles, this, _1 );
		}
	}
	void ParticleEffect::CreateParticles( const GameTime& gameTime )
	{
		mRunningTime = 0.0f;
		mStar1Angle = 0.0f;
		mStar1Scale = 10.0f;
		mStar2Angle = 0.0f;
		mStar2Scale = 10.0f;

		mSunDotStar1 = new SunDotStar( *mGame, *mCamera, L"Content\\Textures\\SundotStar1.png" );
		mSunDotStar1->Initialize();
		mSunDotStar1->SetPosition( Position() );
		mSunDotStar1->SetScale( XMFLOAT3( 10, 10, 10 ) );

		mSunDotStar2 = new SunDotStar( *mGame, *mCamera, L"Content\\Textures\\SundotStar2.png" );
		mSunDotStar2->Initialize();
		mSunDotStar2->SetPosition( Position() );
		mSunDotStar2->SetScale( XMFLOAT3( 10, 10, 10 ) );

		mEffectState = std::bind( &ParticleEffect::AdvanceEffect, this, _1 );

	}
	void ParticleEffect::AdvanceEffect( const GameTime & gameTime )
	{
		if( mSunDotStar1 )
		{
			///Update SunDotStar1
			mSunDotStar1->Update( gameTime );
			mStar1Angle += gameTime.ElapsedGameTime() * 60.0f;
			mSunDotStar1->SetRotation( XMFLOAT3( 0.0f, mStar1Angle, 0.0f ) );


			float deltaTime = XM_PI * static_cast<float>(gameTime.ElapsedGameTime());
			float deltaScale = sin( mRunningTime + deltaTime * 1.0f ) - sin( mRunningTime * 1.0f );
			mStar1Scale += deltaScale * 20.0f;
			mRunningTime += deltaTime;
			mSunDotStar1->SetScale( XMFLOAT3( mStar1Scale, mStar1Scale, mStar1Scale ) );
			if( mStar1Scale < 6 )
			{
				DeleteObject( mSunDotStar1 );
				mSunDotStar1 = nullptr;
			}

		}

		if( mSunDotStar2 )
		{
			///Update SunDotStar2
			mSunDotStar2->Update( gameTime );
			mStar2Angle -= gameTime.ElapsedGameTime() * 60.0f;
			mSunDotStar2->SetRotation( XMFLOAT3( 0.0f, mStar2Angle, 0.0f ) );


			float deltaTime = XM_PI * static_cast<float>(gameTime.ElapsedGameTime());
			float deltaScale = sin( mRunningTime + deltaTime * 1.0f ) - sin( mRunningTime * 1.0f );
			mStar2Scale += deltaScale * 20.0f;
			mRunningTime += deltaTime;
			mSunDotStar2->SetScale( XMFLOAT3( mStar2Scale, mStar2Scale, mStar2Scale ) );
			if( mStar1Scale < 6 )
			{
				DeleteObject( mSunDotStar2 );
				mSunDotStar2 = nullptr;
			}
		}

		if( !mSunDotStar1 && !mSunDotStar2 )
		{
			mEffectState = nullptr;
		}
	}
	void ParticleEffect::DestroyParticles()
	{
		DeleteObject( mSunDotStar1 );
		DeleteObject( mSunDotStar2 );
	}



}