#include "StaticModel.h"
#include "StaticModelMaterial.h"
#include "Game.h"
#include "GameException.h"
#include "VectorHelper.h"
#include "MatrixHelper.h"
#include "ColorHelper.h"
#include "Camera.h"
#include "Model.h"
#include "Mesh.h"
#include "Utility.h"
#include "DirectionalLight.h"
//#include "Keyboard.h"
#include <WICTextureLoader.h>
#include "RasterizerStates.h"

#include "RenderStateHelper.h"

#include <iomanip>
#include <SpriteBatch.h>
#include <SpriteFont.h>
#include <sstream>

namespace Rendering
{
	const float StaticModel::LightModulationRate = UCHAR_MAX;
	const XMFLOAT2 StaticModel::LightRotationRate = XMFLOAT2( XM_2PI, XM_2PI );

	StaticModel::StaticModel( Actor& actor, const DirectionalLight& directionalLightSource )
		:
		mActor( actor ),
		mDirectionalLightSource( directionalLightSource ),
		mEffect( nullptr ),
		mMaterial( nullptr ),
		mColorTextureShaderResourceView( nullptr ),
		mNormalMapShaderResourceView( nullptr ),
		mVertexBuffer( nullptr ),
		mIndexBuffer( nullptr ),
		mIndexCount( 0 ),

		mAmbientColor( 1.0f, 1.0f, 1.0f, 0.0f ),
		mSpecularColor( 1.0f, 1.0f, 1.0f, 1.0f ),
		mSpecularPower( 25.0f ),
		mModelFilename( "Content\\Models\\Proxy\\Sphere.obj" ),

		bDrawWireframeOnly( false ),

		mRenderStateHelper( nullptr ),
		mSpriteBatch( nullptr ),
		mSpriteFont( nullptr )
	{
		SetNormalMapTexture( L"Content\\Textures\\DefaultNormalMap.png" );
	}
	StaticModel::~StaticModel()
	{
		DeleteObject( mSpriteFont );
		DeleteObject( mSpriteBatch );
		DeleteObject( mRenderStateHelper );
		ReleaseObject( mIndexBuffer );
		ReleaseObject( mVertexBuffer );
		ReleaseObject( mNormalMapShaderResourceView );
		ReleaseObject( mColorTextureShaderResourceView );
		DeleteObject( mMaterial );
		DeleteObject( mEffect );
	}
	void StaticModel::SetModel( const std::string modelFilenamePath )
	{
		mModelFilename = modelFilenamePath;
	}
	void StaticModel::SetColorTexture( const std::wstring textureFilenamePath )
	{
		mColorTextureFilename = textureFilenamePath;
	}
	void StaticModel::SetNormalMapTexture( const std::wstring textureFilenamePath )
	{
		mNormalMapTextureFilename = textureFilenamePath;
	}
	void StaticModel::DrawWireframeOnly( bool in )
	{
		bDrawWireframeOnly = in;
	}
	void StaticModel::SetText( std::wstring in )
	{
		mText = in;
	}
	void StaticModel::DrawAmbientOnly()
	{
		techniqueIterator = mMaterial->GetEffect()->Techniques().begin();
		techniqueIterator++;
		techniqueIterator++;
		mMaterial->SetCurrentTechnique( *techniqueIterator );
	}
	void StaticModel::DrawAmbientDiffuse()
	{
		techniqueIterator = mMaterial->GetEffect()->Techniques().begin();
		techniqueIterator++;
		mMaterial->SetCurrentTechnique( *techniqueIterator );
	}
	void StaticModel::DrawAmbientDiffuseWithNormalMap()
	{
		techniqueIterator = mMaterial->GetEffect()->Techniques().begin();
		techniqueIterator++;
		techniqueIterator++;
		techniqueIterator++;
		techniqueIterator++;
		mMaterial->SetCurrentTechnique( *techniqueIterator );
	}
	void StaticModel::DrawPhongWithNormalMap()
	{
		techniqueIterator = mMaterial->GetEffect()->Techniques().begin();
		techniqueIterator++;
		techniqueIterator++;
		techniqueIterator++;
		mMaterial->SetCurrentTechnique( *techniqueIterator );
	}
	void StaticModel::Initialize()
	{
		SetCurrentDirectory( Utility::ExecutableDirectory().c_str() );

		std::unique_ptr<Model> model( new Model( *mActor.GetGame(), mModelFilename, true ) );

		// Initialize the material
		mEffect = new Effect( *mActor.GetGame() );
		mEffect->LoadCompiledEffect( L"Content\\Effects\\StaticModel.cso" );

		mMaterial = new StaticModelMaterial();
		mMaterial->Initialize( *mEffect );

		techniqueIterator = mMaterial->GetEffect()->Techniques().begin();

		Mesh* mesh = model->Meshes().at( 0 );
		mMaterial->CreateVertexBuffer( mActor.GetGame()->Direct3DDevice(), *mesh, &mVertexBuffer );
		mesh->CreateIndexBuffer( &mIndexBuffer );
		mIndexCount = mesh->Indices().size();

		//if texture filename has not been initialized, set drawmode to wireframe
		if( !mColorTextureFilename.size() )
		{
			bDrawWireframeOnly = true;
		}

		if( !bDrawWireframeOnly )
		{
			HRESULT hr = DirectX::CreateWICTextureFromFile( mActor.GetGame()->Direct3DDevice(), mActor.GetGame()->Direct3DDeviceContext(), mColorTextureFilename.c_str(), nullptr, &mColorTextureShaderResourceView );
			if( FAILED( hr ) )
			{
				throw GameException( "CreateWICTextureFromFile() failed.", hr );
			}

			hr = DirectX::CreateWICTextureFromFile( mActor.GetGame()->Direct3DDevice(), mActor.GetGame()->Direct3DDeviceContext(), mNormalMapTextureFilename.c_str(), nullptr, &mNormalMapShaderResourceView );
			if( FAILED( hr ) )
			{
				throw GameException( "CreateWICTextureFromFile() failed.", hr );
			}
		}

		mRenderStateHelper = new RenderStateHelper( *mActor.GetGame() );

		mSpriteBatch = new SpriteBatch( mActor.GetGame()->Direct3DDeviceContext() );
		mSpriteFont = new SpriteFont( mActor.GetGame()->Direct3DDevice(), L"Content\\Fonts\\TimesNewRoman_12_bold.spritefont" );

	}
	void StaticModel::Update( const GameTime & gameTime )
	{
	}
	void StaticModel::Draw( const GameTime & gameTime )
	{
		ID3D11DeviceContext* direct3DDeviceContext = mActor.GetGame()->Direct3DDeviceContext();
		direct3DDeviceContext->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST );

		Pass* pass = mMaterial->CurrentTechnique()->Passes().at( 0 );
		ID3D11InputLayout* inputLayout = mMaterial->InputLayouts().at( pass );
		direct3DDeviceContext->IASetInputLayout( inputLayout );

		UINT stride = mMaterial->VertexSize();
		UINT offset = 0;
		direct3DDeviceContext->IASetVertexBuffers( 0, 1, &mVertexBuffer, &stride, &offset );
		direct3DDeviceContext->IASetIndexBuffer( mIndexBuffer, DXGI_FORMAT_R32_UINT, 0 );

		XMMATRIX worldMatrix = XMLoadFloat4x4( &mActor.WorldMatrix() );
		XMMATRIX wvp = worldMatrix * mActor.GetCamera()->ViewMatrix() * mActor.GetCamera()->ProjectionMatrix();
		XMVECTOR ambientColor = XMLoadColor( &mAmbientColor );
		XMVECTOR specularColor = XMLoadColor( &mSpecularColor );

		//
		mMaterial->WorldViewProjection() << wvp;
		mMaterial->World() << worldMatrix;
		mMaterial->SpecularColor() << specularColor;
		mMaterial->SpecularPower() << mSpecularPower;
		mMaterial->AmbientColor() << ambientColor;
		mMaterial->LightColor() << mDirectionalLightSource.ColorVector();
		mMaterial->LightDirection() << mDirectionalLightSource.DirectionVector();
		mMaterial->ColorTexture() << mColorTextureShaderResourceView;
		mMaterial->NormalMapTexture() << mNormalMapShaderResourceView;
		mMaterial->CameraPosition() << mActor.GetCamera()->PositionVector();
		//

		pass->Apply( 0, direct3DDeviceContext );

		if( bDrawWireframeOnly )
		{
			mActor.GetGame()->Direct3DDeviceContext()->RSSetState( RasterizerStates::Wireframe );
		}

		direct3DDeviceContext->DrawIndexed( mIndexCount, 0, 0 );

		mRenderStateHelper->SaveAll();
		mSpriteBatch->Begin();
		std::wostringstream text;
		text << std::setprecision( 4 ) << mText.c_str();
		mSpriteFont->DrawString( mSpriteBatch, text.str().c_str(), XMFLOAT2( mActor.Position().x, mActor.Position().y ), DirectX::Colors::Red );
		mSpriteBatch->End();
		mRenderStateHelper->RestoreAll();

	}
	void StaticModel::UpdateAmbientLight( const GameTime & gameTime )
	{
	}
	void StaticModel::UpdateSpecularLight( const GameTime & gameTime )
	{
	}
}