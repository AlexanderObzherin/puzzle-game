#include "Bumblebee.h"

#include "AnimatedModel.h"
#include "GameTime.h"
#include "BlockPath.h"
#include "GameGrid.h"
#include "Block.h"
#include "Point.h"
#include "LawnBlockPath.h"

//for std::bind stuff. Is needed to bind functions with parameters.
using namespace std::placeholders;

namespace Rendering
{
	RTTI_DEFINITIONS( Bumblebee )

		Bumblebee::Bumblebee( Game& game, Camera& camera, const DirectionalLight& directionalLightSource )
		:
		Insect( game, camera, directionalLightSource ),
		mTimer( 0.0f )
	{}
	Bumblebee::~Bumblebee()
	{
		
	}

	void Bumblebee::Initialize()
	{
		Insect::Initialize();

		SetScale( XMFLOAT3( 0.8f, 0.8f, 0.8f ) );

		mAnimatedModel = std::unique_ptr<AnimatedModel>( new AnimatedModel( *this, mDirectionalLightSource ) );
		mAnimatedModel->SetModelName( "Content\\Models\\Bumblebee\\Bumblebee.dae" );
		mAnimatedModel->SetColorTexture( L"Content\\Models\\Bumblebee\\BumblebeeColorTexture.png" );
		mAnimatedModel->SetNormalMapTexture( L"Content\\Models\\Bumblebee\\BumblebeeNormalMapTexture.png" );
		mAnimatedModel->Initialize();
		mAnimatedModel->DrawAmbientDiffuseWithNormalMap();

		mAnimatedModel->AddAnimation( "BumblebeeMoving", 0.0f, 1.2f );
		mAnimatedModel->AddAnimation( "BumblebeeWingsMoving", 1.7f, 1.8f );
		mAnimatedModel->SetAnimation( "BumblebeeMoving" );

		mVelocityFactor = 15.0f;
	}
	void Bumblebee::Update( const GameTime & gameTime )
	{
		Insect::Update( gameTime );

	}
	void Bumblebee::Draw( const GameTime & gameTime )
	{
		Insect::Draw( gameTime );
	}

	void Bumblebee::PlayWalkingScenario()
	{
		SetPosition( CreateRandomPosition() );
		mAnimatedModel->SetAnimation( "BumblebeeWingsMoving" );
		mTargetPoint = SearchForClosestFreePoint();
		if( mTargetPoint )
		{
			mActionState = std::bind( &Bumblebee::PickFlower, this, _1 );
		}
		else
		{
			//If there is no free flower for this, set it inactive
			SetEnabled( false );
		}
	}
	void Bumblebee::SmoothRotation( const GameTime & gameTime )
	{
		//Project Velocity direction unto XZ plane first
		XMVECTOR cross1 = CrossProduct( XMLoadFloat3( &mVelocityDirection), XMLoadFloat3(&Vector3Helper::Up) );
		XMVECTOR cross2 = CrossProduct( XMLoadFloat3( &Vector3Helper::Up), cross1 );
		XMFLOAT3 proj;
		XMStoreFloat3( &proj, cross2 );

		mTargetYaw = GetRotator( proj ).y;

		if ((int)mCurrentYaw > (int)mTargetYaw)
		{
			mCurrentYaw -= mAngularVelocity * gameTime.ElapsedGameTime();
		}
		else
		{
			mCurrentYaw += mAngularVelocity * gameTime.ElapsedGameTime();
		}

		SetRotation(XMFLOAT3(0.0f, mCurrentYaw, 0.0f));
	}
	void Bumblebee::PickFlower( const GameTime & gameTime )
	{
		//Move to target points
		if( mTargetPoint )
		{
			XMVECTOR distanceVector = (mTargetPoint->PositionVector() - this->PositionVector());

			float distance = XMVector3Length( distanceVector ).m128_f32[0];
			if( (int)distance > 0 )
			{
				XMStoreFloat3( &mVelocityDirection, distanceVector * 0.05f );

				if( mTargetPoint )
				{
					mTargetPoint->GetBlock().SetIsOwnedByInsect( true );
				}
			}
			else
			{
				//
				std::random_device randomDevice;
				std::default_random_engine randomGenerator( randomDevice() );
				std::uniform_int_distribution<int> distanceDistribution( -180, 180 );

				mTargetYaw = distanceDistribution( randomGenerator );

				mAnimatedModel->SetAnimation( "BumblebeeMoving" );
				mActionState = bind( &Bumblebee::Sit, this, _1 );
			}


			Move( gameTime );
			SmoothRotation( gameTime );
		}
	}
	void Bumblebee::Sit( const GameTime & gameTime )
	{
		SmoothRotation( gameTime );

		mTimer += gameTime.ElapsedGameTime();

		if ( mTimer > 20.0f )
		{
			mTargetPoint->GetBlock().SetIsOwnedByInsect( false );

			mAdditionalPoint =  std::make_unique<Point>( *mGame, *mCamera, mDirectionalLightSource, mTargetPoint->GetBlock() );
			mTargetPoint = mAdditionalPoint.get();
			
			//Create random pos above the lawn
			mTargetPoint->SetPosition( CreateRandomPosition() );
			
			mActionState = bind( &Bumblebee::FlyAway, this, _1 );
		}
		
	}
	void Bumblebee::FlyAway( const GameTime & gameTime )
	{
		mAnimatedModel->SetAnimation( "BumblebeeWingsMoving" );

		//Move to target points
		if( mTargetPoint )
		{
			XMVECTOR distanceVector = mTargetPoint->PositionVector() - this->PositionVector();
			float distance = XMVector3Length( distanceVector ).m128_f32[0];
			if( (int)distance > 0 )
			{
				XMStoreFloat3( &mVelocityDirection, XMVector3Normalize( distanceVector ) );

				if( mTargetPoint )
				{
						mTargetPoint->GetBlock().SetIsOwnedByInsect( true );
				}
			}
			else
			{
				mAnimatedModel->SetAnimation( "BumblebeeMoving" );

				mActionState = nullptr;
				SetEnabled( false );
			}


			Move( gameTime );
			SmoothRotation( gameTime );
		}

	}

}