#include "Level3.h"
#include "GameException.h"
#include "WICTextureLoader.h"
#include "Level1.h"
#include "RenderStateHelper.h"
#include <sstream>
#include <iomanip>
#include <SpriteBatch.h>
#include <SpriteFont.h>
#include "Game.h"
#include "Utility.h"
#include "ColorHelper.h"
#include "Keyboard.h"
#include "Camera.h"
#include "ThirdPersonCameraController.h"
#include "FirstPersonCameraController.h"
#include "CameraTarget.h"

#include "LadybugPlayable.h"
#include "GameGrid.h"
#include "LawnBlockPath.h"

#include "Bumblebee.h"
#include "AglaisUrticae.h"
#include "NymphalisAntiopa.h"
#include "PierisBrassicae.h"
#include "PolyommatusSemiargus.h"
#include "PyrgusAndromedae.h"

#include "StartScene.h"
#include "IntermediateScene.h"

#include "Block.h"
#include "Point.h"
//
#include "Lawn.h"
#include "ParticleEffect.h"
#include "AudioPlayer.h"

#include "AraneusAngulatus.h"

//for std::bind stuff. Is needed to bind functions with parameters.
using namespace std::placeholders;

namespace Rendering
{
	RTTI_DEFINITIONS( Level3 )

	Level3::Level3(Game& game, SceneManager& sceneManager)
		:
		LevelsCommon(game, sceneManager),
		mTimer( 0.0f ),
		bGameIsOver( false ),
		bGameIsFinished( false ),
		bIsWarning( false )
	{
	}
	Level3::~Level3()
	{
		for (auto& it : mAraneusAngulatuss)
		{
			DeleteObject(it);
		}
	}

	void Level3::Initialize()
	{
		//Set level's name displayed on screen
		mLevelNum = 3;

		//Set num of grids in level
		mNumberOfGameGrids = 13;

		//Level scenario defined as map of void func.
		//We iterate through them in level by switching to determined function.
		mScenarioTerms["01"] = std::bind(&Level3::ScenarioTerm01, this, _1);
		mScenarioTerms["02"] = std::bind(&Level3::ScenarioTerm02, this, _1);
		mScenarioTerms["03"] = std::bind(&Level3::ScenarioTerm03, this, _1);
		mScenarioTerms["04"] = std::bind(&Level3::ScenarioTerm04, this, _1);
		mScenarioTerms["05"] = std::bind(&Level3::ScenarioTerm05, this, _1);
		mScenarioTerms["06"] = std::bind(&Level3::ScenarioTerm06, this, _1);

		//Set start scenario term
		mCurrentScenarioTerm = mScenarioTerms["01"];

		LevelsCommon::Initialize();

		////Create list for randomizer of Insect Spawner
		mFlyingInsectsList = new Randomizer<Insect>();
		auto CreateBumblebee = [&]() { return new Bumblebee( *mGame, *mCamera, *mDirectionalLight ); };
		mFlyingInsectsList->AddToSetList( 30, CreateBumblebee );
		auto CreateAglaisUrticae = [&]() { return new AglaisUrticae( *mGame, *mCamera, *mDirectionalLight ); };
		mFlyingInsectsList->AddToSetList( 30, CreateAglaisUrticae );
		auto CreateNymphalisAntiopa = [&]() { return new NymphalisAntiopa( *mGame, *mCamera, *mDirectionalLight ); };
		mFlyingInsectsList->AddToSetList( 30, CreateNymphalisAntiopa );
		auto CreatePierisBrassicae = [&]() { return new PierisBrassicae( *mGame, *mCamera, *mDirectionalLight ); };
		mFlyingInsectsList->AddToSetList( 30, CreatePierisBrassicae );
		auto CreatePolyommatusSemiargus = [&]() { return new PolyommatusSemiargus( *mGame, *mCamera, *mDirectionalLight ); };
		mFlyingInsectsList->AddToSetList( 30, CreatePolyommatusSemiargus );
		auto CreatePyrgusAndromedae = [&]() { return new PyrgusAndromedae( *mGame, *mCamera, *mDirectionalLight ); };
		mFlyingInsectsList->AddToSetList( 30, CreatePyrgusAndromedae );

		mAudioPlayer->PlayAudioBackground("Content\\Audio\\MorningInTheMagicalForest.mp3", 0.075f);

		mAraneusAngulatuss.push_back(new AraneusAngulatus(*mGame, *mCamera, *mDirectionalLight));
		mAraneusAngulatuss.back()->Initialize();
		mAraneusAngulatuss.back()->SetBlockPath(*mGameGridIterator);

	}
	void Level3::Update(const GameTime & gameTime)
	{
		if ( !bGameIsPaused )
		{
			for (auto& it : mAraneusAngulatuss)
			{
				it->Update(gameTime);
			}
		}

		LevelsCommon::Update(gameTime);
	}
	void Level3::Draw(const GameTime & gameTime)
	{
		for (auto& it : mAraneusAngulatuss)
		{
			it->Draw(gameTime);
		}



		LevelsCommon::Draw(gameTime);

		mRenderStateHelper->SaveAll();

		if ( bGameIsOver )
		{
			mSpriteBatch->Begin();
			mSpriteFont->DrawString(mSpriteBatch, L"Game Over", XMFLOAT2((int)((mGame->ScreenWidth() / 2) - (16.6f * 9 / 2))/* - (letter w * num of let / 2)*/,
				(int)(mGame->ScreenHeight() / 5)), DirectX::Colors::Red);
			mSpriteBatch->End();
		}

		if ( bGameIsFinished )
		{
			mSpriteBatch->Begin();
			mSpriteFont->DrawString(mSpriteBatch, L"Congratulations!", XMFLOAT2((int)((mGame->ScreenWidth() / 2) - (16.6f * 16 / 2))/* - (letter w * num of let / 2)*/,
				(int)(mGame->ScreenHeight() / 5)), DirectX::Colors::Red);
			mSpriteBatch->End();
		}

		if ( bIsWarning )
		{
			mSpriteBatch->Begin();
			mSpriteFont->DrawString(mSpriteBatch, L"Beware The Spider!", XMFLOAT2((int)((mGame->ScreenWidth() / 2) - (16.6f * 18 / 2))/* - (letter w * num of let / 2)*/,
				(int)(mGame->ScreenHeight() / 5)), DirectX::Colors::Red);
			mSpriteBatch->End();
		}

		mRenderStateHelper->RestoreAll();
	}

	void Level3::ScenarioTerm01(const GameTime & gameTime)
	{
		bIsItSpawning = false;

		(*mGameGridIterator)->SetEnabled(false);

		mLadybugPlayable->SetPosition((*mGameGridIterator)->GetStartBlock()->GetPointPath().at(1)->Position());
		mLadybugPlayable->SetIsRunning(false);

		mAraneusAngulatuss.back()->SetPosition((*mGameGridIterator)->GetFinishBlock()->GetPointPath().at(1)->Position());
		mAraneusAngulatuss.back()->SetIsRunning(false);

		if (mTimer < 1.0f)
		{
			//Set camera target to ladybug
			mCameraTarget->SetPosition(mLadybugPlayable->Position()/*(*mGameGridIterator)->GetCenter()*/);
			mCameraTarget->SetNewPosition(mLadybugPlayable->Position()/*(*mGameGridIterator)->GetCenter()*/);
		}
		if (mTimer > 10.0f)
		{
			bIsWarning = true;
			//Set camera target to spider
			mCameraTarget->SetPosition(mAraneusAngulatuss.back()->Position());
			mCameraTarget->SetNewPosition(mAraneusAngulatuss.back()->Position());
		}

		mCameraController->SetEnabled(false);
		mCameraController->SetDistance(50.0f);
		mCameraController->SetAngleView(CameraViewAngle::angle45deg);

		//Switch to next
		SwitchScenarioToTerm("02");
	}
	void Level3::ScenarioTerm02(const GameTime & gameTime)
	{
		mTimer += gameTime.ElapsedGameTime();
		if (mTimer > 10.0f && mTimer < 10.5f)
		{
			mTimer++;
			SwitchScenarioToTerm("01");
		}

		if (mTimer > 25.0f)
		{
			bIsWarning = false;
			mTimer = 0.0f;
			SwitchScenarioToTerm("03");
		}
	}
	void Level3::ScenarioTerm03(const GameTime & gameTime)
	{
		bGridIsPlaying = false;
		mLadybugWingMovingTimer = 0.0f;

		if (mGameGridIterator != mGameGrids.end() && mLawnBlockPathIterator != mLawnBlockPath.end())
		{
			//Camera
			mCameraController->SetEnabled(true);
			mCameraController->SetDistance(100.0f);
			mCameraController->SetAngleView(CameraViewAngle::top);

			//Set new game grid as enabled
			(*mGameGridIterator)->SetEnabled(true);

			//Set cameraTarget to move to this new grid
			mCameraTarget->SetNewPosition(dynamic_cast<GameGrid*>(*mGameGridIterator)->GetCenter());

			//Switch characters to walk through new gameGrid

			mLadybugPlayable->SetBlockPath(*mGameGridIterator);
			mLadybugPlayable->SetPosition((*mGameGridIterator)->GetStartBlock()->GetPointPath().at(1)->Position());
			mLadybugPlayable->SetIsRunning(true);

			mAraneusAngulatuss.back()->SetBlockPath(*mGameGridIterator);
			mAraneusAngulatuss.back()->SetPosition((*mGameGridIterator)->GetFinishBlock()->GetPointPath().at(1)->Position());
			mAraneusAngulatuss.back()->SetIsRunning(true);
		}

		bIsItSpawning = true;

		SwitchScenarioToTerm("04");
	}
	void Level3::ScenarioTerm04(const GameTime& gameTime)
	{
		//Menu
		if (mKeyboard->WasKeyPressedThisFrame(DIK_ESCAPE))
		{
			bGameIsPaused = !bGameIsPaused;

			mMenuBgScale = 1.0f;
		}
		if (mKeyboard->WasKeyPressedThisFrame(DIK_UPARROW) && !(mMenuActiveOption & Menu::ReturnToGame) && bGameIsPaused)
		{
			mMenuActiveOption = (Menu)(mMenuActiveOption >> 1);
		}
		if (mKeyboard->WasKeyPressedThisFrame(DIK_DOWNARROW) && !(mMenuActiveOption & Menu::ExitMenu) && bGameIsPaused)
		{
			mMenuActiveOption = (Menu)(mMenuActiveOption << 1);
		}
		if (mKeyboard->WasKeyPressedThisFrame(DIK_RETURN))
		{
			if ((mMenuActiveOption & Menu::ReturnToGame))
			{
				bGameIsPaused = !bGameIsPaused;
			}
			if ((mMenuActiveOption & Menu::StartLevel))
			{
				//Start this level again
				mLevelName = Scene::LevelName::LevelName3;
				SceneSwitching( new IntermediateScene( *mGame, mSceneManager ) );
				return;
			}
			if ((mMenuActiveOption & Menu::StartLevel1))
			{
				//Start game from first level
				mLevelName = Scene::LevelName::LevelName3;
				SceneSwitching( new IntermediateScene( *mGame, mSceneManager ) );
				return;
			}
			if ((mMenuActiveOption & Menu::ExitMenu))
			{
				//Exit to Windows
				mGame->Exit();
			}
		}
		///

		{
			//Check if Ladybug collided with middle point of finish block
			XMVECTOR distanceVector = XMVector3Length(mLadybugPlayable->PositionVector()
				- (dynamic_cast<GameGrid*>(*mGameGridIterator))->GetFinishBlock()->GetPointPath().at(1)->PositionVector());
			float distance = distanceVector.m128_f32[0];
			if (distance <= 2.0f)
			{
				//Play audio effect and particle effect once,
				//Wings moving for a few seconds and
				//Switch to next GameGrid
				if (mLadybugPlayable->GetIsRunning())
				{
					mParticleEffect->SetPosition(mLadybugPlayable->PositionVector());
					mParticleEffect->PlayEffect();

					if (!(mAudioPlayer->IsSamplePlaying("Content\\Audio\\BellTree.mp3")))
					{
						mAudioPlayer->PlaySample("Content\\Audio\\BellTree.mp3");
					}
				}

				mLadybugPlayable->SetIsRunning(false);
			}
		}

		{
			//Check if Gastrophysa Polygoni collided with middle point of Start Block
			XMVECTOR distanceVector = XMVector3Length(mAraneusAngulatuss.back()->PositionVector()
				- (dynamic_cast<GameGrid*>(*mGameGridIterator))->GetStartBlock()->GetPointPath().at(1)->PositionVector());
			float distance = distanceVector.m128_f32[0];
			if (distance <= 2.0f)
			{
				mAraneusAngulatuss.back()->SetIsRunning(false);
			}
		}

		if (!bGridIsPlaying)
		{
			if (mCameraTarget->GetDistanceBetweenTarget() > 8.0f)
			{
				mCameraController->SetDistance(160.0f);
			}
			else
			{
				//delete previous gastrophysa
				if (*mGameGridIterator != mGameGrids.front())
				{
					mAraneusAngulatuss.pop_front();
				}

				mCameraController->SetDistance(100.0f);
				bGridIsPlaying = true;
				if (mGameGridIterator != mGameGrids.begin())
				{
					mLawnBlockPathIterator--;
					mLawnBlockPathIterator--;
					(*mLawnBlockPathIterator)->SetEnabled(false);
					mLawnBlockPathIterator++;
					mLawnBlockPathIterator++;
				}
			}
		}

		//In this level we switch to next gamegrid then both characters are set to IsRunning false,
		//i.e. both of them gain opposite finish block
		if (!(mLadybugPlayable->GetIsRunning()) && !(mAraneusAngulatuss.back()->GetIsRunning()))
		{
			(*mGameGridIterator)->SetEnabled(false);
			mGameGridIterator++;

			//Here we start to keep enabled to draw previous, current and next lawn grid
			mLawnBlockPathIterator++;
			(*mLawnBlockPathIterator)->SetEnabled(true);
			mLawnBlockPathIterator++;
			if (mLawnBlockPathIterator != mLawnBlockPath.end())
			{
				(*mLawnBlockPathIterator)->SetEnabled(true);
			}
			mLawnBlockPathIterator--;
			////

			if (mGameGridIterator != mGameGrids.end())
			{
				//Here we need to create new Gastrophysa on new gamegrid.
				mAraneusAngulatuss.push_back(new AraneusAngulatus(*mGame, *mCamera, *mDirectionalLight));
				mAraneusAngulatuss.back()->Initialize();
				mAraneusAngulatuss.back()->SetBlockPath(*mGameGridIterator);
				SwitchScenarioToTerm("03");
			}
			else
			{
				//If we get end of gameGrid then we need to switch to next scene
				mGameGridIterator--;
				mLawnBlockPathIterator--;

				mAudioPlayer->StopAudioBackGround();
				mAudioPlayer->PlaySample( "Content\\Audio\\Finish.mp3", false, 0.075f );
				SwitchScenarioToTerm("05");
			}
		}

		UpdateInsectSpawner(gameTime);

		//Check if two playable characters on gamegrid is collided, them turn them back
		auto distanceVectorBetChar = mLadybugPlayable->PositionVector() - mAraneusAngulatuss.back()->PositionVector();
		auto distanceVectorSq = XMVector3Dot(distanceVectorBetChar, distanceVectorBetChar);
		float distanceBetweenCharacters = sqrtf(distanceVectorSq.m128_f32[0] + distanceVectorSq.m128_f32[1] + distanceVectorSq.m128_f32[2]);

		if ((int)distanceBetweenCharacters < 6)
		{
			bGameIsOver = true;
			bIsItSpawning = false;

			mLadybugPlayable->SetIsRunning( false );
			mAraneusAngulatuss.back()->SetIsRunning( false );

			mCameraTarget->SetPosition(mLadybugPlayable->Position()/*(*mGameGridIterator)->GetCenter()*/);
			mCameraTarget->SetNewPosition(mLadybugPlayable->Position()/*(*mGameGridIterator)->GetCenter()*/);

			mCameraController->SetEnabled(false);
			mCameraController->SetDistance(50.0f);
			mCameraController->SetAngleView(CameraViewAngle::angle45deg);

			SwitchScenarioToTerm( "06" );
		}

	}
	void Level3::ScenarioTerm05(const GameTime & gameTime)
	{
		bGameIsFinished = true;
		bIsItSpawning = false;

		//Start menu is activated
		(*mGameGridIterator)->SetEnabled(false);

		mLadybugPlayable->SetIsRunning(false);


		mCameraTarget->SetPosition(mLadybugPlayable->Position()/*(*mGameGridIterator)->GetCenter()*/);
		mCameraTarget->SetNewPosition(mLadybugPlayable->Position()/*(*mGameGridIterator)->GetCenter()*/);

		mCameraController->SetEnabled(false);
		mCameraController->SetDistance(50.0f);
		mCameraController->SetAngleView(CameraViewAngle::angle45deg);

		//mCurrentScenarioTerm = nullptr;

		mLadybugWingMovingTimer += gameTime.ElapsedGameTime();
		if (mLadybugWingMovingTimer > 20.0f)
		{
			mLevelName = Scene::LevelName::LevelName1;
			SceneSwitching( new IntermediateScene( *mGame, mSceneManager ) );
		}
	}
	void Level3::ScenarioTerm06(const GameTime & gameTime)
	{
		mTimer += gameTime.ElapsedGameTime();
	
		if (mTimer > 20.0f)
		{
			mLevelName = Scene::LevelName::LevelName1;
			SceneSwitching( new IntermediateScene( *mGame, mSceneManager ) );
		}
	}


}