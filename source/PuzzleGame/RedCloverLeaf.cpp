#include "RedCloverLeaf.h"
#include "StaticModel.h"

namespace Rendering
{
	RTTI_DEFINITIONS( RedCloverLeaf )

		RedCloverLeaf::RedCloverLeaf( Game& game, Camera& camera, const DirectionalLight& directionalLightSource )
		:
		Actor( game, camera ),
		mDirectionalLightSource( directionalLightSource ),
		mShape( nullptr )
	{
	}
	RedCloverLeaf::~RedCloverLeaf()
	{
		DeleteObject( mShape );
	}

	void RedCloverLeaf::Initialize()
	{
		SetScale( XMFLOAT3( 6.0f, 6.0f, 6.0f ) );
		Actor::Initialize();

		mShape = new StaticModel( *this, mDirectionalLightSource );
		mShape->SetModel( "Content\\Models\\RedClover\\RedCloverLeaf.obj" );
		mShape->SetColorTexture( L"Content\\Models\\RedClover\\RedCloverColorTexture.png" );
		mShape->SetNormalMapTexture( L"Content\\Models\\RedClover\\RedCloverNormalMapTexture.png" );

		mShape->Initialize();
		mShape->DrawAmbientDiffuseWithNormalMap();
	}
	void RedCloverLeaf::Update( const GameTime& gameTime )
	{
		Actor::Update( gameTime );
	}
	void RedCloverLeaf::Draw( const GameTime& gameTime )
	{
		mShape->Draw( gameTime );
	}

}