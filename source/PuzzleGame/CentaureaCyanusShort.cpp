#include "CentaureaCyanusShort.h"
#include "StaticModel.h"

namespace Rendering
{
	RTTI_DEFINITIONS( CentaureaCyanusShort )

		CentaureaCyanusShort::CentaureaCyanusShort( Game& game, Camera& camera, const DirectionalLight& directionalLightSource )
		:
		Actor( game, camera ),
		mDirectionalLightSource( directionalLightSource ),
		mShape( nullptr )
	{
	}
	CentaureaCyanusShort::~CentaureaCyanusShort()
	{
		DeleteObject( mShape );
	}

	void CentaureaCyanusShort::Initialize()
	{
		SetScale( XMFLOAT3( 4.5f, 4.5f, 4.5f ) );
		Actor::Initialize();

		mShape = new StaticModel( *this, mDirectionalLightSource );
		mShape->SetModel( "Content\\Models\\CentaureaCyanus\\CentaureaCyanusShort.obj" );
		mShape->SetColorTexture( L"Content\\Models\\CentaureaCyanus\\CentaureaCyanusColorTexture.png" );

		mShape->Initialize();
		mShape->DrawAmbientDiffuse();
		
	}
	void CentaureaCyanusShort::Update( const GameTime& gameTime )
	{
		Actor::Update( gameTime );
		
	}
	void CentaureaCyanusShort::Draw( const GameTime& gameTime )
	{
		mShape->Draw( gameTime );
	}

}