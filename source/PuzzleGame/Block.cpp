#include "Block.h"
#include "StaticModel.h"
#include "Point.h"
#include "GameException.h"
#include "GameTime.h"
#include "VectorHelper.h"
#include <random>

#include "Chamomile.h"
#include "Hepatica.h"
#include "Taraxacum.h"
#include "RedCloverLeaf.h"
#include "RedCloverShort.h"
#include "RedClover.h"
#include "CentaureaCyanusShort.h"
#include "CentaureaCyanus.h"
#include "Grass.h"

namespace Rendering
{
	RTTI_DEFINITIONS( Block )

	Block::Block( Game& game, Camera& camera, const DirectionalLight& directionalLightSource )
		:
		Actor( game, camera ),
		mGridPos( ),
		mDirectionalLightSource( directionalLightSource ),
		mShape( nullptr ),
		mDistanceFloat( 0.0f ),
		bIsActive( false ),
		bIsOwnedByInsect( false ),
		mVelocityFactor( 6.0f )
	{
	}
	Block::~Block()
	{
		DeleteObject( mShape );

		for(auto& it : mPath)
		{
			DeleteObject( it );
		}
	}
	void Block::Initialize()
	{
		Actor::Initialize();

		SetScale( XMFLOAT3( 10.0f, 10.0f, 10.0f ) );
	}
	void Block::Update( const GameTime& gameTime )
	{
		Actor::Update( gameTime );

		if( mShape )
		{
			mShape->Update( gameTime );
		}

		for( auto& it : mPath )
		{
			it->Update( gameTime );
		}

	}
	void Block::Draw( const GameTime& gameTime )
	{
		if( mShape )
		{
			mShape->Draw( gameTime );
		}

		////Uncomment to see target points (for edit purpose)
		//for( auto& it : mPath )
		//{
		//	it->Draw( gameTime );
		//}
	}
	std::vector<Point*>& Block::GetPointPath()
	{
		return mPath;
	}
	void Block::SetActive( bool in )
	{
		bIsActive = in;
	}
	bool Block::GetIsActive() const
	{
		return bIsActive;
	}
	void Block::SetIsOwnedByInsect( bool in )
	{
		bIsOwnedByInsect = in;
	}
	bool Block::GetIsOwnedByInsect() const
	{
		return bIsOwnedByInsect;
	}
	XMINT2 Block::GetGridPos() const
	{
 		return mGridPos;
	}
	void Block::SetGridPos( XMINT2 in )
	{
		mGridPos = in;
	}
	float Block::GetDistanceFloat() const
	{
		return mDistanceFloat;
	}
	void Block::Move( XMFLOAT3 movementDirection, const GameTime& gameTime )
	{
		XMVECTOR curPosition = XMLoadFloat3( &mTransitionVectorForAttachment );
		XMVECTOR direction = XMLoadFloat3( &movementDirection );
		XMStoreFloat3( &mTransitionVectorForAttachment, curPosition + direction * mVelocityFactor * gameTime.ElapsedGameTime() );
	}

	RTTI_DEFINITIONS( GameBlock )
		GameBlock::GameBlock( Game& game, Camera& camera, const DirectionalLight& directionalLightSource )
		:
		Block( game, camera, directionalLightSource ),
		mBlockFrame( nullptr )
	{
	}
	GameBlock::~GameBlock()
	{
		DeleteObject( mBlockFrame );
	}
	void GameBlock::Initialize()
	{
		//Shape
		mBlockFrame = new StaticModel( *this, mDirectionalLightSource );
		mBlockFrame->SetModel( "Content\\Models\\Block\\BlockFrame.obj" );
		mBlockFrame->SetColorTexture( L"Content\\Models\\Block\\BlockFrameTexture.png" );
		mBlockFrame->Initialize();
		mBlockFrame->DrawAmbientDiffuse();

		Block::Initialize();
	}
	void GameBlock::Update( const GameTime & gameTime )
	{
		mBlockFrame->Update( gameTime );
		UpdateAmbientLight( gameTime );
		Block::Update( gameTime );
	}
	void GameBlock::Draw( const GameTime & gameTime )
	{
		if( GetIsActive() )
		{
			mBlockFrame->Draw( gameTime );
		}
		Block::Draw( gameTime );
	}
	void GameBlock::UpdateAmbientLight( const GameTime & gameTime )
	{
		float ambientIntensity = mBlockFrame->GetAmbientColor().a;
		XMCOLOR ambientColor = mBlockFrame->GetAmbientColor();

		if( ambientIntensity < UCHAR_MAX )
		{
			ambientColor.a += UCHAR_MAX * gameTime.ElapsedGameTime() * 1.8f;
			mBlockFrame->SetAmbientColor( ambientColor );
		}
		else
		{
			ambientColor.a = 0.0f;
			mBlockFrame->SetAmbientColor( ambientColor );
		}
	}

	RTTI_DEFINITIONS( BlockA )
	BlockA::BlockA( Game& game, Camera& camera, const DirectionalLight& directionalLightSource )
		:
		GameBlock( game, camera, directionalLightSource )
	{
	}
	BlockA::~BlockA()
	{

	}
	void BlockA::Initialize()
	{
		//Shape
		mShape = new StaticModel( *this, mDirectionalLightSource );
		mShape->SetModel( "Content\\Models\\Block\\BlockA.obj" );
		mShape->SetColorTexture( L"Content\\Models\\Block\\StemColorTexture.png" );
		mShape->SetNormalMapTexture( L"Content\\Models\\Block\\BlockABCDNormalMapTexture.png" );
		mShape->Initialize();
		mShape->DrawPhongWithNormalMap();

		//Target Points
		mPath.push_back( new Point( *mGame, *mCamera, mDirectionalLightSource, *this ) );
		mPath.back()->AttachTransitionTo( this, XMFLOAT3( 0.0f, 2.0f, 8.0f ) );
		mPath.back()->Initialize();

		mPath.push_back( new Point( *mGame, *mCamera, mDirectionalLightSource, *this ) );
		mPath.back()->AttachTransitionTo( this, XMFLOAT3( 2.93f, 2.0f, 2.93f ) );
		mPath.back()->Initialize();

		mPath.push_back( new Point( *mGame, *mCamera, mDirectionalLightSource, *this ) );
		mPath.back()->AttachTransitionTo( this, XMFLOAT3( 8.0f, 2.0f, 0.0f ) );
		mPath.back()->Initialize();

		GameBlock::Initialize();
	}
	void BlockA::Draw( const GameTime & gameTime )
	{
		GameBlock::Draw( gameTime );
	}

	RTTI_DEFINITIONS( BlockB )

	BlockB::BlockB( Game& game, Camera& camera, const DirectionalLight& directionalLightSource )
		:
		GameBlock( game, camera, directionalLightSource )
	{
	}
	BlockB::~BlockB()
	{
	}
	void BlockB::Initialize()
	{
		//Shape
		mShape = new StaticModel( *this, mDirectionalLightSource );
		mShape->SetModel( "Content\\Models\\Block\\BlockB.obj" );
		mShape->SetColorTexture( L"Content\\Models\\Block\\StemColorTexture.png" );
		mShape->SetNormalMapTexture( L"Content\\Models\\Block\\BlockABCDNormalMapTexture.png" );
		mShape->Initialize();
		mShape->DrawPhongWithNormalMap();

		//Target Points
		mPath.push_back( new Point( *mGame, *mCamera, mDirectionalLightSource, *this ) );
		mPath.back()->AttachTransitionTo( this, XMFLOAT3( 0.0f, 2.0f, 8.0f ) );
		mPath.back()->Initialize();

		mPath.push_back( new Point( *mGame, *mCamera, mDirectionalLightSource, *this ) );
		mPath.back()->AttachTransitionTo( this, XMFLOAT3( -2.93f, 2.0f, 2.93f ) );
		mPath.back()->Initialize();

		mPath.push_back( new Point( *mGame, *mCamera, mDirectionalLightSource, *this ) );
		mPath.back()->AttachTransitionTo( this, XMFLOAT3( -8.0f, 2.0f, 0.0f ) );
		mPath.back()->Initialize();

		GameBlock::Initialize();
	}

	void BlockB::Draw( const GameTime & gameTime )
	{
		GameBlock::Draw( gameTime );
	}

	RTTI_DEFINITIONS( BlockC )

	BlockC::BlockC( Game& game, Camera& camera, const DirectionalLight& directionalLightSource )
		:
		GameBlock( game, camera, directionalLightSource )
	{
	}
	BlockC::~BlockC()
	{
	}
	void BlockC::Initialize()
	{
		//Shape
		mShape = new StaticModel( *this, mDirectionalLightSource );
		mShape->SetModel( "Content\\Models\\Block\\BlockC.obj" );
		mShape->SetColorTexture( L"Content\\Models\\Block\\StemColorTexture.png" );
		mShape->SetNormalMapTexture( L"Content\\Models\\Block\\BlockABCDNormalMapTexture.png" );
		mShape->Initialize();
		mShape->DrawPhongWithNormalMap();

		//Target Points
		mPath.push_back( new Point( *mGame, *mCamera, mDirectionalLightSource, *this ) );
		mPath.back()->AttachTransitionTo( this, XMFLOAT3( 0.0f, 2.0f, -8.0f ) );
		mPath.back()->Initialize();

		mPath.push_back( new Point( *mGame, *mCamera, mDirectionalLightSource, *this ) );
		mPath.back()->AttachTransitionTo( this, XMFLOAT3( 2.93f, 2.0f, -2.93f ) );
		mPath.back()->Initialize();

		mPath.push_back( new Point( *mGame, *mCamera, mDirectionalLightSource, *this ) );
		mPath.back()->AttachTransitionTo( this, XMFLOAT3( 8.0f, 2.0f, 0.0f ) );
		mPath.back()->Initialize();

		GameBlock::Initialize();
	}

	void BlockC::Draw( const GameTime & gameTime )
	{
		GameBlock::Draw( gameTime );
	}

	RTTI_DEFINITIONS( BlockD )

	BlockD::BlockD( Game& game, Camera& camera, const DirectionalLight& directionalLightSource )
		:
		GameBlock( game, camera, directionalLightSource )
	{
	}
	BlockD::~BlockD()
	{
	}
	void BlockD::Initialize()
	{
		//Shape
		mShape = new StaticModel( *this, mDirectionalLightSource );
		mShape->SetModel( "Content\\Models\\Block\\BlockD.obj" );
		mShape->SetColorTexture( L"Content\\Models\\Block\\StemColorTexture.png" );
		mShape->SetNormalMapTexture( L"Content\\Models\\Block\\BlockABCDNormalMapTexture.png" );
		mShape->Initialize();
		mShape->DrawPhongWithNormalMap();

		//Target Points
		mPath.push_back( new Point( *mGame, *mCamera, mDirectionalLightSource, *this ) );
		mPath.back()->AttachTransitionTo( this, XMFLOAT3( 0.0f, 2.0f, -8.0f ) );
		mPath.back()->Initialize();

		mPath.push_back( new Point( *mGame, *mCamera, mDirectionalLightSource, *this ) );
		mPath.back()->AttachTransitionTo( this, XMFLOAT3( -2.93f, 2.0f, -2.93f ) );
		mPath.back()->Initialize();

		mPath.push_back( new Point( *mGame, *mCamera, mDirectionalLightSource, *this ) );
		mPath.back()->AttachTransitionTo( this, XMFLOAT3( -8.0f, 2.0f, 0.0f ) );
		mPath.back()->Initialize();

		GameBlock::Initialize();
	}

	void BlockD::Draw( const GameTime & gameTime )
	{
		GameBlock::Draw( gameTime );
	}

	RTTI_DEFINITIONS( BlockE )

	BlockE::BlockE( Game& game, Camera& camera, const DirectionalLight& directionalLightSource )
		:
		GameBlock( game, camera, directionalLightSource )
	{
	}
	BlockE::~BlockE()
	{
	}
	void BlockE::Initialize()
	{
		//Shape
		mShape = new StaticModel( *this, mDirectionalLightSource );
		mShape->SetModel( "Content\\Models\\Block\\BlockE.obj" );
		mShape->SetColorTexture( L"Content\\Models\\Block\\StemColorTexture.png" );
		mShape->SetNormalMapTexture( L"Content\\Models\\Block\\BlockENormalMapTexture.png" );
		mShape->Initialize();
		mShape->DrawPhongWithNormalMap();

		//Target Points
		mPath.push_back( new Point( *mGame, *mCamera, mDirectionalLightSource, *this ) );
		mPath.back()->AttachTransitionTo( this, XMFLOAT3( -8.0f, 2.0f, 0.0f ) );
		mPath.back()->Initialize();

		mPath.push_back( new Point( *mGame, *mCamera, mDirectionalLightSource, *this ) );
		mPath.back()->AttachTransitionTo( this, XMFLOAT3( 0.0f, 2.0f, 0.0f ) );
		mPath.back()->Initialize();

		mPath.push_back( new Point( *mGame, *mCamera, mDirectionalLightSource, *this ) );
		mPath.back()->AttachTransitionTo( this, XMFLOAT3( 8.0f, 2.0f, 0.0f ) );
		mPath.back()->Initialize();

		GameBlock::Initialize();
	}

	void BlockE::Draw( const GameTime & gameTime )
	{
		GameBlock::Draw( gameTime );
	}

	RTTI_DEFINITIONS( BlockF )

	BlockF::BlockF( Game& game, Camera& camera, const DirectionalLight& directionalLightSource )
		:
		GameBlock( game, camera, directionalLightSource )
	{
	}
	BlockF::~BlockF()
	{
	}
	void BlockF::Initialize()
	{
		//Shape
		mShape = new StaticModel( *this, mDirectionalLightSource );
		mShape->SetModel( "Content\\Models\\Block\\BlockF.obj" );
		mShape->SetColorTexture( L"Content\\Models\\Block\\StemColorTexture.png" );
		mShape->SetNormalMapTexture( L"Content\\Models\\Block\\BlockFNormalMapTexture.png" );
		mShape->Initialize();
		mShape->DrawPhongWithNormalMap();

		//Target Points
		mPath.push_back( new Point( *mGame, *mCamera, mDirectionalLightSource, *this ) );
		mPath.back()->AttachTransitionTo( this, XMFLOAT3( 0.0f, 2.0f, -8.0f ) );
		mPath.back()->Initialize();

		mPath.push_back( new Point( *mGame, *mCamera, mDirectionalLightSource, *this ) );
		mPath.back()->AttachTransitionTo( this, XMFLOAT3( 0.0f, 2.0f, 0.0f ) );
		mPath.back()->Initialize();

		mPath.push_back( new Point( *mGame, *mCamera, mDirectionalLightSource, *this ) );
		mPath.back()->AttachTransitionTo( this, XMFLOAT3( 0.0f, 2.0f, 8.0f ) );
		mPath.back()->Initialize();

		GameBlock::Initialize();
	}

	void BlockF::Draw( const GameTime & gameTime )
	{
		GameBlock::Draw( gameTime );
	}

	RTTI_DEFINITIONS( BlockG )

	BlockG::BlockG( Game& game, Camera& camera, const DirectionalLight& directionalLightSource )
		:
		Block( game, camera, directionalLightSource )
	{
	}
	BlockG::~BlockG()
	{
	}
	void BlockG::Initialize()
	{
		//Target Points
		mPath.push_back( new Point( *mGame, *mCamera, mDirectionalLightSource, *this ) );
		mPath.back()->AttachTransitionTo( this, XMFLOAT3( -8.0f, 2.0f, 0.0f ) );
		mPath.back()->Initialize();

		mPath.push_back( new Point( *mGame, *mCamera, mDirectionalLightSource, *this ) );
		mPath.back()->AttachTransitionTo( this, XMFLOAT3( 0.0f, 2.0f, 0.0f ) );
		mPath.back()->Initialize();

		mPath.push_back( new Point( *mGame, *mCamera, mDirectionalLightSource, *this ) );
		mPath.back()->AttachTransitionTo( this, XMFLOAT3( 8.0f, 2.0f, 0.0f ) );
		mPath.back()->Initialize();

		Block::Initialize();
	}
	void BlockG::Draw( const GameTime & gameTime )
	{
		Block::Draw( gameTime );
	}

	RTTI_DEFINITIONS( ChamomileBlock )

	ChamomileBlock::ChamomileBlock( Game& game, Camera& camera, const DirectionalLight& directionalLightSource )
		:
		Block( game, camera, directionalLightSource )
	{
	}
	ChamomileBlock::~ChamomileBlock()
	{
	}
	void ChamomileBlock::Initialize()
	{
		mChamomile = std::unique_ptr<Chamomile>( new Chamomile( *mGame, *mCamera, mDirectionalLightSource ) );
		mChamomile.get()->AttachTransitionTo( this, XMFLOAT3( 0.0f, 0.0f, 0.0f ) );

		mChamomile.get()->Initialize();

		Block::Initialize();
	}
	void ChamomileBlock::Update( const GameTime & gameTime )
	{
		mChamomile->Update( gameTime );
		Block::Update( gameTime );
	}
	void ChamomileBlock::Draw( const GameTime & gameTime )
	{
		mChamomile->Draw( gameTime );
		Block::Draw( gameTime );
	}


	RTTI_DEFINITIONS( BlockHepatica )

	BlockHepatica::BlockHepatica( Game& game, Camera& camera, const DirectionalLight& directionalLightSource )
		:
		Block( game, camera, directionalLightSource )
	{
	}
	BlockHepatica::~BlockHepatica()
	{
	}
	void BlockHepatica::Initialize()
	{
		//Target Points
		mPath.push_back( new Point( *mGame, *mCamera, mDirectionalLightSource, *this ) );
		mPath.back()->AttachTransitionTo( this, XMFLOAT3( 0.0f, 18.0f, 0.0f ) );
		mPath.back()->Initialize();

		mHepatica = std::unique_ptr<Hepatica>( new Hepatica( *mGame, *mCamera, mDirectionalLightSource ) );
		mHepatica.get()->AttachTransitionTo( this, XMFLOAT3( 0.0f, 0.0f, 0.0f ) );

		///Set random yaw rotation
		std::random_device randomDevice;
		std::default_random_engine randomGenerator( randomDevice() );
		std::uniform_int_distribution<int> probDistribution( 0, 360 );
		int yaw = probDistribution( randomGenerator );
		mHepatica.get()->SetRotation( XMFLOAT3( 0.0f, yaw, 0.0f ) );

		mHepatica.get()->Initialize();

		Block::Initialize();
	}
	void BlockHepatica::Update( const GameTime & gameTime )
	{
		Block::Update( gameTime );
		mHepatica->Update( gameTime );
	}
	void BlockHepatica::Draw( const GameTime & gameTime )
	{
		mHepatica->Draw( gameTime );

		Block::Draw( gameTime );
	}

	RTTI_DEFINITIONS( TaraxacumBlock )

	TaraxacumBlock::TaraxacumBlock( Game& game, Camera& camera, const DirectionalLight& directionalLightSource )
		:
		Block( game, camera, directionalLightSource )
	{
	}
	TaraxacumBlock::~TaraxacumBlock()
	{
	}
	void TaraxacumBlock::Initialize()
	{
		//Target Points
		mPath.push_back( new Point( *mGame, *mCamera, mDirectionalLightSource, *this ) );
		mPath.back()->AttachTransitionTo( this, XMFLOAT3( 0.0f, 35.0f, 0.0f ) );
		mPath.back()->Initialize();

		mTaraxacum = std::unique_ptr<Taraxacum>( new Taraxacum( *mGame, *mCamera, mDirectionalLightSource ) );
		mTaraxacum.get()->AttachTransitionTo( this, XMFLOAT3( 0.0f, 0.0f, 0.0f ) );

		///Set random yaw rotation
		std::random_device randomDevice;
		std::default_random_engine randomGenerator( randomDevice() );
		std::uniform_int_distribution<int> probDistribution( 0, 360 );
		int yaw = probDistribution( randomGenerator );
		mTaraxacum.get()->SetRotation( XMFLOAT3( 0.0f, yaw, 0.0f ) );

		mTaraxacum.get()->Initialize();

		Block::Initialize();
	}
	void TaraxacumBlock::Update( const GameTime & gameTime )
	{
		Block::Update( gameTime );
		mTaraxacum->Update( gameTime );
	}
	void TaraxacumBlock::Draw( const GameTime & gameTime )
	{
		mTaraxacum->Draw( gameTime );

		Block::Draw( gameTime );
	}

	RTTI_DEFINITIONS( RedCloverShortBlock )

	RedCloverShortBlock::RedCloverShortBlock( Game& game, Camera& camera, const DirectionalLight& directionalLightSource )
		:
		Block( game, camera, directionalLightSource )
	{
	}
	RedCloverShortBlock::~RedCloverShortBlock()
	{
	}
	void RedCloverShortBlock::Initialize()
	{
		//Target Points
		mPath.push_back( new Point( *mGame, *mCamera, mDirectionalLightSource, *this ) );
		mPath.back()->AttachTransitionTo( this, XMFLOAT3( 0.0f, 55.0f, 0.0f ) );
		mPath.back()->Initialize();

		mRedCloverShort = std::unique_ptr<RedCloverShort>( new RedCloverShort( *mGame, *mCamera, mDirectionalLightSource ) );
		mRedCloverShort.get()->AttachTransitionTo( this, XMFLOAT3( 0.0f, 0.0f, 0.0f ) );

		///Set random yaw rotation
		std::random_device randomDevice;
		std::default_random_engine randomGenerator( randomDevice() );
		std::uniform_int_distribution<int> probDistribution( 0, 360 );
		int yaw = probDistribution( randomGenerator );
		mRedCloverShort.get()->SetRotation( XMFLOAT3( 0.0f, yaw, 0.0f ) );

		mRedCloverShort.get()->Initialize();

		Block::Initialize();
	}
	void RedCloverShortBlock::Update( const GameTime & gameTime )
	{
		Block::Update( gameTime );
		mRedCloverShort->Update( gameTime );
	}
	void RedCloverShortBlock::Draw( const GameTime & gameTime )
	{
		mRedCloverShort->Draw( gameTime );

		Block::Draw( gameTime );
	}

	RTTI_DEFINITIONS( RedCloverLeafBlock )

		RedCloverLeafBlock::RedCloverLeafBlock( Game& game, Camera& camera, const DirectionalLight& directionalLightSource )
		:
		Block( game, camera, directionalLightSource )
	{
	}
	RedCloverLeafBlock::~RedCloverLeafBlock()
	{
	}
	void RedCloverLeafBlock::Initialize()
	{
		mRedCloverLeaf = std::unique_ptr<RedCloverLeaf>( new RedCloverLeaf( *mGame, *mCamera, mDirectionalLightSource ) );
		mRedCloverLeaf.get()->AttachTransitionTo( this, XMFLOAT3( 0.0f, 0.0f, 0.0f ) );
		SetRotation( XMFLOAT3( 0.0f, 0.0f, 30.0f ) );
		mRedCloverLeaf.get()->Initialize();

		///Set random yaw rotation
		std::random_device randomDevice;
		std::default_random_engine randomGenerator( randomDevice() );
		std::uniform_int_distribution<int> probDistribution( 0, 360 );
		int yaw = probDistribution( randomGenerator );
		mRedCloverLeaf.get()->SetRotation( XMFLOAT3( 0.0f, yaw, 0.0f ) );

		Block::Initialize();
	}
	void RedCloverLeafBlock::Update( const GameTime & gameTime )
	{
		Block::Update( gameTime );
		mRedCloverLeaf->Update( gameTime );
	}
	void RedCloverLeafBlock::Draw( const GameTime & gameTime )
	{
		mRedCloverLeaf->Draw( gameTime );

		Block::Draw( gameTime );
	}

	RTTI_DEFINITIONS( RedCloverBlock )

	RedCloverBlock::RedCloverBlock( Game& game, Camera& camera, const DirectionalLight& directionalLightSource )
		:
		Block( game, camera, directionalLightSource )
	{
	}
	RedCloverBlock::~RedCloverBlock()
	{
	}
	void RedCloverBlock::Initialize()
	{
		mRedClover = std::unique_ptr<RedClover>( new RedClover( *mGame, *mCamera, mDirectionalLightSource ) );
		mRedClover.get()->AttachTransitionTo( this, XMFLOAT3( 0.0f, 0.0f, 0.0f ) );

		///Set random yaw rotation
		std::random_device randomDevice;
		std::default_random_engine randomGenerator( randomDevice() );
		std::uniform_int_distribution<int> probDistribution( 0, 360 );
		int yaw = probDistribution( randomGenerator );
		mRedClover.get()->SetRotation( XMFLOAT3( 0.0f, yaw, 0.0f ) );

		mRedClover.get()->Initialize();

		Block::Initialize();
	}
	void RedCloverBlock::Update( const GameTime & gameTime )
	{
		Block::Update( gameTime );
		mRedClover->Update( gameTime );
	}
	void RedCloverBlock::Draw( const GameTime & gameTime )
	{
		mRedClover->Draw( gameTime );

		Block::Draw( gameTime );
	}

	RTTI_DEFINITIONS( CentaureaCyanusShortBlock )

	CentaureaCyanusShortBlock::CentaureaCyanusShortBlock( Game& game, Camera& camera, const DirectionalLight& directionalLightSource )
		:
		Block( game, camera, directionalLightSource )
	{
	}
	CentaureaCyanusShortBlock::~CentaureaCyanusShortBlock()
	{
	}
	void CentaureaCyanusShortBlock::Initialize()
	{
		//Target Points
		mPath.push_back( new Point( *mGame, *mCamera, mDirectionalLightSource, *this ) );
		mPath.back()->AttachTransitionTo( this, XMFLOAT3( 0.0f, 39.0f, 0.0f ) );
		mPath.back()->Initialize();

		mCentaureaCyanusShort = std::unique_ptr<CentaureaCyanusShort>( new CentaureaCyanusShort( *mGame, *mCamera, mDirectionalLightSource ) );
		mCentaureaCyanusShort.get()->AttachTransitionTo( this, XMFLOAT3( 0.0f, 0.0f, 0.0f ) );

		///Set random yaw rotation
		std::random_device randomDevice;
		std::default_random_engine randomGenerator( randomDevice() );
		std::uniform_int_distribution<int> probDistribution( 0, 360 );
		int yaw = probDistribution( randomGenerator );
		mCentaureaCyanusShort.get()->SetRotation( XMFLOAT3( 0.0f, yaw, 0.0f ) );

		mCentaureaCyanusShort.get()->Initialize();

		Block::Initialize();
	}
	void CentaureaCyanusShortBlock::Update( const GameTime & gameTime )
	{
		Block::Update( gameTime );
		mCentaureaCyanusShort->Update( gameTime );
	}
	void CentaureaCyanusShortBlock::Draw( const GameTime & gameTime )
	{
		mCentaureaCyanusShort->Draw( gameTime );

		Block::Draw( gameTime );
	}

	RTTI_DEFINITIONS( CentaureaCyanusBlock )

	CentaureaCyanusBlock::CentaureaCyanusBlock( Game& game, Camera& camera, const DirectionalLight& directionalLightSource )
		:
		Block( game, camera, directionalLightSource )
	{
	}
	CentaureaCyanusBlock::~CentaureaCyanusBlock()
	{
	}
	void CentaureaCyanusBlock::Initialize()
	{
		//Target Points
		mPath.push_back( new Point( *mGame, *mCamera, mDirectionalLightSource, *this ) );
		mPath.back()->AttachTransitionTo( this, XMFLOAT3( 0.0f, 35.0f, 0.0f ) );
		mPath.back()->Initialize();

		mCentaureaCyanus = std::unique_ptr<CentaureaCyanus>( new CentaureaCyanus( *mGame, *mCamera, mDirectionalLightSource ) );
		mCentaureaCyanus.get()->AttachTransitionTo( this, XMFLOAT3( 0.0f, 0.0f, 0.0f ) );

		///Set random yaw rotation
		std::random_device randomDevice;
		std::default_random_engine randomGenerator( randomDevice() );
		std::uniform_int_distribution<int> probDistribution( 0, 360 );
		int yaw = probDistribution( randomGenerator );
		mCentaureaCyanus.get()->SetRotation( XMFLOAT3( 0.0f, yaw, 0.0f ) );

		mCentaureaCyanus.get()->Initialize();

		Block::Initialize();
	}
	void CentaureaCyanusBlock::Update( const GameTime & gameTime )
	{
		Block::Update( gameTime );
		mCentaureaCyanus->Update( gameTime );
	}
	void CentaureaCyanusBlock::Draw( const GameTime & gameTime )
	{
		mCentaureaCyanus->Draw( gameTime );

		Block::Draw( gameTime );
	}

	RTTI_DEFINITIONS( GrassBlock )

		GrassBlock::GrassBlock( Game& game, Camera& camera, const DirectionalLight& directionalLightSource )
		:
		Block( game, camera, directionalLightSource )
	{
	}
	GrassBlock::~GrassBlock()
	{
	}
	void GrassBlock::Initialize()
	{
		mGrass = std::unique_ptr<Grass>( new Grass( *mGame, *mCamera, mDirectionalLightSource ) );
		mGrass.get()->AttachTransitionTo( this, XMFLOAT3( 0.0f, 0.0f, 0.0f ) );

		///Set random yaw rotation
		std::random_device randomDevice;
		std::default_random_engine randomGenerator( randomDevice() );
		std::uniform_int_distribution<int> probDistribution( 0, 360 );
		int yaw = probDistribution( randomGenerator );
		mGrass.get()->SetRotation( XMFLOAT3( 0.0f, yaw, 0.0f ) );

		mGrass.get()->Initialize();

		Block::Initialize();
	}
	void GrassBlock::Update( const GameTime & gameTime )
	{
		Block::Update( gameTime );
		mGrass->Update( gameTime );
	}
	void GrassBlock::Draw( const GameTime & gameTime )
	{
		mGrass->Draw( gameTime );

		Block::Draw( gameTime );
	}
}