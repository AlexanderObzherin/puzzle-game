#pragma once

#include "RenderStateHelper.h"
#include "Actor.h"

using namespace Library;

namespace Rendering
{
	class PointSprite;

	class Logo : public Actor
	{
		RTTI_DECLARATIONS( Logo, Actor )

	public:
		Logo( Game& game, Camera& camera );
		~Logo();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

	protected:
		PointSprite* mPointSprite;

	};
}