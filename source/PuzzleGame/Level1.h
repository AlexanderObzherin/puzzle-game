#pragma once

#include "LevelsCommon.h"
#include <list>
#include <unordered_map>
#include <functional>
#include "Randomizer.h"

namespace Rendering
{
	class Level1 : public LevelsCommon
	{
		RTTI_DECLARATIONS( Level1, LevelsCommon )

	private:
		enum StartMenu
		{
			StartGame = 1,
			ExitStartMenu = 2
		};

	public:
		Level1( Game& game, SceneManager& sceneManager );
		~Level1();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		void ScenarioTerm01( const GameTime& gameTime );
		void ScenarioTerm02( const GameTime& gameTime );
		void ScenarioTerm03( const GameTime& gameTime );
		void ScenarioTerm04( const GameTime& gameTime );
		void ScenarioTerm05( const GameTime& gameTime );

	private:
		//Start menu
		StartMenu mStartMenuActiveOption;
		bool bStartMenuIsActive;

		//To measure time of ladybug wings moving
		float mLadybugWingMovingTimer;


	};

}

