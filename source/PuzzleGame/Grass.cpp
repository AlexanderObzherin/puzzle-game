#include "Grass.h"
#include "StaticModel.h"

namespace Rendering
{
	RTTI_DEFINITIONS( Grass )

		Grass::Grass( Game& game, Camera& camera, const DirectionalLight& directionalLightSource )
		:
		Actor( game, camera ),
		mDirectionalLightSource( directionalLightSource ),
		mShape( nullptr )
	{
	}

	Grass::~Grass()
	{
		DeleteObject( mShape );
	}

	void Grass::Initialize()
	{
		SetScale( XMFLOAT3( 2.0f, 2.0f, 2.0f ) );
		Actor::Initialize();

		mShape = new StaticModel( *this, mDirectionalLightSource );
		mShape->SetModel( "Content\\Models\\Grass\\Grass.obj" );
		mShape->SetColorTexture( L"Content\\Models\\Grass\\GrassColorTexture.png" );

		mShape->Initialize();
		mShape->DrawAmbientDiffuse();
	}
	void Grass::Update( const GameTime& gameTime )
	{
		Actor::Update( gameTime );
	}
	void Grass::Draw( const GameTime& gameTime )
	{
		mShape->Draw( gameTime );
	}

}