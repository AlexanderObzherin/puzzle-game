#include "SceneManager.h"
#include "StartScene.h"

namespace Rendering
{
	SceneManager::SceneManager( Game & game )
		:
		DrawableGameComponent( game ),
		mCurrentScene( nullptr )
	{
	}
	SceneManager::~SceneManager()
	{
		DeleteObject( mCurrentScene );
	}
	void SceneManager::Initialize()
	{
		SetCurrentScene( new StartScene( *mGame, *this ) );
		GetCurrentScene().Initialize();		
	}
	void SceneManager::Update( const GameTime & gameTime )
	{
		GetCurrentScene().Update( gameTime );
	}
	void SceneManager::Draw( const GameTime & gameTime )
	{
		GetCurrentScene().Draw( gameTime );
	}
}