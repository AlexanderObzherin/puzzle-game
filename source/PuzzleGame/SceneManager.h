#pragma once

#include "DrawableGameComponent.h"

using namespace Library;

namespace Rendering
{
	class Scene;
	class SceneManager : public DrawableGameComponent
	{
	public:
		SceneManager( Game& game );
		~SceneManager();

		virtual void Initialize() override;
		virtual void Update( const GameTime& gameTime ) override;
		virtual void Draw( const GameTime& gameTime ) override;

		Scene& GetCurrentScene() 
		{ 
			return *mCurrentScene; 
		}
		void SetCurrentScene( Scene* newScene )
		{
			mCurrentScene = newScene;
		}

	protected:
		Scene* mCurrentScene;
	};

}
