#include "Lawn.h"
#include "StaticModel.h"

namespace Rendering
{
	RTTI_DEFINITIONS( Lawn )

		Lawn::Lawn( Game& game, Camera& camera, const DirectionalLight& directionalLightSource )
		:
		Actor( game, camera ),
		mDirectionalLightSource( directionalLightSource ),
		mShape( nullptr )
	{
	}

	Lawn::~Lawn()
	{
		DeleteObject( mShape );
	}

	void Lawn::Initialize()
	{
		Actor::Initialize();

		mShape = new StaticModel( *this, mDirectionalLightSource );
		mShape->SetModel( "Content\\Models\\Lawn\\Floor.obj" );
		mShape->SetColorTexture( L"Content\\Models\\Lawn\\LawnColorTexture.png" );

		mShape->Initialize();
		mShape->DrawAmbientDiffuseWithNormalMap();
	}
	void Lawn::Update( const GameTime& gameTime )
	{
		Actor::Update( gameTime );
	}
	void Lawn::Draw( const GameTime& gameTime )
	{
		mShape->Draw( gameTime );
	}

}